#!/usr/bin/bash


# Basic range in for loop

i=1
while [ $i -le $1 ]; do
	run_name="run$i"
    	cd $run_name/

		bsub  -P c7 -q $2 ./run.sh

    	cd ..

	i=$(($i+1))
done

