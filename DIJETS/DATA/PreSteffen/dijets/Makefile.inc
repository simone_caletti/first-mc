# PYTHIA configuration file.
# Generated on Mon 16 Nov 2020 07:22:15 PM CET with the user supplied options:
# --prefix=/home/scaletti/PYTHIA8
# --with-hepmc2=/home/scaletti/hepmc2
# --with-fastjet3=/home/scaletti/fastjet
# --with-lhapdf6=/home/scaletti/lhapdf
# --with-rivet=/home/scaletti/RIVET2

# Install directory prefixes.
PREFIX_BIN=/home/scaletti/PYTHIA8/bin
PREFIX_INCLUDE=/home/scaletti/PYTHIA8/include
PREFIX_LIB=/home/scaletti/PYTHIA8/lib
PREFIX_SHARE=/home/scaletti/PYTHIA8/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
CXX=g++
CXX_COMMON=-O2 -std=c++11 -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname,
LIB_SUFFIX=.so
OBJ_COMMON=

EVTGEN_USE=false
EVTGEN_CONFIG=
EVTGEN_BIN=
EVTGEN_INCLUDE=
EVTGEN_LIB=

FASTJET3_USE=true
FASTJET3_CONFIG=fastjet-config
FASTJET3_BIN=/home/scaletti/fastjet/bin/
FASTJET3_INCLUDE=-I/home/scaletti/fastjet/include
FASTJET3_LIB=-L/home/scaletti/fastjet/lib -Wl,-rpath,/home/scaletti/fastjet/lib -lfastjet

HEPMC2_USE=true
HEPMC2_CONFIG=
HEPMC2_BIN=/home/scaletti/hepmc2/
HEPMC2_INCLUDE=-I/home/scaletti/hepmc2/include
HEPMC2_LIB=-L/home/scaletti/hepmc2/lib -Wl,-rpath,/home/scaletti/hepmc2/lib -lHepMC

HEPMC3_USE=false
HEPMC3_CONFIG=
HEPMC3_BIN=
HEPMC3_INCLUDE=
HEPMC3_LIB=

LHAPDF5_USE=false
LHAPDF5_CONFIG=
LHAPDF5_BIN=
LHAPDF5_INCLUDE=
LHAPDF5_LIB=

LHAPDF6_USE=true
LHAPDF6_CONFIG=lhapdf-config
LHAPDF6_BIN=/home/scaletti/lhapdf/bin/
LHAPDF6_INCLUDE=-I/home/scaletti/lhapdf/include
LHAPDF6_LIB=-L/home/scaletti/lhapdf/lib -Wl,-rpath,/home/scaletti/lhapdf/lib -lLHAPDF

POWHEG_USE=false
POWHEG_CONFIG=lhapdf-config
POWHEG_BIN=
POWHEG_INCLUDE=
POWHEG_LIB=

RIVET_USE=true
RIVET_CONFIG=rivet-config
RIVET_BIN=/home/scaletti/RIVET2/bin/
RIVET_INCLUDE=-I/home/scaletti/RIVET2/include -I/home/scaletti/fastjet/include -I/home/scaletti/hepmc2/include -I/home/scaletti/yoda-1.7.7/include
RIVET_LIB=-L/home/scaletti/RIVET2/lib -Wl,-rpath,/home/scaletti/RIVET2/lib -lRivet -L/home/scaletti/fastjet/lib -Wl,-rpath,/home/scaletti/fastjet/lib -lfastjet -L/home/scaletti/hepmc2/lib -Wl,-rpath,/home/scaletti/hepmc2/lib -lHepMC -L/home/scaletti/yoda-1.7.7/lib -Wl,-rpath,/home/scaletti/yoda-1.7.7/lib -lYODA

ROOT_USE=false
ROOT_CONFIG=rivet-config
ROOT_BIN=
ROOT_INCLUDE=
ROOT_LIB=

YODA_USE=true
YODA_CONFIG=yoda-config
YODA_BIN=
YODA_INCLUDE=-I/home/scaletti/yoda-1.7.7/include
YODA_LIB=-L/home/scaletti/yoda-1.7.7/lib -Wl,-rpath,/home/scaletti/yoda-1.7.7/lib -lYODA

GZIP_USE=false
GZIP_CONFIG=yoda-config
GZIP_BIN=
GZIP_INCLUDE=
GZIP_LIB=

PYTHON_USE=false
PYTHON_CONFIG=yoda-config
PYTHON_BIN=
PYTHON_INCLUDE=
PYTHON_LIB=

MG5MES_USE=false
MG5MES_CONFIG=yoda-config
MG5MES_BIN=
MG5MES_INCLUDE=
MG5MES_LIB=

OPENMP_USE=false
OPENMP_CONFIG=yoda-config
OPENMP_BIN=
OPENMP_INCLUDE=
OPENMP_LIB=
