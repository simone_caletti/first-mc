#!/usr/bin/bash

echo "YODAMERGE all the YODA in each run folder."
echo " "

#Create one YODA file per slice collecting events from all the runs in the slice.
echo "Merging slice$i . . ."
to_yodamerge=""

j=1
while [ $j -le $1 ]; do
	to_yodamerge+="run$j/sim_$j.yoda "
    		

	j=$(($j+1))
done

yodamerge $to_yodamerge -o merge.yoda
echo ". . . done!"
cd ..

