import numpy as np
###########################################
# YODA functions
###########################################
## This one is used to read *.dat* files
## containing yoda histograms
###########################################
def yoda_get_bins (path, obs):
    plot = open(path + obs)

    if_begin = False
    
    if_end = False
    
    x_low  = []
    x_high = []
    
    err_minus = []
    err_plus  = []
    
    bin_edges = []
    bin_entry = []
    
    for line in plot:
        words = line.split()
 
        if len(words) == 6:
            if words[0] == '#' and words[1] == 'xlow' and words[2] == 'xhigh':
                # xlow	 xhigh	 val	 errminus	 errplus
                if_begin = True
                
                if_end = False
                
        if len(words) == 3:
            if words[0] == '#' and words[1] == 'END' and words[2] == 'HISTO1D':
                if_end = True
            
                if_begin = False
                
                break

        if len(words) == 5 and if_begin == True and if_end == False:
            x_low.append( float(words[0]) )
            
            x_high.append( float(words[1]) )
                
            bin_entry.append( float(words[2]) )
            
            err_minus.append( float(words[3]) )
            
            err_plus.append( float(words[4]) )
    
    for el in x_low:
        bin_edges.append(el)
    
    bin_edges.append( x_high[-1] )
    
    bin_center = []
    
    for i, el in enumerate(bin_edges[:-1]):
        bin_center.append( 0.5 * (bin_edges[i + 1] + bin_edges[i]) )
    
    return bin_edges, bin_center, bin_entry, err_minus, err_plus
###########################################
## This one is used to extract x-section
## out of the one-bin histograms containing
## x-sections per pT-slice
###########################################
def yoda_get_xsection (path_log, path_xs):
    # Extract X-section
    sigma_MC = 0.
    
    errminus = 0.
    
    errplus = 0.
    
    # Let's get a number of successful runs
    log_file = open(path_log)
    
    for line in log_file:
        words = line.split()
        
        if len(words) > 0 and words[0] == 'central':
            n_jobs = float(words[5])
        if len(words) > 0 and words[0] == 'plots':
            n_jobs = float(words[5])
    
    print('N jobs = ', n_jobs)

    plot_xs = open(path_xs)
 
    if_begin = False
    
    if_end = False
    
    for line in plot_xs:
        words = line.split()
        
        if len(words) == 6:
            if words[0] == '#' and words[1] == 'xlow' and words[2] == 'xhigh':
                # xlow	 xhigh	 val	 errminus	 errplus
                if_begin = True
                
                if_end = False
        
        if len(words) == 5 and if_begin == True and if_end == False:
            sigma_MC = float(words[2]) / n_jobs
     
            errminus = float(words[3]) / n_jobs
            
            errpluss = float(words[4]) / n_jobs
      
            break
    
    return sigma_MC, errminus, errplus
###########################################
def yoda_dn_dl (path, obs):
    edges, bins, hist, err_m, err_p = yoda_get_bins (path, obs)
    
    N = np.sum ( hist * np.diff(edges) )
 
    return hist, N
###########################################
