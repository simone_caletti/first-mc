#include <fstream>
#include <string>
#include <string>

#include "Pythia8/Pythia.h"

#include "Pythia8Plugins/Pythia8Rivet.h" 

using namespace Pythia8;
//---------------------------------------------------------------------------------------------------------------------------
void set_pythia (Pythia &pythia, int seed, std::string mode) 
{
    //Common settings
    // Seed 
    pythia.readString("Random:setSeed = on");

    pythia.readString("Random:seed = " + std::to_string(seed));
    
    pythia.readString("Beams:frameType = 1");
    pythia.readString("Beams:eCM = 13000.");
 
    pythia.readString("PhaseSpace:pTHatMin  = 75");

    //  Process 
    pythia.readString("WeakBosonAndParton:qg2gmZq = on");
    pythia.readString("WeakBosonAndParton:qqbar2gmZg  = on");
    
    // Force decay Z -> mu+-
    pythia.readString("23:onMode = off");
    pythia.readString("23:onIfAny = 13");
 
    // Process-level
    if (mode == "PS") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = off");
        pythia.readString("HadronLevel:all = off");
    }
    else if (mode == "PS_HAD") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = off");
        pythia.readString("HadronLevel:all = on");
    }
    else if (mode == "PS_MPI_HAD") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = on");
        pythia.readString("HadronLevel:all = on");
    }
    else {
        std::cout << "Wrong settings!\n";
        
        abort();
    }
    
    pythia.readString("PartonLevel:Remnants  = on");
    pythia.readString("Check:event = on");
}
//---------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[]) 
{
    // Get argumets from command line ( <n_of_events> <seed> <mode> <sim_name>)
    if (argc != 5) 
    {
        std::cout << "Wring number of argumenst " << argc << " != 5 is provided!\n";
    
        abort();
    }
    
    int n_event = std::stoi(argv[1]);
    
    int seed = std::stoi(argv[2]);
    
    std::string mode = argv[3];
    
    std::string yoda_name = argv[4];
    
    //Set Pythia
    Pythia pythia;
    
    set_pythia (pythia, seed, mode);
 
    pythia.init();
    
    // Create a pipe between PYTHIA and RIVET
    Pythia8Rivet rivet (pythia, yoda_name + ".yoda"); 
    
    // Set name of the RIVET analysis you use  
    rivet.addAnalysis("PAPER_FIN_R8");
    rivet.addAnalysis("PAPER_FIN_R4");
    rivet.addAnalysis("PAPER_FIN_R8_CH");
    rivet.addAnalysis("PAPER_FIN_R4_CH");

    // Loop over all events
    for (int iEvent = 0; iEvent < n_event; ++iEvent) 
    {
        // Check if the event is not broken
        if (!pythia.next()) continue; 
        
	// If event is OK pass it to the RIVET
        rivet(); 
    } 
 
    // End of job. Create YODA files
    rivet.done(); 
    
    return 0;
}

