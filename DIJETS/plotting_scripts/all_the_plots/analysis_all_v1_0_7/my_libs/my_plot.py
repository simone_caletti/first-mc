import math as m
import numpy as np

from my_drawers import plot_boxes, get_y, get_ratio, plot_ratio, plot_env

# These are used to read yoda output
from my_yoda import yoda_get_bins, yoda_get_xsection, yoda_dn_dl

###################################################################
#####  DRAWERS
###################################################################
###################################################################
# This one is used to plot yoda files 
# and to convert them into histograms
# also in case of the Sherpa it is used to 
# get the scale variation error
# Plots $(\lambda / \sigma) (d\sigma / d\lambda)$ distrbutions
# and corresponding errors and ratios
# Returns $d\sigma / d\lambda$ distrbutions and corresponding 
# errors
####################################################################
def plot_mc(ax, path, col, ls, R, alpha, pTL, pTR, if_scl_var, is_sd, if_show = True, if_hatch = False):
    # Set names of the files we read 
    if is_sd:
        sd = "_SD_"
    else:
        sd = "_"
 
    if alpha == 0.5:
        obs = 'PAPER_FIN_R'   + str(R) + '_CH_lin' + sd + 'l05_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    elif alpha == 1.0:
        obs = 'PAPER_FIN_R'   + str(R) + '_CH_lin' + sd + 'l10_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    elif alpha == 2.0:
        obs = 'PAPER_FIN_R'   + str(R) + '_CH_lin' + sd + 'l20_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    else:
        print ('Wrong value of alpha!')
    
    #print(path)
    
    xsec_name = 'PAPER_FIN_R'   + str(R) + '_CH_XS_CUTS_pT_' + str(pTL) + '_' + str(pTR) + '.dat'

    # Get central value for the dN / d\lambda distribution
    edges_c, bins_c, h_c, er_m_c, er_p_c = yoda_get_bins (path + '/central/', obs)
    
    # Number of events (area under a histogram)
    N_c = np.sum ( h_c * np.diff(edges_c) )

    # Scale it to unity and plot (\lambda / \sigma) (d\sigma / d\lambda)
    h_c_fnl = []
    
    for x, y in zip(bins_c, h_c):
        h_c_fnl.append(x * y / N_c)
 
    # Plot (\lambda / \sigma) (d\sigma / d\lambda)
    if if_show:
        ax.hist(bins_c, bins=edges_c, weights=h_c_fnl, color=col, linestyle=ls, linewidth=6, histtype= 'step')

    # Extract X-section
    sig_c_pb = [] # sigma errmin, errpls
    
    sig_c_pb = yoda_get_xsection (path + 'n_jobs_log.dat', path + '/central/' + xsec_name)

    print ('sigma_cnt = ', sig_c_pb[0], ' pb')
    
    # Get d\sigma / d\lambda distribution
    # this one we return
    h_c_ds_dl = []
    
    for el in h_c:
        h_c_ds_dl.append(sig_c_pb[0] * el / N_c)

    # Do scale variation
    if if_scl_var and if_show:
        # Extract dN_i / d\lambda and N_i for each scale (7-point variation) 
        h_sc = []
        N_sc = []
    
        # Run over yoda files for each combunatin of $\mu_F$, $\mu_R$
        for i in range(1, 8):
            pth  =  path + "scale_{}/".format(str(i))

            h_t, N_t = yoda_dn_dl (pth, obs) 
        
            h_sc.append(h_t)
        
            N_sc.append(N_t)
        
        # Compute per-bin ratios $(dN_i / d\lambda) / (dN_c / d\lamda)$
        # wrt central value distribution 
        # convert them into errors for the (d\sigma/d\lambda) distrbutions
        err_min = []
        err_max = []
    
        for i0, i1, i2, i3, i4, i5, i6, i7 in zip(h_c, h_sc[0], h_sc[1], h_sc[2], h_sc[3], h_sc[4], h_sc[5], h_sc[6]):
            i1 = i1 / i0  
            i2 = i2 / i0  
            i3 = i3 / i0  
            i4 = i4 / i0  
            i5 = i5 / i0  
            i6 = i6 / i0  
            i7 = i7 / i0  
        
            sf_max = max(i1, i2, i3, i4, i5, i6, i7)
            sf_min = min(i1, i2, i3, i4, i5, i6, i7)
 
            err_min.append( sig_c_pb[0] * i0 * (1 - sf_min) / N_c )
            err_max.append( sig_c_pb[0] * i0 * (sf_max - 1) / N_c )
            
        # We got d\sigma / d\lambda and corresponding scale var. errors
        # Now we want to plot (\lambda / \sigma) (d\sigma / d\lambda)
        # therefore we need to rescale each bin by (\lambda / \sigma)
        d_m_fnl = []
        d_p_fnl = []
        
        for x, e1, e2 in zip(bins_c, err_min, err_max):
            d_m_fnl.append(x * e1 / sig_c_pb[0])
            d_p_fnl.append(x * e2 / sig_c_pb[0])
        
        # Plot shaded boxes (scale variation error)
        plot_boxes(ax, col, edges_c, h_c_fnl, d_m_fnl, d_p_fnl, if_hatch)
            
    # We need to return \sigma, d\sigma / d\lambda and scale var. errors
    # Note that these distrbutions are not scaled to unity
    if if_scl_var:
        return edges_c, sig_c_pb[0], h_c_ds_dl, err_min, err_max
    else:
        return edges_c, sig_c_pb[0], h_c_ds_dl
###################################################################
# Same as above but should be used for charged fs-particles only
####################################################################
def plot_mc_chr(ax, path, col, ls, R, alpha, pTL, pTR, if_scl_var, is_sd, if_show = True, if_hatch = False):
    # Set names of the files we read 
    if is_sd:
        sd = "_SD_"
    else:
        sd = "_"
 
    if alpha == 0.5:
        obs = 'PAPER_FIN_R'   + str(R) + '_ch_lin' + sd + 'l05_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    elif alpha == 1.0:
        obs = 'PAPER_FIN_R'   + str(R) + '_ch_lin' + sd + 'l10_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    elif alpha == 2.0:
        obs = 'PAPER_FIN_R'   + str(R) + '_ch_lin' + sd + 'l20_pT_' + str(pTL) + '_' + str(pTR) + '.dat'
    else:
        print ('Wrong value of alpha!')
        
    xsec_name = 'PAPER_FIN_R'   + str(R) + '_XS_CUTS_pT_' + str(pTL) + '_' + str(pTR) + '.dat'

    # Get central value for the dN / d\lambda distribution
    edges_c, bins_c, h_c, er_m_c, er_p_c = yoda_get_bins (path + '/central/', obs)
    
    # Number of events (area under a histogram)
    N_c = np.sum ( h_c * np.diff(edges_c) )

    # Scale it to unity and plot (\lambda / \sigma) (d\sigma / d\lambda)
    h_c_fnl = []
    
    for x, y in zip(bins_c, h_c):
        h_c_fnl.append(x * y / N_c)
 
    # Plot (\lambda / \sigma) (d\sigma / d\lambda)
    if if_show:
        ax.hist(bins_c, bins=edges_c, weights=h_c_fnl, color=col, linestyle=ls, linewidth=6, histtype= 'step')

    # Extract X-section
    sig_c_pb = [] # sigma errmin, errpls
    
    sig_c_pb = yoda_get_xsection (path + 'n_jobs_log.dat', path + '/central/' + xsec_name)

    print ('sigma_cnt = ', sig_c_pb[0], ' pb')
    
    # Get d\sigma / d\lambda distribution
    # this one we return
    h_c_ds_dl = []
    
    for el in h_c:
        h_c_ds_dl.append(sig_c_pb[0] * el / N_c)

    # Do scale variation
    if if_scl_var and if_show:
        # Extract dN_i / d\lambda and N_i for each scale (7-point variation) 
        h_sc = []
        N_sc = []
    
        # Run over yoda files for each combunatin of $\mu_F$, $\mu_R$
        for i in range(1, 8):
            pth  =  path + "scale_{}/".format(str(i))

            h_t, N_t = yoda_dn_dl (pth, obs) 
        
            h_sc.append(h_t)
        
            N_sc.append(N_t)
        
        # Compute per-bin ratios $(dN_i / d\lambda) / (dN_c / d\lamda)$
        # wrt central value distribution 
        # convert them into errors for the (d\sigma/d\lambda) distrbutions
        err_min = []
        err_max = []
    
        for i0, i1, i2, i3, i4, i5, i6, i7 in zip(h_c, h_sc[0], h_sc[1], h_sc[2], h_sc[3], h_sc[4], h_sc[5], h_sc[6]):
            i1 = i1 / i0  
            i2 = i2 / i0  
            i3 = i3 / i0  
            i4 = i4 / i0  
            i5 = i5 / i0  
            i6 = i6 / i0  
            i7 = i7 / i0  
        
            sf_max = max(i1, i2, i3, i4, i5, i6, i7)
            sf_min = min(i1, i2, i3, i4, i5, i6, i7)
 
            err_min.append( sig_c_pb[0] * i0 * (1 - sf_min) / N_c )
            err_max.append( sig_c_pb[0] * i0 * (sf_max - 1) / N_c )
            
        # We got d\sigma / d\lambda and corresponding scale var. errors
        # Now we want to plot (\lambda / \sigma) (d\sigma / d\lambda)
        # therefore we need to rescale each bin by (\lambda / \sigma)
        d_m_fnl = []
        d_p_fnl = []
        
        for x, e1, e2 in zip(bins_c, err_min, err_max):
            d_m_fnl.append(x * e1 / sig_c_pb[0])
            d_p_fnl.append(x * e2 / sig_c_pb[0])
        
        # Plot shaded boxes (scale variation error)
        plot_boxes(ax, col, edges_c, h_c_fnl, d_m_fnl, d_p_fnl, if_hatch)
            
    # We need to return \sigma, d\sigma / d\lambda and scale var. errors
    # Note that these distrbutions are not scaled to unity
    if if_scl_var:
        return edges_c, sig_c_pb[0], h_c_ds_dl, err_min, err_max
    else:
        return edges_c, sig_c_pb[0], h_c_ds_dl
####################################################################
# This one is used to plot resumed NLO+NLL' predictions
####################################################################
def plot_res(ax, path, col, ls, R, alpha, pTL, pTR,  if_sd, if_ch_bins, if_show = True, if_hatch = False):
    # convert pT-slice into y (CMS notation)
    y = get_y(pTL, pTR)

    r = 0
    
    if R == 8:
        r = 2
    elif R == 4:
        r = 1
    
    sd = 0
    
    if if_sd:
        sd  = 2
    else:
        sd = 1
 
    prefix = 'CMS_2018_PAS_SMP_18_QGX_ZPJ_d' + str(r) + str(sd) + '-x' 
 
    obs = ''
  
    if alpha == 0.5:
        if if_ch_bins:
            # Bins used CMS for charged fs-particles only
            obs = prefix + '08-y' + y + '.dat'
        else:
            # for charged + neutral 
            obs = prefix + '03-y' + y + '.dat'
    elif alpha == 1.0:
        if if_ch_bins:
            # Bins used CMS for charged fs-particles only
            obs = prefix + '09-y' + y + '.dat'
        else:
            # for charged + neutral 
            obs = prefix + '04-y' + y + '.dat'
    elif alpha == 2.0:
        if if_ch_bins:
            # Bins used CMS for charged fs-particles only
            obs = prefix + '10-y' + y + '.dat'
        else:
            # for charged + neutral 
            obs = prefix + '05-y' + y + '.dat'
    else:
        print ('Wrong value of alpha!')
    
    # Get central value for the d\sgima / d\lambda distribution
    edges, bins, h_res, er_min, er_pls= yoda_get_bins (path, obs)
    
    # Number of events (area under a histogram)
    sig_res_pb = np.sum ( h_res * np.diff(edges) )

    print('sigma_res = ', sig_res_pb, ' pb')
    
    # Scale it to unity and plot (\lambda / \sigma) (d\sigma / d\lambda)
    h_res_fnl = []
    
    for x, y in zip(bins, h_res):
        h_res_fnl.append(x * y / sig_res_pb)
 
    # Plot (\lambda / \sigma) (d\sigma / d\lambda)
    if if_show:
        ax.hist(bins, bins=edges, weights=h_res_fnl, color=col, linestyle=ls, linewidth=6, histtype= 'step')
    
    
    # We got d\sigma / d\lambda and corresponding scale var. errors
    # Now we want to plot scale variation for  (\lambda / \sigma) (d\sigma / d\lambda)
    d_m_fnl = []
    d_p_fnl = []
    
    # Rescale errors by (\lambda / \sigma) factor
    for x, e1, e2 in zip(bins, er_min, er_pls):
        d_m_fnl.append(x * e1 / sig_res_pb)
        d_p_fnl.append(x * e2 / sig_res_pb)
        
    # Plot shaded boxes (scale variation error)
    if if_show:
        plot_boxes(ax, col, edges, h_res_fnl, d_m_fnl, d_p_fnl, if_hatch)
    
    # Return (d\sigma / d\lambda)
    return edges, sig_res_pb, h_res, er_min, er_pls
###################################################################
# This one is used to plot NP/PS envelopes
# and to get corresponding error bands
###################################################################
def plot_band_np_vs_ps(ax, col, lb, ls, edges, h1_np, h2_np, h3_np, h1_ps, h2_ps, h3_ps, if_show, if_hatch = False):
    rc_1 = get_ratio(h1_np, h1_ps)
    rc_2 = get_ratio(h2_np, h2_ps)
    rc_3 = get_ratio(h3_np, h3_ps)
 
    # Make the envelope
    r_min = []
    r_max = []
    r_cnt = []
    
    # Get the sym error
    err_min = []
    err_pls = []
    
    for i1, i2, i3 in zip(rc_1, rc_2, rc_3):
        min_val = min(i1, i2, i3)
        max_val = max(i1, i2, i3)
 
        c_val = (max_val + min_val) / 2.
 
        r_min.append(min_val)
        r_max.append(max_val)
        
        r_cnt.append(c_val)
        
        err_min.append(c_val - min_val)
        
        err_pls.append(max_val - c_val)
    
    if if_show:
        # Plot the central line
        plot_ratio (ax, col, '', ls, edges, r_cnt, [1] * len(r_cnt))
    
        # Plot the envelope
        plot_env(ax, col, edges, r_cnt, r_min, r_max, if_hatch)
    
    return r_cnt, err_min, err_pls
