#####################
# Cosmetics for axes
#####################
def set_logy_axis (ax, pow_min, pow_max, if_show = True):
    ax.set_yscale('log')

    yticks_ps    = []
    yticks_lb    = []

    pow_it = pow_min
    
    while pow_it <= pow_max:
        
        yticks_ps.append( m.pow(10, pow_it) )
        yticks_lb.append( pow_it )
        
        pow_it = pow_it + 1
    
    yticks = []
    for el in yticks_lb:
        string = '$10^{' + str(el) +  '}$'
        yticks.append( string )
    
    ax.set_yticks(yticks_ps)
    ax.set_yticklabels(yticks)

    shift = 1e-8

    if y_axis[0] > 0:
        l_brd = y_axis[0] - shift
    else:
        l_brd = y_axis[0] + shift

    if y_axis[1] > 0:
        r_brd = y_axis[1] - shift
    else:
        r_brd = y_axis[1] + shift



    ax.set_ylim( m.pow(10, pow_min), m.pow(10, pow_max) )
    
    if if_show == False:
        ax.set_yticklabels([])
#####################
def set_y_axis (ax, y_axis, if_show = True):
    ax.set_ylim(y_axis[0], y_axis[1])
    
    yticks_ps    = []
    yticks_lb    = []
    
    y = y_axis[0]
    
    while y < y_axis[1] + y_axis[2]:
        yticks_ps.append(y)
        
        yticks_lb.append(y)

        y = y + y_axis[2]
     
    yticks = []
 
    # Set precision of labels
    str_len = len( str(y_axis[2]) )
    
    my_prc = str_len
    
    if 0 < y_axis[2] and y_axis[2]  < 1.:
        my_prc = str_len - 2
    
    for el in yticks_lb:
        string = '$' + "{value:{width}.{precision}f}".format(value=el, width=0, precision=my_prc) + '$'
            
        yticks.append( string  )
        
    
    ax.set_yticks(yticks_ps)
    ax.set_yticklabels(yticks)

    shift = 1e-8

    if y_axis[0] > 0:
        l_brd = y_axis[0] - shift
    else:
        l_brd = y_axis[0] + shift

    if y_axis[1] > 0:
        r_brd = y_axis[1] - shift
    else:
        r_brd = y_axis[1] + shift


    ax.set_ylim(l_brd, r_brd)
    
    if if_show == False:
        ax.set_yticklabels([])
#####################
def set_logx_axis (ax, x_min, x_max, if_show = True):
    ax.set_xscale('log')
    
    xticks_ps    = [0.001,       0.01,        0.1,     0.2,    0.4,     0.8,     1]
    xticks_lb    = ['$10^{-3}$', '$10^{-2}$', '$0.1$', '$0.2$','$0.4$', '$0.8$', '$1$'] 
 
    xticks_ps_fn = []
    xticks_lb_fn = []
 
    for i,j  in zip(xticks_ps, xticks_lb):
        if x_min <= i and i <= x_max:
            xticks_ps_fn.append(i)
            xticks_lb_fn.append(j)
        
    ax.set_xticks(xticks_ps_fn)
    ax.set_xticklabels(xticks_lb_fn)

    ax.set_xlim( x_min, x_max )
    
    if if_show == False:
        ax.set_xticklabels([])
#####################
def set_x_axis (ax, x_axis, if_show = True):
    xticks_ps    = []
    xticks_lb    = []
    
    x = x_axis[0]
    
    while x <= x_axis[1]:
        xticks_ps.append(x)
        
        xticks_lb.append(x)

        x = x + x_axis[2]
    
    xticks = []
    for el in xticks_lb:
        string = '$' + "{:.1f}".format(el) + '$'
        xticks.append( string  )
    
    ax.set_xticks(xticks_ps)
    ax.set_xticklabels(xticks)

    ax.set_xlim(x_axis[0], x_axis[1])
    
    if if_show == False:
        ax.set_xticklabels([])
#######################################################################################
def axes_break (ax1, ax2, w_ratio21, h_ratio21 = 1, d = 0.01, lw = 0.8):
    kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False, linewidth=lw)

    #ax1.plot((x1, x2), (y1, y2), **kwargs)
    ax1.plot((1 - w_ratio21 * d, 1 + w_ratio21 * d), (-d / h_ratio21,    +d / h_ratio21), **kwargs)
    
    ax1.plot((1 - w_ratio21 * d, 1 + w_ratio21 * d), (1 - d / h_ratio21, 1 + d / h_ratio21), **kwargs)
    

    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes

    ax2.plot((-d, +d), (-d / h_ratio21,    +d / h_ratio21), **kwargs)
    
    ax2.plot((-d, +d), (1 - d / h_ratio21, 1 + d / h_ratio21), **kwargs)
    
    ax1.spines['right'].set_visible(False)
    ax2.spines['left'].set_visible(False)
#######################################################################################
def hide_ticks(ax):
    yticks = ax.yaxis.get_major_ticks()
    
    yticks[-1].set_visible(False)
    yticks[0].set_visible(False)
        
    xticks = ax.xaxis.get_major_ticks()
    xticks[-1].set_visible(False)
    #xticks[0].set_visible(False)
#######################################################################################

