# -*- ThePEG-repository -*-

##################################################
## Herwig/Matchbox example input file
##################################################

##################################################
## Collider type
##################################################
read snippets/Matchbox.in
read snippets/PPCollider.in

##################################################
## Beam energy sqrt(s)
##################################################

cd /Herwig/EventHandlers
set EventHandler:LuminosityFunction:Energy 13000*GeV

##################################################
## Process selection
##################################################

## Note that event generation may fail if no matching matrix element has
## been found.  Coupling orders are with respect to the Born process,
## i.e. NLO QCD does not require an additional power of alphas.

## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the order of the couplings
cd /Herwig/MatrixElements/Matchbox
set Factory:OrderInAlphaS 1
set Factory:OrderInAlphaEW 2

## Select the process
## You may use identifiers such as p, pbar, j, l, mu+, h0 etc.
do Factory:Process p p -> mu+ mu- j 

## Special settings required for on-shell production of unstable particles
## enable for on-shell top production
# read Matchbox/OnShellTopProduction.in
## enable for on-shell W, Z or h production
# read Matchbox/OnShellWProduction.in
# read Matchbox/OnShellZProduction.in
# read Matchbox/OnShellHProduction.in
# Special settings for the VBF approximation
# read Matchbox/VBFDiagramsOnly.in

##################################################
## Matrix element library selection
##################################################

## Select a generic tree/loop combination or a
## specialized NLO package

# read Matchbox/MadGraph-GoSam.in
# read Matchbox/MadGraph-MadGraph.in
# read Matchbox/MadGraph-NJet.in
# read Matchbox/MadGraph-OpenLoops.in
# read Matchbox/HJets.in
# read Matchbox/VBFNLO.in

## Uncomment this to use ggh effective couplings
## currently only supported by MadGraph-GoSam

# read Matchbox/HiggsEffective.in

##################################################
## Cut selection
## See the documentation for more options
##################################################
cd /Herwig/Cuts/
set ChargedLeptonPairMassCut:MinMass 70*GeV
set ChargedLeptonPairMassCut:MaxMass 110*GeV

## cuts on additional jets

read Matchbox/DefaultPPJets.in

set /Herwig/Cuts/JetFinder:Variant AntiKt
set /Herwig/Cuts/JetFinder:ConeRadius 0.4
set /Herwig/Cuts/FirstJet:PtMin 25.*GeV

insert JetCuts:JetRegions 0 FirstJet
# insert JetCuts:JetRegions 1 SecondJet
# insert JetCuts:JetRegions 2 ThirdJet
# insert JetCuts:JetRegions 3 FourthJet

##################################################
## Scale choice
## See the documentation for more options
##################################################

cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/LeptonPairPtScale

##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

#read Matchbox/MCatNLO-DefaultShower.in
# read Matchbox/Powheg-DefaultShower.in
## use for strict LO/NLO comparisons
read Matchbox/MCatLO-DefaultShower.in
## use for improved LO showering
# read Matchbox/LO-DefaultShower.in

# read Matchbox/MCatNLO-DipoleShower.in
# read Matchbox/Powheg-DipoleShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DipoleShower.in
## use for improved LO showering
# read Matchbox/LO-DipoleShower.in

# read Matchbox/NLO-NoShower.in
# read Matchbox/LO-NoShower.in

##################################################
## Scale uncertainties
##################################################

# read Matchbox/MuDown.in
# read Matchbox/MuUp.in

##################################################
## Shower scale uncertainties
##################################################

# read Matchbox/MuQDown.in
# read Matchbox/MuQUp.in

##################################################
## PDF choice
##################################################

read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
# read Matchbox/FiveFlavourNoBMassScheme.in
read Matchbox/CT14.in
# read Matchbox/MMHT2014.in

##################################################
### ShowerHandler(s)
###################################################

cd /Herwig/EventHandlers
#set EventHandler:CascadeHandler NULL           # No Shower
set EventHandler:CascadeHandler:MPIHandler NULL # No MPIs
set EventHandler:HadronizationHandler NULL      # No hadronization
set EventHandler:DecayHandler NULL              # No decays

##################################################
## Analyses
##################################################

# To avoind warnings about colored final state particles
cd /Herwig/Analysis
set Basics:CheckQuark No


cd /Herwig/Analysis
insert Rivet:Analyses 0 PAPER_FIN_R4
insert Rivet:Analyses 0 PAPER_FIN_R4_CH
insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 Rivet
# insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 HepMC

##################################################
## Save the generator
##################################################

do /Herwig/MatrixElements/Matchbox/Factory:ProductionMode

cd /Herwig/Generators
saverun LHC-Matchbox EventGenerator
