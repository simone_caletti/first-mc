#!/usr/bin/bash

mkdir merged

echo 'First merge central value simulations'

cd central/
yodamerge *.yoda* -o zj_ps_nlo_mer_cent_R4.yoda
mkdir ../merged/central
mv zj_ps_nlo_mer_cent_R4.yoda ../merged/central

echo 'Now ME + PS scale variation'

echo 'Scale_1'
cd ../env_mur0.5_muf0.5
yodamerge *.yoda* -o zj_ps_nlo_mer_mur0.5_muf0.5_R4.yoda
mkdir ../merged/scale_1
mv zj_ps_nlo_mer_mur0.5_muf0.5_R4.yoda ../merged/scale_1

echo 'Scale_2'
cd ../env_mur0.5_muf1
yodamerge *.yoda* -o zj_ps_nlo_mer_mur0.5_muf1_R4.yoda
mkdir ../merged/scale_2
mv zj_ps_nlo_mer_mur0.5_muf1_R4.yoda ../merged/scale_2

echo 'Scale_3'
cd ../env_mur1_muf0.5
yodamerge *.yoda* -o zj_ps_nlo_mer_mur1_muf0.5_R4.yoda
mkdir ../merged/scale_3
mv zj_ps_nlo_mer_mur1_muf0.5_R4.yoda ../merged/scale_3

echo 'Scale_4'
cd ../env_mur1_muf1
yodamerge *.yoda* -o zj_ps_nlo_mer_mur1_muf1_R4.yoda
mkdir ../merged/scale_4
mv zj_ps_nlo_mer_mur1_muf1_R4.yoda ../merged/scale_4

echo 'Scale_5'
cd ../env_mur1_muf2
yodamerge *.yoda* -o zj_ps_nlo_mer_mur1_muf2_R4.yoda
mkdir ../merged/scale_5
mv zj_ps_nlo_mer_mur1_muf2_R4.yoda ../merged/scale_5

echo 'Scale_6'
cd ../env_mur2_muf1
yodamerge *.yoda* -o zj_ps_nlo_mer_mur2_muf1_R4.yoda
mkdir ../merged/scale_6
mv zj_ps_nlo_mer_mur2_muf1_R4.yoda ../merged/scale_6

echo 'Scale_7'
cd ../env_mur2_muf2
yodamerge *.yoda* -o zj_ps_nlo_mer_mur2_muf2_R4.yoda
mkdir ../merged/scale_7
mv zj_ps_nlo_mer_mur2_muf2_R4.yoda ../merged/scale_7


