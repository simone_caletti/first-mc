#!/usr/bin/python2.7

import os

mur = [0.5, 0.5, 1,   1, 1, 2, 2]
muf = [0.5, 1,   0.5, 1, 2, 1, 2]

# Make corresponding folders

if not os.path.exists("central"):
    os.mkdir("central")

for j, k in zip(mur, muf):
    d_name = "env_mur{}_muf{}".format(str(j), str(k))
    
    if not os.path.exists(d_name):
        os.mkdir(d_name)

# Move files there
for i in range(1, 1001):
    print 'RUN #' + str(i)
    
    os.system('mv analysis_{}.yoda central/'.format(str(i)))
    
    for j, k in zip(mur, muf):
        d_name = "env_mur{}_muf{}".format(str(j), str(k))
        
        if j == 1 and k == 1:
            f_name='analysis_{}.MUR{}_MUF{}_PDF261000.yoda'.format(str(i), str(j), str(k))
        else:
            f_name='analysis_{}.MUR{}_MUF{}_PDF261000_PSMUR{}_PSMUF{}.yoda'.format(str(i), str(j), str(k), str(j), str(k))
         
        os.system("mv " + f_name + ' ' + d_name + '/')
        


