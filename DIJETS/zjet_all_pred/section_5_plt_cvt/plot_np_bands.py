#! /usr/bin/env python3
# My libs
import sys

sys.path.append('../my_libs_cvt')

from cosmetics import *
from my_drawers import * 
from my_plot import plot_mc, plot_band_np_vs_ps

# Standard libs
import numpy as np
import math as m

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

# Choose a LaTeX font
plt.rc('text',       usetex     = True)
plt.rc('text.latex', preamble   = r"\usepackage{amsmath}")
plt.rc('font',       weight='bold')

# Main sizes
plt.rc('font',   size       = 70)          
plt.rc('axes',   labelsize  = 70)     
plt.rc('axes',   titlesize  = 70)     
plt.rc('axes',   labelpad   = 20)     
plt.rc('hatch',  linewidth  = 0.8)

# Lines: somehow does not work for histos
plt.rc('lines', linewidth = 6)

# Legend
plt.rc('legend', fontsize   = 46) 
plt.rc('legend', framealpha = None)

# Set ticks
plt.rc('ytick.major',   width     = 3) 
plt.rc('ytick.major',   size      = 30) 
plt.rc('ytick.major',   pad       = 10)
plt.rc('ytick.major',   pad       = 15) 

plt.rc('ytick.minor',   size      = 15) 
plt.rc('ytick',         direction = 'in') 

plt.rc('xtick.major',   width     = 3) 
plt.rc('xtick.major',   size      = 30) 
plt.rc('xtick.major',   pad       = 15) 
plt.rc('xtick.major',   size      = 30) 

plt.rc('xtick.minor',   size      = 15) 
plt.rc('xtick',         direction = 'in') 

# Grids
plt.rc('axes', grid = True)
plt.rc('axes.grid', which='major')
plt.rc('grid', linestyle='dotted')
plt.rc('grid', linewidth=0.5)

#######################################################################################
### This one is in charge for custom legend labels
### Needed to switch from "boxes" to lines in histograms
### and to added shaded areas around lines 
### x and y set the legend location
#######################################################################################
def set_custom_legend(ax, col1, col2, x, y, alpha_val = 0.3):
    patches, labels = ax.get_legend_handles_labels()

    # Remove old pathches and labels
    patches = []
    labels = []

    # Define your own patches and set labels
    ###
    mRect = mpatches.Patch(facecolor=col1, alpha=alpha_val, edgecolor=None)       
 
    mLine = mlines([0],[0], color=col1)
 
    patches.append((mRect,mLine))
    
    labels.append(r'$\mathrm{HL \, (all \, hadrons) \,/\, PL}$')
    
    ###
    hRect = mpatches.Patch(facecolor='white', hatch="X", alpha=0.3, edgecolor=col2)       
 
    hLine = mlines([0],[0], color=col2)
    
    patches.append((hRect,hLine))
    
    labels.append(r'$\mathrm{HL \, (charged \, hadrons \, only) \,/\, PL}$')
    
    ax.legend(patches,  labels, loc = (x, y), borderaxespad=2.)
#######################################################################################
def plot_hist(ax, col, if_hatch, pTL, pTR, R, alpha,  if_gr = False, if_ch_tr = True):
    # Choose the write folder (I have three different folders depending on the pT slice I use)
    # The first one  for the pT interval pT in [50, 150)
    # The second one for the pT interval pT in [150, 408)
    # And the last one for the pT interval pT in [408, 1500]
    if pTL < 150:
        pt_sl = '_pT50'
    elif pTL >= 150 and pTL < 408:
        pt_sl = '_pT150'
    else:
        pt_sl = '_pT408'
 
    # Set path
    # PYTHIA 
    p_path_ps = '../pythia_data_ps_cvt/pythia_ps_R{}'.format( str(R) ) + pt_sl + '/'
    p_path_np = '../pythia_data_np_cvt/pythia_np_R{}'.format( str(R) ) + pt_sl + '/'
 
    # HERWIG
    h_path_ps = '../herwig_data_ps_cvt/herwig_ps_R{}'.format( str(R) ) + pt_sl + '/'
    h_path_np = '../herwig_data_np_cvt/herwig_np_R{}'.format( str(R) ) + pt_sl + '/'
 
    # SHERPA
    s_path_ps = '../sherpa_data_ps_cvt/sherpa_ps_R{}'.format( str(R) ) + pt_sl + '/'
    s_path_np = '../sherpa_data_np_cvt/sherpa_np_R{}'.format( str(R) ) + pt_sl + '/'

    # def plot_mc(ax, path, col, ls, R, alpha, pTL, pTR, bin_choice, if_scl_var, if_SD, if_show = True, if_hatch = False):
    # bin_choice = 'all', 'chr', 'chr_w_all_in'


    # Get ps-level bin_edges, x-section and d\sigma/d\lambda distributions
    # Pythia, Herwig, Sherpa                                                           
    edges_ps, p_sig_ps, p_hst_ps = plot_mc(ax, p_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    edges_ps, h_sig_ps, h_hst_ps = plot_mc(ax, h_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    edges_ps, s_sig_ps, s_hst_ps = plot_mc(ax, s_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    
    if if_ch_tr:
        edges_np, p_sig_np, p_hst_np = plot_mc(ax, p_path_np, '', '-', R, alpha, pTL, pTR, 'chr', False, if_gr, False)
        edges_np, h_sig_np, h_hst_np = plot_mc(ax, h_path_np, '', '-', R, alpha, pTL, pTR, 'chr', False, if_gr, False)
        edges_np, s_sig_np, s_hst_np = plot_mc(ax, s_path_np, '', '-', R, alpha, pTL, pTR, 'chr', False, if_gr, False)
    else:
        edges_np, p_sig_np, p_hst_np = plot_mc(ax, p_path_np, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
        edges_np, h_sig_np, h_hst_np = plot_mc(ax, h_path_np, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
        edges_np, s_sig_np, s_hst_np = plot_mc(ax, s_path_np, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
 
    # Uncertainty band for NP effects
    plot_band_np_vs_ps(ax, col, '', '-', edges_ps, 
                       p_hst_np, h_hst_np, s_hst_np, 
                       p_hst_ps, h_hst_ps, s_hst_ps, True, if_hatch)
#####################
def plot_all(col1, col2, R, pTL, pTR, alpha, if_SD, y_min, y_max, y_step, X, Y, X1, Y1):
    plt.subplots(1, 1, sharex=True, figsize=(30., 15.), dpi=100)

    # make outer gridspec
    # define relative widths
    w1 = 1.     # width of the first bin
    w12 = 0.25  # width of the empty ('broken') space between the first bin and the rest
    w2 = 20.    # width of the rest of the plot (these are the main bins)
    
    # needed to set the slope of the slanted lines
    ratio_w21 = w2 / w1

    all_widths = [w1, w12, w2] 
    
    # Main grid (canvas)
    # Adjust left, bottom, right and top parameters to remove unwated white spaces
    outer = gridspec.GridSpec(1, 3,  width_ratios  = all_widths, 
                                     wspace=0., hspace=0.,
                                     left=0.13, bottom=0.2, right=0.99, top=0.9)

    # make nested gridspecs
    # first row (hist)
    # array of axes for the first row; ax1[0] -> first bin, ax1[1] -> the rest of the bins
    ax1 = []    
    
    # Loop  over the main grid and fill in arrays of axes
    # we skip outer[1] to create line breaks!
    # first row
    for i in [0, 2]:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[i])

        ax1.append( plt.subplot(gs[0,0]) )

    # Set axis breaks
    axes_break(ax1[0], ax1[1], ratio_w21)

    # Set grids
    for el in ax1:
        el.plot([0,1], [1,1], linewidth = 4, linestyle='-', color = 'black')

    # Set x-axes
    # Ranges have to be adjusted manually
    # First bins
    x_ax = []
    
    if alpha == 0.5:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 1.0:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 2.0:
        x_ax = [1e-4, 1e-3, 1]
    
    set_x_axis (ax1[0],  x_ax, False)
        
    # Other bins (the main part of the plot)
    # use, for example, delta = 5.001e-2 instead of  5e-2  to avoid an overlap between slanted lines and minor ticks on x-axis
    if alpha == 0.5:
        set_logx_axis (ax1[1],  5.001 * x_ax[1], 1)
    elif alpha == 1.0:
        set_logx_axis (ax1[1],  1.001 * x_ax[1], 1)
    elif alpha == 2.0:
        set_logx_axis (ax1[1],  2.001 * x_ax[1], 1)
    
    # Set y-axes
    # Set range and step for ticks on y-axes (for histo plots)
    y_ax_h = [y_min, y_max, y_step]
    
    # First bin 
    set_y_axis(ax1[0], y_ax_h)
    
    # Other bins 
    set_y_axis(ax1[1], y_ax_h, False)

    # Set font size 
    # Needed to make some labels a bit bigger
    # Has to be adjusted manually
    f_size=80

    # Name y-axes
    ax1[0].set_ylabel (r'$\frac{d\sigma^{\mathrm{HL}}}{d\lambda} \,/\, \frac{d\sigma^{\mathrm{PL}}}{d\lambda}$', fontsize=f_size)

    # Set lables for the lower x-axis
    if alpha == 0.5:
        ax1[1].set_xlabel  ('$\lambda^1_{1/2} \,\, [\mathrm{LHA}]$',  fontsize=f_size)
    elif alpha == 1.0:
        ax1[1].set_xlabel  ('$\lambda^1_{1} \,\, [\mathrm{Width}]$',  fontsize=f_size)
    elif alpha == 2.0:
        ax1[1].set_xlabel  ('$\lambda^1_{2} \,\, [\mathrm{Thrust}]$', fontsize=f_size)

    # Hide unwated ticks for x- and y-axes
    for el in ax1:
        hide_ticks(el)
    
    # Actuall plotting starts here
    # All FS-hadrons
    plot_hist(ax1[0], col1, False, pTL, pTR, R, alpha, if_SD, False)
    plot_hist(ax1[1], col1, False, pTL, pTR, R, alpha, if_SD, False)


    # Charged FS-hadrons only
    plot_hist(ax1[0], col2, True, pTL, pTR, R, alpha, if_SD, True)
    plot_hist(ax1[1], col2, True, pTL, pTR, R, alpha, if_SD, True)

    # Set Legend
    set_custom_legend(ax1[1], col1, col2, X, Y)
    
    # Set annotation
    if if_SD:
        if R == 8:
            ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.8$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=f_size,
                            bbox=dict(boxstyle="round", fc="1"))
        elif R == 4:
            ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.4$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=f_size,
                            bbox=dict(boxstyle="round", fc="1"))
    else:
        if R == 8:
            ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.8$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=f_size,
                            bbox=dict(boxstyle="round", fc="1"))
        elif R == 4:
            ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.4$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=f_size,
                            bbox=dict(boxstyle="round", fc="1"))
            
            
            
    # Add some text on top of each column
    tit = r' $p_{} \in [{}, {}]$'.format("{T, \, \\rm jet}", str(pTL), str(pTR))  + r' $\mathrm{GeV}$'
    
    ax_tw = ax1[1].twiny()
    ax_tw.set_xticks([])
    ax_tw.set_xlabel (tit, fontsize=f_size, labelpad=40)

    # Hide unwanted axes
    ax_tw.spines['left'].set_visible(False)
    ax_tw.spines['top'].set_visible(False)
    
    # Hide unwanted ticks 
    ax1[1].tick_params(axis = "y", which = "both", left = False)

    # Save your nice plot!
    if if_SD:
        plt.savefig('NP_BAND_lin_ch_ZJ_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '_SD.pdf')
    else:
        plt.savefig('NP_BAND_lin_ch_ZJ_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '.pdf')
###########################################################################
#pT_bins = [50, 65, 88, 120, 150, 186, 254, 326, 408, 1500]

#pT_bins = [120, 150]
pT_bins = [408, 1500]

R = 8


# Loop ver pT bins. Make plots
i = 0
while i < len(pT_bins) - 1:
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
 
    print ('pT in [', str(pTL), ', ', str(pTR), '] GeV')
 
    '''
    # Adjusted for pT[120, 150] interval
    # Ungroomed
    #                                                y_min y_max y_step   x     y    x1    y1
    plot_all('red', 'blue', R, pTL, pTR, 0.5, False, 0,    5.0,  0.50,   0.00, 0.70, 0.50, 0.70)
    plot_all('red', 'blue', R, pTL, pTR, 1.0, False, 0,    2.5,  0.25,   0.00, 0.70, 0.50, 0.15)
    plot_all('red', 'blue', R, pTL, pTR, 2.0, False, 0.,   2.5,  0.25,   0.00, 0.70, 0.50, 0.15)

    # Groomed
    #                                               y_min  y_max y_step   x     y     x1    y1
    plot_all('red', 'blue', R, pTL, pTR, 0.5, True, 0,     5.00,  0.50,   0.00, 0.70, 0.50, 0.7)
    plot_all('red', 'blue', R, pTL, pTR, 1.0, True, 0.25,  2.25,  0.25,   0.20, 0.10, 0.50, 0.7)
    plot_all('red', 'blue', R, pTL, pTR, 2.0, True, 0.75,  2.00,  0.25,   0.00, 0.70, 0.50, 0.7)
    '''
    
    
    # Adjusted for pT[408, 1500] interval
    # Ungroomed
    #                                                y_min y_max y_step   x     y     x1    y1
    plot_all('red', 'blue', R, pTL, pTR, 0.5, False, 0,    2.5,  0.5,   0.05, 0.70, 0.5, 0.15)
    plot_all('red', 'blue', R, pTL, pTR, 1.0, False, 0.25, 1.5,  0.25,  0.20, 0.05, 0.7, 0.15)
    plot_all('red', 'blue', R, pTL, pTR, 2.0, False, 0.25, 1.75, 0.25,  0.20, 0.05, 0.7, 0.15)

    # Groomed
    plot_all('red', 'blue', R, pTL, pTR, 0.5, True, 0,    2.5,  0.5,   0.3, 0.70, 0.50, 0.15)
    plot_all('red', 'blue', R, pTL, pTR, 1.0, True, 0.85, 1.30, 0.05,  0.08, 0.05, 0.50, 0.70)
    plot_all('red', 'blue', R, pTL, pTR, 2.0, True, 0.85, 1.20, 0.05,  0.05, 0.70, 0.70, 0.15)
 
    i += 1



