#!/usr/bin/env python3

import sys
import numpy as np

###########################################
## This one is used to read *.dat* files
## containing yoda histograms
###########################################
def yoda_get_bins (path):
    plot = open(path)

    if_begin = False
    
    if_end = False
    
    x_low  = []
    x_high = []
    
    err_minus = []
    err_plus  = []
    
    bin_edges = []
    bin_entry = []
    
    for line in plot:
        words = line.split()
 
        if len(words) == 6:
            if words[0] == '#' and words[1] == 'xlow' and words[2] == 'xhigh':
                # xlow	 xhigh	 val	 errminus	 errplus
                if_begin = True
                
                if_end = False
                
        if len(words) == 3:
            if words[0] == '#' and words[1] == 'END' and words[2] == 'HISTO1D':
                if_end = True
            
                if_begin = False
                
                break

        if len(words) == 5 and if_begin == True and if_end == False:
            x_low.append( float(words[0]) )
            
            x_high.append( float(words[1]) )
                
            bin_entry.append( float(words[2]) )
            
            err_minus.append( float(words[3]) )
            
            err_plus.append( float(words[4]) )
    
    for el in x_low:
        bin_edges.append(el)
    
    bin_edges.append( x_high[-1] )
    
    bin_center = []
    
    for i, el in enumerate(bin_edges[:-1]):
        bin_center.append( 0.5 * (bin_edges[i + 1] + bin_edges[i]) )
    
    return bin_edges, bin_entry
###########################################
f_name = str(sys.argv[1])
  
print("Integrating over:", f_name)

edges, hist = yoda_get_bins(f_name)


print ('-------------------------------')
print ('Integral over histogram = ', np.sum(hist * np.diff(edges)) )
print ('-------------------------------')
