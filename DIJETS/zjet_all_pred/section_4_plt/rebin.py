#!/usr/bin/python3.8

from matplotlib import pyplot as plt

import matplotlib as mpt
######################################################################
def rebin_hist(edges, hist, rebin_f):
    re_bin_edges = []
    re_bin_entry = []
    
    if rebin_f == 1:
        re_bin_edges = edges
        re_bin_entry = hist
    elif  len(hist) % rebin_f == 0:
        # Get new bin edges
        i = 0
        while rebin_f * i <= len(edges):
            re_bin_edges.append(edges[rebin_f * i])
            
            i += 1
 
        # Get new bin entries
        for j in range(0, len(hist) // rebin_f):
            print ('j = ', j, ' [', j * rebin_f, (j+1) * rebin_f - 1, ']') 
            
            sum_h = sum(hist[j*rebin_f:(j+1)*rebin_f])
            print (sum_h)
            
            re_bin_entry.append(sum_h)
    else:
        print ('You use wrong rebinning factor!')
        
    return re_bin_edges, re_bin_entry
######################################################################
######################################################################
######################################################################
def plot_hist(ax, bin_edges, hist, col):
    bin_center = []
    
    for i, el in enumerate(bin_edges[:-1]):
        bin_center.append( 0.5 * (bin_edges[i + 1] + bin_edges[i]) )
    
    ax.hist(bin_center, bins=bin_edges, weights=hist, color=col, histtype= 'step')
######################################################################
f, ax = plt.subplots(1, 1, sharex=True, figsize=(30., 20), dpi=100)

bin_edges = [0, 1, 2, 3, 4, 5, 6]
bin_entry = [1, 2, 3, 4, 5, 6]

plot_hist(ax, bin_edges, bin_entry, 'red')

print ('Old bin edges are')
print (bin_edges)

print ('Old hist is')
print (bin_entry)

edges_new, hist_new = rebin_hist(bin_edges, bin_entry, 3)

plot_hist(ax, edges_new, hist_new, 'blue')


print ('New bin edges are')
print (edges_new)

print ('New hist is')
print (hist_new)


plt.show()



