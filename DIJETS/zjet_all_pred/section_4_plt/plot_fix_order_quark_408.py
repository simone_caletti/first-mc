#!/usr/bin/python3.8

import numpy as np
import math as m
# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import matplotlib as mpt

# Choose a LaTeX font
plt.rc('text',       usetex     = True)
plt.rc('text.latex', preamble   = r"\usepackage{amsmath}")
plt.rc('legend',     framealpha = None)

# Main sizes
plt.rc('font',   size       = 90)          
plt.rc('axes',   labelsize  = 100)     
plt.rc('axes',   titlesize  = 70)     
plt.rc('axes',   labelpad   = 30)     
plt.rc('legend', fontsize   = 60) 
plt.rc('hatch',  linewidth  = 2.0)

# Set ticks
plt.rc('ytick.major',   width     = 3) 
plt.rc('ytick.major',   size      = 30) 
plt.rc('ytick.major',   pad       = 10) 
plt.rc('ytick.minor',   visible   = False) 
plt.rc('ytick',         direction = 'in') 

plt.rc('xtick.major',   width     = 3) 
plt.rc('xtick.major',   size      = 30) 
plt.rc('xtick.major',   pad       = 20) 
plt.rc('xtick.minor',   size      = 15) 
plt.rc('xtick',         direction = 'in') 

# Lines: somehow does not work for histos
plt.rc('lines', linewidth = 8)

from my_fo_fnc import set_plot, daniel_make_plot
#######################################################################################
def set_custom_legend(ax, x, y):
    patches, labels = ax.get_legend_handles_labels()

    # Remove old pathches and labels
    patches = []
    labels = []

    # Define your own patches and set labels
    ###
    gLine = mlines([0],[0], color='red', linestyle='-')
    
    patches.append(gLine)
    
    labels.append('$\mathrm{LO}$')
 
    ###
    gLine = mlines([0],[0], color='green', linestyle='-')
    
    patches.append(gLine)
    
    labels.append('$\mathrm{NLO}$')
 
    ###
    bLine = mlines([0],[0], color='blue', linestyle='-')
    
    patches.append(bLine)
    
    labels.append('$\mathrm{\exp \,\, LO}$')
 
    ###
    oLine = mlines([0],[0], color='orange', linestyle='-')
    
    patches.append(oLine)
    
    labels.append('$\mathrm{exp \,\, NLO}$')
 
    ###
    pLine = mlines([0],[0], color='purple', linestyle='-')
    
    patches.append(pLine)
    
    labels.append('$\mathrm{exp \,\, NLO+C}$')
    
    ax.legend(patches,  labels, loc = (x, y), borderaxespad=2.)
#######################################################################################
def get_name (alpha):
    res = ''
    
    if alpha == 0.5:
        res = '05'
    elif alpha == 1.0:
        res = '10'
    elif alpha == 2.0:
        res = '20'
    
    return res
#################################################################################
# Master plot
def plot_all(channel, bin_f, R, alpha, pTL, pTR,  if_SD, x_ax, y_ax, r_ax, X1, Y1, X2, Y2):
    plt.subplots(1, 1, sharex=True, figsize=(30., 30), dpi=100)
    
    plt.subplots_adjust(hspace=0., wspace=0.)

    # Main grid (canvas)
    outer = gridspec.GridSpec(1, 1, wspace=0., hspace=0., left=0.18, bottom=0.12, right=0.98, top=0.94) 
    
    # make nested gridspecs
    axes = []
    
    gs = gridspec.GridSpecFromSubplotSpec(2, 1, height_ratios = [2, 1], subplot_spec = outer[0])

    axes.append( plt.subplot(gs[0,0]) )
    axes.append( plt.subplot(gs[1,0]) )

    
    # Set axes
    # Ranges have to be adjusted manually
    #####
    # LHA
    set_plot(axes[0], axes[1], pTL, pTR, alpha, x_ax, y_ax, r_ax)

    # Actuall plotting starts here
    daniel_make_plot(axes[0], axes[1], channel, alpha, bin_f, R, pTL, pTR, if_SD)

    axes[0].legend(loc = (X1, Y1), borderaxespad=2.)

    set_custom_legend(axes[0], X1, Y1)

    if channel == 'Gluon':
        if if_SD:
            axes[0].annotate('$\mathrm{Gluon \,\, channel}$\n' + 
                             '$\mathrm{Groomed}$',
                              xy=(X2, Y2), xycoords='axes fraction',
                              size=85,
                              bbox=dict(boxstyle="round", fc="1"))
        else:
            axes[0].annotate('$\mathrm{Gluon \,\, channel}$\n' + 
                             '$\mathrm{Ungroomed}$',
                              xy=(X2, Y2), xycoords='axes fraction',
                              size=85,
                              bbox=dict(boxstyle="round", fc="1"))
        
    if channel == 'Quark':
        if if_SD:
            axes[0].annotate('$\mathrm{Quark \,\, channel}$\n' + 
                             '$\mathrm{Groomed}$',
                              xy=(X2, Y2), xycoords='axes fraction',
                              size=85,
                              bbox=dict(boxstyle="round", fc="1"))
        else:
            axes[0].annotate('$\mathrm{Quark \,\, channel}$\n' + 
                             '$\mathrm{Ungroomed}$',
                              xy=(X2, Y2), xycoords='axes fraction',
                              size=85,
                              bbox=dict(boxstyle="round", fc="1"))
    
    # Save your nice plot!
    sd_prf = ''
    
    if if_SD:
        sd_prf = '_SD'
    
    if channel == 'Quark':
        plt.savefig('FO_Quark_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + sd_prf +  '.pdf')
    elif channel == 'Gluon':
        plt.savefig('FO_Gluon_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + sd_prf +  '.pdf')
    else:
        print ('you have chosen a wrong channel!')
###########################
#pT_bins = [50, 65, 88, 120, 150, 186, 254, 326, 408, 1500]
#pT_bins = [120, 150]
pT_bins = [408, 1500]

# Set binning factor
bin_f = 1

# Channel
chan = 'Quark'

# jet radius
R = 8

# Loop ver pT bins. Make plots
i = 0
while i < len(pT_bins) - 1:
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
 
    print ('pT in [', str(pTL), ', ', str(pTR), '] GeV')

    # alpha  0.5
    #      #x_min,       x_max
    x_ax = [m.pow(10, -1.33),    1]
    y_ax = [0,       2,     0.25]
    r_ax = [-0.20,   0.50,  0.10]
     
    plot_all(chan, bin_f, R, 0.5, pTL, pTR, False, x_ax, y_ax, r_ax, 0.64, 0.60, 0.3, 0.8)
    
    
    y_ax = [0,      1.6,   0.25]
    r_ax = [-0.3,   0.45,  0.15]

    plot_all(chan, bin_f, R, 0.5, pTL, pTR, True,  x_ax, y_ax, r_ax, 0.66, 0.60, 0.34, 0.84)

    
    # alpha 1.0
    x_ax = [m.pow(10, -2.5),   1]
    y_ax = [0,      2,     0.25]
    r_ax = [-0.25,  0.75,   0.25]
    
    plot_all(chan, bin_f, R, 1.0, pTL, pTR, False, x_ax, y_ax, r_ax, 0.63, 0.60, 0.3, 0.84)
    
    
    y_ax = [0,    1.25, 0.25]
    r_ax = [-0.2, 0.3,  0.1]
    
    plot_all(chan, bin_f, R, 1.0, pTL, pTR, True,  x_ax, y_ax, r_ax, 0.05, 0.60, 0.6, 0.7)

    
    # alpha 2.0
    x_ax = [m.pow(10, -4.76),  1]

    y_ax = [-1,   1.5,   0.25]
    r_ax = [-0.5, 0.75,  0.25]

    plot_all(chan, bin_f, R, 2.0, pTL, pTR, False, x_ax, y_ax, r_ax, 0.25, 0.05, 0.65, 0.15)
    
    y_ax = [0,   0.6,  0.1]
    r_ax = [-0.15, 0.15, 0.05]
    
    plot_all(chan, bin_f, R, 2.0, pTL, pTR, True,  x_ax, y_ax, r_ax, 0.05, 0.60, 0.45, 0.75)
    
    
    i += 1
