#!/usr/bin/python3.8

import numpy as np
import math as m
# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import matplotlib as mpt

# Choose a LaTeX font
plt.rc('text',       usetex     = True)
plt.rc('text.latex', preamble   = r"\usepackage{amsmath}")
plt.rc('legend',     framealpha = None)

# Main sizes
plt.rc('font',   size       = 90)          
plt.rc('axes',   labelsize  = 100)     
plt.rc('axes',   titlesize  = 70)     
plt.rc('axes',   labelpad   = 30)     
plt.rc('legend', fontsize   = 75) 
plt.rc('hatch',  linewidth  = 2.0)

# Set ticks
plt.rc('ytick.major',   width     = 3) 
plt.rc('ytick.major',   size      = 30) 
plt.rc('ytick.major',   pad       = 10) 
plt.rc('ytick.minor',   visible   = False) 
plt.rc('ytick',         direction = 'in') 

plt.rc('xtick.major',   width     = 3) 
plt.rc('xtick.major',   size      = 30) 
plt.rc('xtick.major',   pad       = 20) 
plt.rc('xtick.minor',   size      = 15) 
plt.rc('xtick',         direction = 'in') 

# Lines: somehow does not work for histos
plt.rc('lines', linewidth = 8)


from my_fo_fnc import set_plot_match,  daniel_make_plot_match
#######################################################################################
### This one is in charge for custom legend labels
### Needed to switch from "boxes" to lines in histograms
### and to added shaded areas around lines 
### x and y set the legend location
#######################################################################################
def set_custom_legend(ax, x, y, alpha_val = 0.3):
    patches, labels = ax.get_legend_handles_labels()

    # Remove old pathches and labels
    patches = []
    labels = []

    # Define your own patches and set labels
    ###
    gLine = mlines([0],[0], color='green', linestyle='-')
    
    patches.append(gLine)
    
    labels.append('$\mathrm{NLO}$')
 
    ###
    bLine = mlines([0],[0], color='black', linestyle='-')
    
    patches.append(bLine)
    
    labels.append('$\mathrm{NLO+NLL}^\prime$')
 
    
    ###
    rRect = mpatches.Patch(facecolor='red', alpha=alpha_val, edgecolor=None)       
 
    patches.append(rRect)
    
    labels.append('$\mathrm{Gluon}$')
    
    
    ###
    bRect = mpatches.Patch(facecolor='blue', alpha=alpha_val, edgecolor=None)       
 
    patches.append(bRect)
    
    labels.append('$\mathrm{Quark}$')
    
    ax.legend(patches,  labels, loc = (x, y), borderaxespad=2.)
#######################################################################################
def get_name (alpha):
    res = ''
    
    if alpha == 0.5:
        res = '05'
    elif alpha == 1.0:
        res = '10'
    elif alpha == 2.0:
        res = '20'
    
    return res
#################################################################################
# Master plot
def plot_all(R, bin_f, pTL, pTR,  alpha, if_SD, x_ax, y_ax, X1, Y1, X2, Y2):
    plt.subplots(1, 1, sharex=True, figsize=(30., 30), dpi=100)
    
    # Main grid (canvas)
    outer = gridspec.GridSpec(1, 1, left=0.15, bottom=0.12, right=0.98, top=0.94) 

    # make nested gridspecs
    axes = []
    
    gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[0])

    axes.append( plt.subplot(gs[0,0]) )
    
    # Set axes
    # Ranges have to be adjusted manually
    #####
    set_plot_match(axes[0], pTL, pTR, alpha, x_ax, y_ax)

    # Actuall plotting starts here
    daniel_make_plot_match(axes[0], alpha,  bin_f, R, pTL, pTR, if_SD)
 
    set_custom_legend(axes[0], X1, Y1)

    if if_SD:
        axes[0].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.8$',
                            xy=(X2, Y2), xycoords='axes fraction',
                            size=85,
                            bbox=dict(boxstyle="round", fc="1"))
    else:
        axes[0].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.8$',
                            xy=(X2, Y2), xycoords='axes fraction',
                            size=85, 
                            bbox=dict(boxstyle="round", fc="1"))
 
 
    # Save your nice plot!
    if if_SD:
        plt.savefig('Matched_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '_SD.pdf')
    else:
        plt.savefig('Matched_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '.pdf')
###########################
###########################
#pT_bins = [120, 150]
pT_bins = [408, 1500]

R = 8

# Rebinning factor
bin_f = 4

# Loop ver pT bins. Make plots
i = 0
while i < len(pT_bins) - 1:
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
 
    print ('pT in [', str(pTL), ', ', str(pTR), '] GeV')
    
    '''
    # Adjusted for pT[120, 150] bin
    plot_all(R, bin_f, pTL, pTR, 0.5, False, [m.pow(10, -1.6), 1], [0, 2,   0.2], 0.55, 0.70, 0.70, 0.55)
    plot_all(R, bin_f, pTL, pTR, 1.0, False, [m.pow(10, -2.5), 1], [0, 1.2, 0.2], 0.60, 0.70, 0.70, 0.55)
    plot_all(R, bin_f, pTL, pTR, 2.0, False, [m.pow(10, -5.0), 1], [0, 1.2, 0.2], 0.60, 0.70, 0.70, 0.58)

    plot_all(R, bin_f, pTL, pTR, 0.5, True,  [m.pow(10,-1.6), 1], [0, 1.6, 0.2], 0.60, 0.70, 0.70, 0.55)
    plot_all(R, bin_f, pTL, pTR, 1.0, True,  [m.pow(10,-2.5), 1], [0, 1.0, 0.2], 0.60, 0.70, 0.70, 0.60)
    plot_all(R, bin_f, pTL, pTR, 2.0, True,  [m.pow(10,-5.0), 1], [0, 0.5, 0.1], 0.05, 0.70, 0.15, 0.60)
    '''
    
    
    # Adjusted for pT[408, 1500] bin
    plot_all(R, bin_f, pTL, pTR, 0.5, False, [m.pow(10, -1.6), 1], [0, 1.4, 0.2], 0.6, 0.7, 0.7, 0.6)
    plot_all(R, bin_f, pTL, pTR, 1.0, False, [m.pow(10, -2.5), 1], [0, 1.0, 0.2], 0.6, 0.7, 0.7, 0.6)
    plot_all(R, bin_f, pTL, pTR, 2.0, False, [m.pow(10, -5.),  1], [0, 1.0, 0.2], 0.6, 0.7, 0.7, 0.6)

    plot_all(R, bin_f, pTL, pTR, 0.5, True,  [m.pow(10, -1.6), 1], [0, 1.4, 0.2],  0.6,  0.7, 0.7,  0.6)
    plot_all(R, bin_f, pTL, pTR, 1.0, True,  [m.pow(10, -2.5), 1], [0, 0.8, 0.1],  0.05, 0.7, 0.15, 0.6)
    plot_all(R, bin_f, pTL, pTR, 2.0, True,  [m.pow(10, -5),   1],   [0, 0.4, 0.05], 0.05, 0.7, 0.15, 0.6)
    
        
    i += 1
