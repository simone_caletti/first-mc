#!/usr/bin/python2.7

import os
import sys

n_runs = int(sys.argv[1])

n = 1
while n <= n_runs:
    fileName = "run_" + str(n) + ".sh"

    fileObj = open(fileName, "a")

    fileObj.write("#!/usr/bin/bash\n")

    fileObj.write("../bin/Sherpa -R " + str(n) + " -A analysis_" + str(n) + "\n")

    fileObj.close()
    
    os.system("chmod +x " + fileName)

    n = n + 1

  


