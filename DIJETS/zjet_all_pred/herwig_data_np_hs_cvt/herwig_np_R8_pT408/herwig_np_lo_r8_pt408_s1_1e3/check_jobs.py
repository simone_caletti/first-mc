#!/usr/bin/python2.7
import os

def get_n_files (n_of_dir, outFile):
    path, dirs, files = next(os.walk(n_of_dir))

    f_count = len(files)

    info  = n_of_dir + '	Number of Files = ' + str(f_count) + '\n'

    print info

    outFile.write(info)

    return n_of_dir
#####################
list_of_names = ['plots']

outFile  = open("n_jobs_log.dat", "w+") 

for el in list_of_names:
    get_n_files (el, outFile)

outFile.close()
