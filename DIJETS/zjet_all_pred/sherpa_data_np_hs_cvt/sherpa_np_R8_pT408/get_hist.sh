#!/bin/bash

echo 'Getting .dat files'

echo 'central'
cd central/
rivet-cmphistos central.yoda
cd ../

echo 'scale_1'
cd scale_1/
rivet-cmphistos scale_1.yoda
cd ../

echo 'scale_2'
cd scale_2/
rivet-cmphistos scale_2.yoda
cd ../

echo 'scale_3'
cd scale_3/
rivet-cmphistos scale_3.yoda
cd ../

echo 'scale_4'
cd scale_4/
rivet-cmphistos scale_4.yoda
cd ../

echo 'scale_5'
cd scale_5/
rivet-cmphistos scale_5.yoda
cd ../

echo 'scale_6'
cd scale_6/
rivet-cmphistos scale_6.yoda
cd ../

echo 'scale_7'
cd scale_7/
rivet-cmphistos scale_7.yoda
cd ../

echo 'done!'






