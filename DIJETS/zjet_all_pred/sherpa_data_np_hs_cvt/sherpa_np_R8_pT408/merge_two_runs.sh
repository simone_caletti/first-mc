#/bin/bash
############################################################################
echo 'mergin central values'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/central/zj_ps_nlo_mer_cent_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/central/zj_ps_nlo_mer_cent_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o central.yoda

mkdir 'central'

mv central.yoda 'central/'

############################################################################
echo 'now mergin corresponding scales'

echo 'scale 1'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_1/zj_ps_nlo_mer_mur0.5_muf0.5_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_1/zj_ps_nlo_mer_mur0.5_muf0.5_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_1.yoda

mkdir 'scale_1'

mv scale_1.yoda 'scale_1/'
############################################################################
echo 'scale 2'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_2/zj_ps_nlo_mer_mur0.5_muf1_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_2/zj_ps_nlo_mer_mur0.5_muf1_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_2.yoda

mkdir 'scale_2'

mv scale_2.yoda 'scale_2/'
############################################################################
echo 'scale 3'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_3/zj_ps_nlo_mer_mur1_muf0.5_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_3/zj_ps_nlo_mer_mur1_muf0.5_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_3.yoda

mkdir 'scale_3'

mv scale_3.yoda 'scale_3/'
############################################################################
echo 'scale 4'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_4/zj_ps_nlo_mer_mur1_muf1_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_4/zj_ps_nlo_mer_mur1_muf1_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_4.yoda

mkdir 'scale_4'

mv scale_4.yoda 'scale_4/'
############################################################################
echo 'scale 5'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_5/zj_ps_nlo_mer_mur1_muf2_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_5/zj_ps_nlo_mer_mur1_muf2_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_5.yoda

mkdir 'scale_5'

mv scale_5.yoda 'scale_5/'
############################################################################
echo 'scale 6'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_6/zj_ps_nlo_mer_mur2_muf1_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_6/zj_ps_nlo_mer_mur2_muf1_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_6.yoda

mkdir 'scale_6'

mv scale_6.yoda 'scale_6/'
############################################################################
echo 'scale 7'

p1="sherpa_np_nlo_mer_r8_pt408_s1_1e3/scale_7/zj_ps_nlo_mer_mur2_muf2_R8.yoda"
p2="sherpa_np_nlo_mer_r8_pt408_s1e3_2e3/scale_7/zj_ps_nlo_mer_mur2_muf2_R8.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o scale_7.yoda

mkdir 'scale_7'

mv scale_7.yoda 'scale_7/'






