#!/usr/bin/env python3

import os
import sys

#########################################################
def get_cms_name (prefix, bin_choice, R, if_SD, alpha, pt_min, pt_max):
    # Set channel
    if R == 8:
        d1 = 2
    elif R == 4:
        d1 = 1
    else:
        print('Wrong value of R!')
 
    if if_SD:
        d2 = 2
    else:
        d2 = 1
        
    # Set observable 
    if alpha == '05':
        if bin_choice == 'all':
            xx = '03'
        elif bin_choice == 'chr':
            xx = '08'
        elif bin_choice == 'chr_w_all_in':
            xx = '11'
        else:
            print('Wrong value of the bin_choice variable!')
    elif alpha == '10':
        if bin_choice == 'all':
            xx = '04'
        elif bin_choice == 'chr':
            xx = '09'
        elif bin_choice == 'chr_w_all_in':
            xx = '12'
        else:
            print('Wrong value of the bin_choice variable!')
    elif alpha == '20':
        if bin_choice == 'all':
            xx = '05'
        elif bin_choice == 'chr':
            xx = '10'
        elif bin_choice == 'chr_w_all_in':
            xx = '13'
        else:
            print('Wrong value of the bin_choice variable!')
    else:
        print ('Wrong value of alpha!')
        
    # Set pT-bin 
    if pt_min == 50 and pt_max == 65:
        yy = '01'
    elif pt_min == 65 and pt_max == 88:
        yy = '02'
    elif pt_min == 88 and pt_max == 120:
        yy = '03'
    elif pt_min == 120 and pt_max == 150:
        yy = '04'
    elif pt_min == 150 and pt_max == 186:
        yy = '05'
    elif pt_min == 186 and pt_max == 254:
        yy = '06'
    elif pt_min == 254 and pt_max == 326:
        yy = '07'
    elif pt_min == 326 and pt_max == 408:
        yy = '08'
    elif pt_min == 408 and pt_max == 1500:
        yy = '09'
    else:
        print ('There is no such pT-bin!')
    
    # Get the final name
    name = prefix + '_d' + str(d1) + str(d2) + '-x' + str(xx) + '-y' + str(yy)

    return name
########################################################
# Get some parameters
R = int(sys.argv[1])

f_name = sys.argv[2]

pT_bins = [50, 65, 88, 120, 150, 186, 254, 326, 408, 1500]

print('Convering files in', f_name)

for i, el in enumerate(pT_bins[:-1]):
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
    
    print ('\npT in [' + str(pTL) + ', ' + str(pTR) + ']\n')
    
    print ('Charged+neutral CMS bins:')
    
    print ('Ungroomed charged+neutral bins')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_lin_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'all', R, False, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
    
    print ('Groomed charged+neutral bins')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_lin_SD_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'all', R, True, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
    
    
    print ('\nCharged-only CMS bins:')
    
    print ('Ungroomed charged-only bins')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_ch_lin_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'chr', R, False, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
    
    
    print ('Groomed charged-only bins')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_ch_lin_SD_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'chr', R, True, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
    
    
    print ('\nCharged-only CMS bins filled up with ALL fs-particles!:')
    
    print ('Ungroomed charged-only bins filled up with ALL fs-particles!')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_CH_lin_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'chr_w_all_in', R, False, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
    
    print ('Groomed charged-only bins filled up with ALL fs-particles!')
    for a in ['05', '10', '20']:
        before = f_name + 'PAPER_FIN_R{}_CH_lin_SD_l{}_pT_{}_{}.dat'.format(str(R), a, str(pTL), str(pTR))
    
        after  = f_name + get_cms_name ('CMS_MC', 'chr_w_all_in', R, True, a, pTL, pTR)
    
        print('cp ' + before + ' ' + after)
        os.system('cp ' + before + ' ' + after)
        
        
    print ('\nCross section per pT-bin (same for all angularities)')
    before = f_name + 'PAPER_FIN_R{}_XS_CUTS_pT_{}_{}.dat'.format(str(R), str(pTL), str(pTR))
    
    after  = f_name + 'CMS_MC_XS_R{}_pT_{}_{}.dat'.format(str(R), str(pTL), str(pTR))
    
    print('cp ' + before + ' ' + after)
    os.system('cp ' + before + ' ' + after)
    
