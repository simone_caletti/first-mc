#!/usr/bin/python2.7
import os

import os
import sys

# We need to provide following info
#n_runs n_event

n_runs   = int(sys.argv[1])

n_events = int(sys.argv[2])

i = 1
while i <= n_runs:
    path = "run" + str(i) + ".sh"

    fileObj = open(path, "a")

    fileObj.write("#!/bin/bash" + "\n")
   
    fileObj.write("Herwig run LHC-Matchbox.run -N " + str(n_events) + " -s " + str(i) + " -t" + " sim_" + str(i) + "\n")

    fileObj.write("mv *.yoda* plots/" + "\n")
    
    fileObj.write("rm  LHC-Matchbox-S" + str(i) + "-sim_" + str(i) + ".log\n")

    fileObj.write("rm  LHC-Matchbox-S" + str(i) + "-sim_" + str(i) + ".tex\n")

    fileObj.write("rm  LHC-Matchbox-S" + str(i) + "-sim_" + str(i) + ".out\n")
    
    fileObj.close()   

    os.system("chmod +x " + path) 

    i = i + 1


