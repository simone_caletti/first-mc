#/bin/bash
############################################################################
echo 'mergin central values'

p1="herwig_ps_lo_r8_pt408_s1_1e3/herwig_ps_lo_r8_pt408_s1_1e3.yoda"
p2="herwig_ps_lo_r8_pt408_s1e3_2e3/herwig_ps_lo_r8_pt408_s1e3_2e3.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o central.yoda

mkdir 'central'

mv central.yoda 'central/'

############################################################################
