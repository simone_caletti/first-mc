#!/usr/bin/python2.7

import os

res_name = "lo"

mur = [0.5, 0.5, 1,   1, 1, 2, 2]
muf = [0.5, 1,   0.5, 1, 2, 1, 2]

# Make corresponding folders

if not os.path.exists("central"):
    os.mkdir("central")


# Move files there
for i in range(0, 1001):
    print 'RUN #' + str(i)
    
    os.system('mv analysis_{}.yoda central/'.format(str(i)))


