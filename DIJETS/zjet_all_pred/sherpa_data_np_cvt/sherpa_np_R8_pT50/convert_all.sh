#!/bin/bash


echo $1

echo 'Do you want to switch to the CMS notation?'
read if_cnv

if [ $if_cnv = 'yes' ]; then
    echo 'Switching to CMS notation'
    
    echo 'central value'
    
    ./to_cms_notation.py $1 central/
    
    rm central/PAPER*
    
    echo 'done!'
    echo 'to get old files type rivet-cmphistos *.yoda* in a corresponding folder'
fi

echo 'Do you want to convert scale variation files as well?'
read if_scl
if [ $if_scl = 'yes' ]; then
    for i in 1 2 3 4 5 6 7  
    do
        name="scale_"$i
        echo $name/
    
        ./to_cms_notation.py $1 $name/
        
        rm $name/PAPER*
    done
    
    
    echo 'done!'
    echo 'to get old files type rivet-cmphistos *.yoda* in a corresponding folder'
fi
