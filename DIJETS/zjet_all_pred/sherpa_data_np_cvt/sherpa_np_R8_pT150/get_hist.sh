#!/bin/bash

echo 'central value first'
cd central/ 
rivet-cmphistos *.yoda*
cd ../


echo 'now scales:'
for i in 1 2 3 4 5 6 7
do
       name="scale_"$i
       echo $name
       cd $name
       rivet-cmphistos *.yoda*
       cd ../
done
