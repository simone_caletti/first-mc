# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

###################################################################
#####  HELPERS
###################################################################
###################################################################
# This one is a simple helper function to convert value of \alpha 
# to the prefixes used in names of your figures
###################################################################
def get_name (alpha):
    res = ''
    
    if alpha == 0.5:
        res = '05'
    elif alpha == 1.0:
        res = '10'
    elif alpha == 2.0:
        res = '20'
    
    return res
# This one is a simple helper function to convert from pTL, pTR 
# to the CMS (Daniel's) binning
####################################################################
def get_y(pTL, pTR):
    res = ''
    if pTL == 50 and pTR == 65:
        res = '01'
    elif pTL == 65   and pTR == 88: 
        res = '02'
    elif pTL == 88   and pTR == 120: 
        res = '03'
    elif pTL == 120 and pTR == 150:
        res = '04'
    elif pTL == 150 and pTR == 186: 
        res = '05'
    elif pTL == 186 and pTR == 254:
        res = '06'
    elif pTL == 254 and pTR == 326:
        res = '07'
    elif pTL == 326 and pTR == 408:
        res = '08'
    elif pTL == 408 and pTR == 1500: 
        res = '09'
    else:
        print('Wrong pT bin!')
   
    return res
###################################################################
# This one is a more complex helper function to switch from my 
# MC naming conention to the on used by the CMS
###################################################################
def get_cms_name (bin_choice, R, if_SD, alpha, pt_min, pt_max, prefix = 'CMS_MC_d'):
    # Set channel
    if R == 8:
        d1 = 2
    elif R == 4:
        d1 = 1
    else:
        print('Wrong value of R!')
 
    if if_SD:
        d2 = 2
    else:
        d2 = 1
        
    # Set observable 
    if alpha == '05':
        if bin_choice == 'all':
            xx = '03'
        elif bin_choice == 'chr':
            xx = '08'
        elif bin_choice == 'chr_w_all_in':
            xx = '11'
        else:
            print('Wrong value of the bin_choice variable!')
    elif alpha == '10':
        if bin_choice == 'all':
            xx = '04'
        elif bin_choice == 'chr':
            xx = '09'
        elif bin_choice == 'chr_w_all_in':
            xx = '12'
        else:
            print('Wrong value of the bin_choice variable!')
    elif alpha == '20':
        if bin_choice == 'all':
            xx = '05'
        elif bin_choice == 'chr':
            xx = '10'
        elif bin_choice == 'chr_w_all_in':
            xx = '13'
        else:
            print('Wrong value of the bin_choice variable!')
    else:
        print ('Wrong value of alpha!')
        
    # Set pT-bin 
    if pt_min == 50 and pt_max == 65:
        yy = '01'
    elif pt_min == 65 and pt_max == 88:
        yy = '02'
    elif pt_min == 88 and pt_max == 120:
        yy = '03'
    elif pt_min == 120 and pt_max == 150:
        yy = '04'
    elif pt_min == 150 and pt_max == 186:
        yy = '05'
    elif pt_min == 186 and pt_max == 254:
        yy = '06'
    elif pt_min == 254 and pt_max == 326:
        yy = '07'
    elif pt_min == 326 and pt_max == 408:
        yy = '08'
    elif pt_min == 408 and pt_max == 1500:
        yy = '09'
    else:
        print ('There is no such pT-bin!')
    
    # Get the final name
    name = prefix + str(d1) + str(d2) + '-x' + str(xx) + '-y' + str(yy)

    return name
###################################################################
# This one is a simple helper function 
# used to  get ratio of two hisrograms.
###################################################################
def get_ratio(h1, h2):
    ratio = []
    
    for i, j in zip(h1, h2):
        ratio.append(i / j)
        
    return ratio
###################################################################
# This one is used to plot a simple ratio of two histograms.
# Ratio of two central values, for the errors see below
###################################################################
def plot_ratio (ax, col, lb, ls, edges, h1, h2):
    # another way to draw the ratio
    for i, el in enumerate(edges[:-1]):
        r1, r2 = 0., 0.
        
        if h2[i] != 0.:
            r1 = h1[i] / h2[i]
 
        # Draw a horizontal line
        line = mlines([edges[i], edges[i+1]], [r1, r1], color=col, linewidth=6, linestyle=ls)

        ax.add_line(line)
        
        # Draw a vertical line
        if i < len(h1) - 1:
            r2 = 0.
            
            if h2[i] != 0.:
                r2 = h1[i+1] / h2[i+1]
        
            line = mlines([edges[i+1], edges[i+1]], [r1, r2], color=col, linewidth=6, linestyle=ls)

            ax.add_line(line)
###################################################################
# This one is used to  plot errors as shaded boxes around 
# central value bins. Is useful to plot scale variation error bands
###################################################################
def plot_boxes(ax, col, edges, hist, err_mns, err_pls, if_hatch = False):
    errorboxes = []
        
    for i, el in enumerate(edges[:-1]):
        # Set position of the bottom left angle of our rectangle
        xanc = edges[i]
        yanc = hist[i] - err_mns[i]
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = err_mns[i] + err_pls[i]
        
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor=col, facecolor='none')

        errorboxes.append(rect)
    
    if if_hatch:
        pc = PatchCollection(errorboxes, facecolor='none', hatch='X', alpha=0.3, edgecolor=col, zorder=3)
    else:
        pc = PatchCollection(errorboxes, facecolor=col, alpha=0.3, edgecolor='none', zorder=3)

    # Add the patch to the axes
    ax.add_collection(pc)
###################################################################
# This one is used to plot errors around unity and around ratio.
###################################################################
def plot_errors_rto(ax, col1, col2, edges, h1, d1_mns, d1_pls, h2, d2_mns, d2_pls):
    errorboxes_rto = []
    
    # Draw errors around ratio h1 / h2
    for i, el in enumerate(edges[:-1]):
        r_min = (h1[i] - d1_mns[i]) / h2[i]
        r_max = (h1[i] + d1_pls[i]) / h2[i]
        
        # Set position of the lower left angle of our  rectangle
        xanc = edges[i]
        yanc = r_min
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = r_max - r_min
 
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor=col1, facecolor='none')
            
        errorboxes_rto.append(rect)
 
    # Actuall drawing
    pc_rto = PatchCollection(errorboxes_rto, facecolor='none', hatch='X', alpha=0.3, edgecolor=col1, zorder=3)

    ax.add_collection(pc_rto)
 
    errorboxes_unt = []
    
    # Draw errors around unity
    for i, el in enumerate(edges[:-1]):
        r_min = (h2[i] - d2_mns[i]) / h2[i]
        r_max = (h2[i] + d2_pls[i]) / h2[i]
        
        # Set position of the lower left angle of our  rectangle
        xanc = edges[i]
        yanc = r_min
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = r_max - r_min
 
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor='r', facecolor='none')
            
        errorboxes_unt.append(rect)
            
    # Actuall drawing
    pc_unt = PatchCollection(errorboxes_unt, facecolor=col2, alpha=0.3, edgecolor=None, zorder=3)

    ax.add_collection(pc_unt)
###################################################################
# This one is used to plot errors around ratio only.
###################################################################
def plot_errors_rto_only(ax, col1, edges, h1, d1_mns, d1_pls, h2, d2_mns, d2_pls):
    errorboxes_rto = []
    
    # Draw errors around ratio h1 / h2
    for i, el in enumerate(edges[:-1]):
        r_min = (h1[i] - d1_mns[i]) / h2[i]
        r_max = (h1[i] + d1_pls[i]) / h2[i]
        
        # Set position of the lower left angle of our  rectangle
        xanc = edges[i]
        yanc = r_min
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = r_max - r_min
 
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor='none', facecolor=col1)
            
        errorboxes_rto.append(rect)
 
    # Actuall drawing
    pc_rto = PatchCollection(errorboxes_rto, facecolor=col1, alpha=0.3, edgecolor='none', zorder=3)

    ax.add_collection(pc_rto)
###################################################################
# Same as above but with solid ratio and 
# hatches around unity
###################################################################
def plot_errors_rto2(ax, col1, col2, edges, h1, d1_mns, d1_pls, h2, d2_mns, d2_pls):
    errorboxes_rto = []
    
    # Draw errors around ratio h1 / h2
    for i, el in enumerate(edges[:-1]):
        r_min = (h1[i] - d1_mns[i]) / h2[i]
        r_max = (h1[i] + d1_pls[i]) / h2[i]
        
        # Set position of the lower left angle of our  rectangle
        xanc = edges[i]
        yanc = r_min
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = r_max - r_min
 
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor='none', facecolor=col1)
            
        errorboxes_rto.append(rect)
 
    # Actuall drawing
    pc_rto = PatchCollection(errorboxes_rto, facecolor=col1, alpha=0.3, edgecolor='none', zorder=3)

    ax.add_collection(pc_rto)
 
    errorboxes_unt = []
    
    # Draw errors around unity
    for i, el in enumerate(edges[:-1]):
        r_min = (h2[i] - d2_mns[i]) / h2[i]
        r_max = (h2[i] + d2_pls[i]) / h2[i]
        
        # Set position of the lower left angle of our  rectangle
        xanc = edges[i]
        yanc = r_min
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = r_max - r_min
 
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=0, hatch='X', edgecolor=col2, facecolor='none')
            
        errorboxes_unt.append(rect)
            
    # Actuall drawing
    pc_unt = PatchCollection(errorboxes_unt, facecolor='none', hatch='X', linewidth=0, alpha=0.3, edgecolor=col2, zorder=3)

    ax.add_collection(pc_unt)
###################################################################
# This one is used to plot envelopes. Requires central value and 
# upper and lower boundaries (like separate hisrograms)
###################################################################
def plot_env(ax, col, edges, hist, hist_min, hist_max, if_hatch = False):
    errorboxes = []
        
    for i, el in enumerate(edges[:-1]):
        bin_cnt = 0.5 * (edges[i+1] + edges[i])
        
        # Set position of the lower left angle of our rectangle
        xanc = edges[i]
        yanc = hist_min[i]
        
        # Set width and height
        wx = edges[i+1] - edges[i]
        hy = hist_max[i] - hist_min[i]
        
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor=col, facecolor='none')
            
        errorboxes.append(rect)
            
    if if_hatch:
        pc = PatchCollection(errorboxes, facecolor='none', hatch='X', alpha=0.3, edgecolor=col, zorder=3)
    else:
        pc = PatchCollection(errorboxes, facecolor=col, alpha=0.3, edgecolor='none', zorder=3)
        
    # Add the patch to the axes
    ax.add_collection(pc)
