#/bin/bash
############################################################################
echo 'mergin central values'

p1="pythia_ps_pt408_s1_1e3/pythia_ps_pt408_s1_1e3.yoda"
p2="pythia_ps_pt408_s1e3_2e3/pythia_ps_pt408_s1e3_2e3.yoda"

echo $p1
echo $p2

yodamerge $p1  $p2 -o central.yoda

mkdir 'central'

mv central.yoda 'central/'

############################################################################
