import numpy as np

import math as m 

import matplotlib.ticker

# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

#######################################################################################
def hide_ticks(ax, if_lx = True):
    yticks = ax.yaxis.get_major_ticks()
    yticks[-1].set_visible(False)
    yticks[0].set_visible(False)
#######################################################################################
def set_logx_axis (ax, x_min, x_max, if_show = True):
    ax.set_xscale('log')
 
    xticks_ps    = [1e-5,         1e-4,       1e-3,        1e-2,        0.1,     0.4,     1]
    xticks_lb    = ['$10^{-5}$', '$10^{-4}$', '$10^{-3}$', '$10^{-2}$', '$0.1$', '$0.4$', '$1$'] 
 
    xticks_ps_fn = []
    xticks_lb_fn = []
 
    for i,j  in zip(xticks_ps, xticks_lb):
        if x_min <= i and i <= x_max:
            xticks_ps_fn.append(i)
            xticks_lb_fn.append(j)
        
    ax.set_xticks(xticks_ps_fn)
    ax.set_xticklabels(xticks_lb_fn)

    ax.set_xlim(x_min, x_max)
    
    if x_min < 1e-3:
        locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12)
        ax.xaxis.set_minor_locator(locmin)
        ax.xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    
    if if_show == False:
        ax.set_xticklabels([])
#######################################################################################
def set_y_axis (ax, y_axis, if_show = True):
    ax.set_ylim(y_axis[0], y_axis[1])
    
    yticks_ps    = []
    yticks_lb    = []
    
    y = y_axis[0]
    
    while y < y_axis[1] + y_axis[2]:
        yticks_ps.append(y)
        
        yticks_lb.append(y)

        y = y + y_axis[2]
     
    yticks = []
 
    # Set precision of labels
    str_len = len( str(y_axis[2]) )
    
    my_prc = str_len
    
    if 0 < y_axis[2] and y_axis[2]  < 1.:
        my_prc = str_len - 2
    
    for el in yticks_lb:
        string = '$' + "{value:{width}.{precision}f}".format(value=el, width=0, precision=my_prc) + '$'
            
        yticks.append( string  )
        
    
    ax.set_yticks(yticks_ps)
    ax.set_yticklabels(yticks)

    shift = 1e-8

    if y_axis[0] > 0:
        l_brd = y_axis[0] - shift
    else:
        l_brd = y_axis[0] + shift

    if y_axis[1] > 0:
        r_brd = y_axis[1] - shift
    else:
        r_brd = y_axis[1] + shift


    ax.set_ylim(l_brd, r_brd)
    
    if if_show == False:
        ax.set_yticklabels([])
#######################################################################################
def set_plot(ax1, ax2, pt_min, pt_max, alpha, x_axis, y_axis, r_axis):
    set_logx_axis (ax1,  x_axis[0], x_axis[1], False)
    set_logx_axis (ax2,  x_axis[0], x_axis[1])
    
    set_y_axis(ax1, y_axis)
    set_y_axis(ax2, r_axis)

    ax2.plot([1e-3,1], [0,0], linewidth = 4, linestyle='-', color = 'black')
    
    ax1.grid(True, which = 'major')
    ax2.grid(True, which = 'major')

    #ax1.set_ylabel ('$(1 / \sigma^{\\rm gluon, (0)}) \, d\sigma^{\\rm gluon} / d\log\lambda$')
    ax1.set_ylabel ('$(1 / \sigma^{\\rm quark, (0)}) \, d\sigma^{\\rm quark} / d\log\lambda$')
            
    ax2.set_ylabel ('$\\mathrm{FO \,-\, Exp.}$', labelpad=5)

    if alpha == 0.5:
        ax2.set_xlabel ('$\lambda^1_{1/2} \, [\mathrm{LHA}]$')
    elif alpha == 1.0:
        ax2.set_xlabel ('$\lambda^1_1 \, [\mathrm{Width}]$')
    elif alpha == 2.0:
        ax2.set_xlabel ('$\lambda^1_2 \, [\mathrm{Thrust}]$')
 

    # Add some text on top
    tit = r' $p_{} \in [{}, {}]$'.format("{T, \\rm jet}", str(pt_min), str(pt_max))  + r' $\mathrm{GeV},$' + r' $\rm parton{\textrm -}level$'
        
    ax_tw = ax1.twiny()
    ax_tw.set_xticks([])
    ax_tw.set_xlabel (tit)
#######################################################################################
def set_plot_match(ax1, pt_min, pt_max, alpha, x_ax, y_ax):
    set_logx_axis (ax1,  x_ax[0], x_ax[1])
    
    set_y_axis(ax1, y_ax)
    
    ax1.grid(True, which = 'major')

    ax1.set_ylabel ('$(1 / \sigma^{\mathrm{LO}}) \, d\sigma / d\log\lambda$')

    if alpha == 0.5:
        ax1.set_xlabel  ('$\lambda^1_{1/2} \, [\mathrm{LHA}]$')
    elif alpha == 1.0:
        ax1.set_xlabel ('$\lambda^1_1 \, [\mathrm{Width}]$')
    elif alpha == 2.0:
        ax1.set_xlabel ('$\lambda^1_2 \, [\mathrm{Thrust}]$')
 
    # Add some text on top
    tit = r' $p_{} \in [{}, {}]$'.format("{T, \\rm jet}", str(pt_min), str(pt_max))  + r' $\mathrm{GeV},$' + r' $\rm parton{\textrm -}level$'
 
    ax1_tw = ax1.twiny()
    ax1_tw.set_xticks([])
    ax1_tw.set_xlabel (tit)
    
    hide_ticks(ax1)
#######################################################################################
def get_y(pTL, pTR):
    if pTL == 50 and pTR == 65:
        res = '01'
    elif pTL == 65   and pTR == 88: 
        res = '02'
    elif pTL == 88   and pTR == 120: 
        res = '03'
    elif pTL == 120 and pTR == 150:
        res = '04'
    elif pTL == 150 and pTR == 186: 
        res = '05'
    elif pTL == 186 and pTR == 254:
        res = '06'
    elif pTL == 254 and pTR == 326:
        res = '07'
    elif pTL == 326 and pTR == 408:
        res = '08'
    elif pTL == 408 and pTR == 1500: 
        res = '09'
    else:
        print('Wrong pT bin!')
   
    return res
#################################################################################
def daniel_get_bins(path, key):
    if_key = False

    plot = open(path)

    if_begin = False
    
    if_end = False
    
    x_low  = []
    x_high = []
    
    err_minus = []
    err_plus  = []
    
    bin_edges = []
    bin_entry = []
    
    for line in plot:
        words = line.split()
        
        new_line = line.replace(" ", "")
        
        key_word = new_line.split()
        
        if len(key_word) and key_word[0] == key:  
            print (line)
            if_key = True
        
        if if_key:
            #print (line)
            
            if len(words) == 6:
                if words[0] == '#' and words[1] == 'xlow' and words[2] == 'xhigh':
                    # xlow	 xhigh	 val	 errminus	 errplus
                    if_begin = True
                
                    if_end = False
                
            if len(words) == 3:
                if words[0] == '#' and words[1] == 'END' and words[2] == 'HISTO1D':
                    if_end = True
            
                    if_begin = False
                
                    break

            if len(words) == 5 and if_begin == True and if_end == False:
                x_low.append( float(words[0]) )
            
                x_high.append( float(words[1]) )
                
                bin_entry.append( float(words[2]) )
            
                err_minus.append( float(words[3]) )
            
                err_plus.append( float(words[4]) )
        
    
    for el in x_low:
        bin_edges.append(el)
    
    bin_edges.append( x_high[-1] )
    
    bin_edges_exp  = []
    
    for el in bin_edges:
        bin_edges_exp.append(m.pow(10, el))
  
  
    return bin_edges_exp, bin_entry
######################################################################
def rebin_hist(edges, hist, rebin_f):
    re_bin_edges = []
    re_bin_entry = []
    
    if rebin_f == 1:
        re_bin_edges = edges
        re_bin_entry = hist
    elif  len(hist) % rebin_f == 0:
        # Get new bin edges
        i = 0
        while rebin_f * i <= len(edges):
            re_bin_edges.append(edges[rebin_f * i])
            
            i += 1
 
        # Get new bin entries
        for j in range(0, len(hist) // rebin_f):
            sum_h = sum(hist[j*rebin_f:(j+1)*rebin_f])
            
            re_bin_entry.append(sum_h)
    else:
        print ('You are use wrong rebinning factor!')
        
    return re_bin_edges, re_bin_entry
#################################################################################
def make_yoda_plot_res (ax, bin_f, path, key, tit, col, lw = 8, ls = '-', if_show = True):
    bin_edges, hist = daniel_get_bins (path, key)
    
    bin_edges_r, hist_r =  rebin_hist(bin_edges, hist, bin_f)
    
    scale_r = np.sum ( hist_r * np.diff(bin_edges_r) )
    
    bin_center_r = []
    
    hist_sc = []
    
    for el in hist_r:
        hist_sc.append(el / bin_f)
    
    for i, el in enumerate(bin_edges_r[:-1]):
        bin_center_r.append( 0.5 * (bin_edges_r[i + 1] + bin_edges_r[i]) )
    
    if if_show:
        ax.hist(bin_center_r, bins=bin_edges_r, weights=hist_sc, color=col,  linewidth=lw, linestyle=ls, label=tit, histtype= 'step')

    return bin_center_r, hist_sc, bin_edges_r
#################################################################################
def make_yoda_plot_match (ax, bin_f, path, key, col, ls = '-'):
    bin_edges, hist = daniel_get_bins (path, key)
    
    bin_edges_r, hist_r =  rebin_hist(bin_edges, hist, bin_f)
    
    bin_center_r = []
    
    hist_sc = []
    
    for el in hist_r:
        hist_sc.append(el / bin_f)
    
    for i, el in enumerate(bin_edges_r[:-1]):
        bin_center_r.append( 0.5 * (bin_edges_r[i + 1] + bin_edges_r[i]) )
    
    bin_center = []
    for i, el in enumerate(bin_edges[:-1]):
        bin_center.append( 0.5 * (bin_edges[i + 1] + bin_edges[i]) )
    
    if col == 'black' or col == 'green':
        ax.hist(bin_center_r, bins=bin_edges_r, weights=hist_sc, color=col, facecolor=None, edgecolor=col, linestyle=ls, linewidth=8, histtype= 'step')
    
    if col == 'blue':
        ax.hist(bin_center_r, bins=bin_edges_r, weights=hist_sc, facecolor=col, edgecolor=None, linestyle=ls, linewidth=8, histtype= 'stepfilled', alpha=0.3)
    
    return bin_center_r, hist_sc, bin_edges_r
#################################################################################
def diff_list (li1, li2):
    res = []
    
    for i, j in zip(li1, li2):
        res.append(i - j)
        
    return res
#################################################################################
def daniel_make_plot(ax1, ax2, my_ch, alpha,  bin_f, R, pTL, pTR, if_sd):
    jet_r = '1'
    if R == 8:
        jet_r = '2'
    
    sd = '1'
    if if_sd:
        sd = '2'
    pt_bin = get_y(pTL, pTR)
    
    if alpha == 0.5:
        path = 'data_fo/FO_' + my_ch + '_d' + jet_r + sd + '-x13-y' + pt_bin + '.dat'
    elif alpha == 1.0:
        path = 'data_fo/FO_' + my_ch + '_d' + jet_r + sd + '-x14-y' + pt_bin + '.dat'
    elif alpha == 2.0:
        path = 'data_fo/FO_' + my_ch + '_d' + jet_r + sd + '-x15-y' + pt_bin + '.dat'
        
    print (path)
    key = [r'Title=$\Sigma_\text{LO}$', 
           r'Title=$\Sigma_\text{expLO}$', 
           r'Title=$\Sigma_\text{NLO}$', 
           r'Title=$\Sigma_\text{expNLO}$',
           r'Title=$\Sigma_\text{expNLO}^{C}$']

    # FO
    center_lo, hist_lo, edges_lo = make_yoda_plot_res (ax1, bin_f, path, key[0], r'$\Sigma_\mathrm{LO}$', 'red')
 
    center_nlo, hist_nlo, edges_nlo = make_yoda_plot_res (ax1, bin_f, path, key[2], r'$\Sigma_\mathrm{NLO}$', 'green')

    # Exp
    center_lo_exp, hist_lo_exp, edges_lo_exp          = make_yoda_plot_res (ax1, bin_f, path, key[1], r'$\Sigma_\mathrm{exp \, LO}$', 'blue')
    
    center_nlo_exp, hist_nlo_exp, edges_nlo_exp       = make_yoda_plot_res (ax1, bin_f, path, key[3], r'$\Sigma_\mathrm{exp \, NLO}$',  'orange')
    
    center_nlo_exp_c, hist_nlo_exp_c, edges_nlo_exp_c = make_yoda_plot_res (ax1, bin_f, path, key[4], r'$\Sigma_\mathrm{exp \, NLO}^{C}$', 'purple')
 
    # Difference 
    dif_lo = diff_list(hist_lo, hist_lo_exp)
    
    ax2.hist(center_lo_exp, bins=edges_lo_exp, weights=dif_lo, linewidth=8, color='blue', linestyle='-', label='', histtype= 'step')
    
    dif_nlo = diff_list(hist_nlo, hist_nlo_exp)
    
    ax2.hist(center_nlo_exp, bins=edges_nlo_exp, weights=dif_nlo, linewidth=8, color='orange', linestyle='-', label='', histtype= 'step')

    dif_nlo_c = diff_list(hist_nlo, hist_nlo_exp_c)
    
    ax2.hist(center_nlo_exp_c, bins=edges_nlo_exp_c, weights=dif_nlo_c, linewidth=8, color='purple', linestyle='-', label='', histtype= 'step')
#################################################################################
###################################################################
# This one is used to  plot errors as shaded boxes around 
# central value bins. Is useful to plot scale variation error bands
###################################################################
def plot_errors(ax, col, bin_edges, hist, err_min, err_max):
    errorboxes = []
        
    for i, el in enumerate(bin_edges[:-1]):
        bin_cnt = 0.5 * (bin_edges[i+1] + bin_edges[i])
        
        # Set position of the lower left angle of our  rectangle
        xanc = bin_edges[i]
        yanc = (hist[i] - err_min[i])
        
        # Set width and height
        wx = bin_edges[i+1] - bin_edges[i]
        hy = (err_min[i] + err_max[i])
        
        rect = mpatches.Rectangle((xanc, yanc), wx, hy, linewidth=8, edgecolor='r', facecolor='none')
            
        errorboxes.append(rect)
            
    pc = PatchCollection(errorboxes, facecolor=col, alpha=0.3, edgecolor=None, zorder=3)

    # Add the patch to the axes
    ax.add_collection(pc)
###################################################################
def daniel_make_plot_match(ax, alpha,  bin_f, R, pTL, pTR, if_sd = False):
    jet_r = '1'
    if R == 8:
        jet_r = '2'
    
    sd = '1'
    if if_sd:
        sd = '2'
    pt_bin = get_y(pTL, pTR)
    
    if alpha == 0.5:
        path = 'data_matching/Match_d' + jet_r + sd + '-x13-y' + pt_bin + '.dat'
    elif alpha == 1.0:
        path = 'data_matching/Match_d' + jet_r + sd + '-x14-y' + pt_bin + '.dat'
    elif alpha == 2.0:
        path = 'data_matching/Match_d' + jet_r + sd + '-x15-y' + pt_bin + '.dat'
        
    print (path)
    key = [r'Title=Gluon',
           r'Title=Quark',
           r'Title=NLO+NLL$^\prime$',
           r'Title=NLO']


    # FO
    make_yoda_plot_match (ax, bin_f, path, key[3], 'green')
 
    # Res
    bin_res, h_res, edges_res = make_yoda_plot_match (ax, bin_f, path, key[2], 'black')
    
    # Quark
    bin_q,  h_q,   edges_q   =  make_yoda_plot_match (ax, bin_f, path, key[1], 'blue')

    
    hc = []
    
    err_p = []
    err_m = []

    for i, j in zip(h_res, h_q):
        c_val = (i + j) / 2.
        
        delta = (i - j) / 2.  
    
        hc.append(c_val)
        
        err_p.append(delta)
        err_m.append(delta)
        
        
    plot_errors(ax, 'red', edges_q, hc, err_m, err_p)



