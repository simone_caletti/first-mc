#!/usr/bin/env python3

# My libs
import sys

sys.path.append('../my_libs_cvt')

from cosmetics import *
from my_drawers import * 
from my_plot import plot_mc, plot_res

# Standard libs
import numpy as np
import math as m

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

# Choose a LaTeX font
plt.rc('text',       usetex     = True)
plt.rc('text.latex', preamble   = r"\usepackage{amsmath}")
plt.rc('font',       weight='bold')

# Main sizes
plt.rc('font',   size       = 70)          
plt.rc('axes',   labelsize  = 70)     
plt.rc('axes',   titlesize  = 70)     
plt.rc('axes',   labelpad   = 20)     
plt.rc('hatch',  linewidth  = 0.8)

# Lines: somehow does not work for histos
plt.rc('lines', linewidth = 6)

# Legend
plt.rc('legend', fontsize   = 46) 
plt.rc('legend', framealpha = None)

# Set ticks
plt.rc('ytick.major',   width     = 3) 
plt.rc('ytick.major',   size      = 30) 
plt.rc('ytick.major',   pad       = 10)
plt.rc('ytick.major',   pad       = 15) 

plt.rc('ytick.minor',   size      = 15) 
plt.rc('ytick',         direction = 'in') 

plt.rc('xtick.major',   width     = 3) 
plt.rc('xtick.major',   size      = 30) 
plt.rc('xtick.major',   pad       = 15) 
plt.rc('xtick.major',   size      = 30) 

plt.rc('xtick.minor',   size      = 15) 
plt.rc('xtick',         direction = 'in') 

# Grids
plt.rc('axes', grid = True)
plt.rc('axes.grid', which='major')
plt.rc('grid', linestyle='dotted')
plt.rc('grid', linewidth=0.5)

#######################################################################################
### This one is in charge for custom legend labels
### Needed to switch from "boxes" to lines in histograms
### and to added shaded areas around lines 
### x and y set the legend location
#######################################################################################
def set_custom_legend(ax, x, y, alpha_val = 0.3):
    patches, labels = ax.get_legend_handles_labels()

    # Remove old pathches and labels
    patches = []
    labels = []

    # Define your own patches and set labels
    ###
    mRect = mpatches.Patch(facecolor='black', alpha=alpha_val, edgecolor='none')       
 
    mLine = mlines([0],[0], color='black')
 
    patches.append((mRect,mLine))
    
    labels.append('$\mathrm{NLO \,+\, NLL^\prime} \, (\mu_R, \mu_F, x_L)$')
    
    ###
    rRect = mpatches.Patch(facecolor='white', hatch='X', alpha=alpha_val, edgecolor='red')       

    rLine = mlines([0],[0], color='red', linestyle='-.')

    patches.append((rRect,rLine))
 
    labels.append('$\mathrm{SHERPA \, MEPS@NLO} \, (\mu_R, \mu_F)$')
 
    ###
    bLine = mlines([0],[0], color='blue', linestyle='--')
    
    patches.append(bLine)
    
    labels.append('$\mathrm{PYTHIA8 \, LO}$')
    
    ###
    gLine = mlines([0],[0], color='green',  linestyle=':')

    patches.append(gLine)

    labels.append('$\mathrm{HERWIG7 \, LO}$')

    ax.legend(patches,  labels, loc = (x, y), borderaxespad=2.)
#######################################################################################
### This one is in charge for actuall plotting 
### Edit it to change colors, line style etc
#######################################################################################
def plot_mc_res(ax1, ax2, pTL, pTR, R, alpha, if_gr = False):
    # Choose the write folder (I have three different folders depending on the pT slice I use)
    # The first one  for the pT interval pT in [50, 150)
    # The second one for the pT interval pT in [150, 408)
    # And the last one for the pT interval pT in [408, 1500]
    
    if pTL < 150:
        pt_sl = '_pT50'
    elif pTL >= 150 and pTL < 408:
        pt_sl = '_pT150'
    else:
        pt_sl = '_pT408'
    
    # Sherpa, Pythia, Herwig
    s_path_ps = '../sherpa_data_ps_cvt/sherpa_ps_R{}'.format( str(R) ) + pt_sl + '/'
    p_path_ps = '../pythia_data_ps_cvt/pythia_ps_R{}'.format( str(R) ) + pt_sl + '/'
    h_path_ps = '../herwig_data_ps_cvt/herwig_ps_R{}'.format( str(R) ) + pt_sl + '/'

    # Get bin edges, x-section in pb, d\sigma / d\lambda distributions and scale errors
    # Plot distributions
    # def plot_mc(ax, path, col, ls, R, alpha, pTL, pTR, bin_choice, if_scl_var, if_SD, if_show = True, if_hatch = False):
    # bin_choice = 'all', 'chr', 'chr_w_all_in'

    edges_mc, s_sig, s_hist, s_err_mns, s_err_pls = plot_mc(ax1, s_path_ps, 'red', '-.', R, alpha, pTL, pTR, 'chr_w_all_in', True, if_gr, True, True)
    
    edges_mc, p_sig, p_hist = plot_mc(ax1, p_path_ps, 'blue',  '--', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr)
    edges_mc, h_sig, h_hist = plot_mc(ax1, h_path_ps, 'green', ':',  R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr)
    
    # Resummed
    path_res     = '../data_R{}_Daniel/'.format( str(R) )
    
    # Here True / False meas use CMS bins for charged / charged + neutral fs-particles
    edges_r, r_sig, r_hist, r_err_mns, r_err_pls = plot_res(ax1, path_res, 'black', '-', R, alpha, pTL, pTR, if_gr, True)

    # Ratios:
    if edges_r != edges_mc:
        print('different bins are used for MC and Res!')
    else:
        # Scale histograms to unity
        # We compute ratio of  scaled (1/sigma)(d\sigma/d\lambda) distributions
        s_hist_sig = [x / s_sig for x in s_hist]
        p_hist_sig = [x / p_sig for x in p_hist]
        h_hist_sig = [x / h_sig for x in h_hist]
        r_hist_sig = [x / r_sig for x in r_hist]
 
        # Plot the ratios MC / NLO+NLL'
        # Sherpa / Res
        plot_ratio (ax2, 'red',  '', '-.',  edges_r, s_hist_sig, r_hist_sig)
        
        # Pythia / Res
        plot_ratio (ax2, 'blue',  '', '--', edges_r, p_hist_sig, r_hist_sig)

        # Herwig / Res
        plot_ratio (ax2, 'green', '', ':',  edges_r, h_hist_sig, r_hist_sig)
        
        # Scale errors
        # We need errors for (1/sigma)(d\sigma/d\lambda) distributions
        # Sherpa
        s_err_mns_sig = [x / s_sig for x in s_err_mns]
        s_err_pls_sig = [x / s_sig for x in s_err_pls]
        
        # Resummed
        r_err_mns_sig = [x / r_sig for x in r_err_mns]
        r_err_pls_sig = [x / r_sig for x in r_err_pls]
        
        # Error around unity
        plot_errors_rto(ax2, 'red', 'black', edges_r, 
                                             s_hist_sig, s_err_mns_sig, s_err_pls_sig, 
                                             r_hist_sig, r_err_mns_sig, r_err_pls_sig)
#######################################################################################
# Master plot
# Set layout and cosmetics
def plot_all(R, pTL, pTR, alpha, if_SD, y_min, y_max, y_step, r_min, r_max, r_step, X, Y, X1, Y1):
    plt.subplots(1, 1, sharex=True, figsize=(30., 30.), dpi=100)

    # make outer gridspec
    # define relative widths
    w1 = 1.     # width of the first bin
    w12 = 0.25  # width of the empty ('broken') space between the first bin and the rest
    w2 = 20.    # width of the rest of the plot (these are the main bins)
    
    # needed to set the slope of the slanted lines
    ratio_w21 = w2 / w1

    all_widths = [w1, w12, w2] 
    
    # define relative heights 
    h1 = 2  # height of the plot with histograms
    h2 = 1  # height of the ratio plot

    # needed to set slope of the slanted lines
    ratio_h21 = h2 / h1
    
    all_heights = [h1, h2]

    # Main grid (canvas)
    # Adjust left, bottom, right and top parameters to remove unwated white spaces
    outer = gridspec.GridSpec(2, 3,  width_ratios  = all_widths, 
                                     height_ratios = all_heights, wspace=0., hspace=0.,
                                     left=0.12, bottom=0.11, right=0.95, top=0.94)

    # make nested gridspecs
    # first row (hist)
    # array of axes for the first row; ax1[0] -> first bin, ax1[1] -> the rest of the bins
    ax1 = []    
    
    # same as above but for the ratios
    ax2 = []
   
    # Loop  over the main grid and fill in arrays of axes
    # we skip outer[1] and outer[4] to create line breaks!
    # first row
    for i in [0, 2]:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[i])

        ax1.append( plt.subplot(gs[0,0]) )

    for i in [3, 5]:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[i])

        ax2.append( plt.subplot(gs[0,0]) )

    # Set axis breaks
    axes_break(ax1[0], ax1[1], ratio_w21)
    axes_break(ax2[0], ax2[1], ratio_w21, ratio_h21)

    # Draw a horizontal line to guide the eye
    for ax in ax2:
        ax.plot([0,1], [1,1], linewidth = 4, linestyle='-', color = 'black')

    # Set x-axes
    # Ranges have to be adjusted manually
    # First bins
    x_ax = []
    
    if alpha == 0.5:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 1.0:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 2.0:
        x_ax = [1e-4, 1e-3, 1]
    
    set_x_axis (ax1[0],  x_ax, False)
    set_x_axis (ax2[0],  x_ax, False)
        
    # Other bins (the main part of the plot)
    # use, for example, delta = 5.001e-2 instead of  5e-2  to avoid an overlap between slanted lines and minor ticks on x-axis
    
    # Upper row
    if alpha == 0.5:
        set_logx_axis (ax1[1],  5.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  5.001 * x_ax[1], 1)
    elif alpha == 1.0:
        set_logx_axis (ax1[1],  1.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  1.001 * x_ax[1], 1)
    elif alpha == 2.0:
        set_logx_axis (ax1[1],  2.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  2.001 * x_ax[1], 1)
    
    # Set y-axes
    # Set range and step for ticks on y-axes (for histo plots)
    y_ax_h = [y_min, y_max, y_step]
    
    # Set range and step for ticks on y-axes (for ratio plots)
    y_ax_r = [r_min, r_max, r_step]
    
    # First bin 
    set_y_axis(ax1[0], y_ax_h)
    set_y_axis(ax2[0], y_ax_r)
    
    # Other bins 
    set_y_axis(ax1[1], y_ax_h, False)
    set_y_axis(ax2[1], y_ax_r, False)

    # Set font size 
    # Needed to make some labels a bit bigger
    # Has to be adjusted manually
    f_size=100

    # Name y-axes
    ax1[0].set_ylabel ('$(\lambda_{\\rm bc}/ \sigma) \, d\sigma / d\lambda$', fontsize=f_size)

    ax2[0].set_ylabel ('$\\mathrm{MC \,/\, NLO \,+\, NLL^\prime}$')
    
    # Set lables for the lower x-axis
    if alpha == 0.5:
        ax2[1].set_xlabel  ('$\lambda^1_{1/2} \,\, [\mathrm{LHA}]$',  fontsize=f_size)
    elif alpha == 1.0:
        ax2[1].set_xlabel  ('$\lambda^1_{1} \,\, [\mathrm{Width}]$',  fontsize=f_size)
    elif alpha == 2.0:
        ax2[1].set_xlabel  ('$\lambda^1_{2} \,\, [\mathrm{Thrust}]$', fontsize=f_size)

    # Hide unwated ticks for x- and y-axes
    for i, j in zip(ax1, ax2):
        hide_ticks(i)
        hide_ticks(j)
    
    # Actuall plotting starts here
    plot_mc_res(ax1[0], ax2[0],  pTL, pTR, R, alpha, if_SD)
    
    plot_mc_res(ax1[1], ax2[1],  pTL, pTR, R, alpha, if_SD)

    # Set Legend
    set_custom_legend(ax1[1], X, Y)
    
    # Set annotation
    if if_SD:
        if R == 8:
            ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.8$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=90,
                            bbox=dict(boxstyle="round", fc="1"))
        elif R == 4:
            ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.4$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=90,
                            bbox=dict(boxstyle="round", fc="1"))
    else:
        if R == 8:
            ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.8$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=90,
                            bbox=dict(boxstyle="round", fc="1"))
        elif R == 4:
            ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.4$',
                            xy=(X1, Y1), xycoords='axes fraction',
                            size=90,
                            bbox=dict(boxstyle="round", fc="1"))
            
    # Add some text on top of each column
    tit = r' $p_{} \in [{}, {}]$'.format("{T, \, \\rm jet}", str(pTL), str(pTR)) + r' $\mathrm{GeV},$'+r' $\rm parton{\textrm -}level$'
    
    ax_tw = ax1[1].twiny()
    ax_tw.set_xticks([])
    ax_tw.set_xlabel (tit, fontsize=f_size, labelpad=40)

    # Hide unwanted axes
    ax_tw.spines['left'].set_visible(False)
    ax_tw.spines['top'].set_visible(False)
    
    # Hide unwanted ticks 
    ax1[1].tick_params(axis = "y", which = "both", left = False)
    ax2[1].tick_params(axis = "y", which = "both", left = False)

    # Save your nice plot!
    if if_SD:
        plt.savefig('SHERPA_vs_RES_lin_ch_ZJ_PS_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '_SD.pdf')
    else:
        plt.savefig('SHERPA_vs_RES_lin_ch_ZJ_PS_pT' + str(pTL) + '_' + str(pTR) + '_a' + get_name(alpha) + '.pdf')
###########################################################################################
#pT_bins = [50, 65, 88, 120, 150, 186, 254, 326, 408, 1500]

#pT_bins = [120, 150]
pT_bins = [408, 1500]

R = 8

# Loop ver pT bins. Make plots
i = 0
while i < len(pT_bins) - 1:
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
 
    print ('pT in [', str(pTL), ', ', str(pTR), '] GeV')

    '''
    # Adjusted for pT[120, 150] interval 
    #                                y_min y_max y_step r_min  r_max r_step x     y     x1    x2
    plot_all(R, pTL, pTR, 0.5, True, 0,    0.7,  0.1,   0.0,   3,    0.5,   0.20, 0.03, 0.75, 0.8)
    plot_all(R, pTL, pTR, 1.0, True, 0,    0.4,  0.05,  0.5,   2,    0.25,  0.20, 0.03, 0.75, 0.8)
    plot_all(R, pTL, pTR, 2.0, True, 0,    0.25, 0.05,  0.5,   1.75, 0.25,  0.20, 0.03, 0.75, 0.8)

    #                                 y_min y_max y_step r_min  r_max r_step x     y     x1    x2
    plot_all(R, pTL, pTR, 0.5, False, 0,    0.7,  0.1,   0.0,   3,    0.5,   0.20, 0.01, 0.65, 0.8)
    plot_all(R, pTL, pTR, 1.0, False, 0,    0.4,  0.05,  0.5,   2.5,  0.5,   0.20, 0.03, 0.70, 0.8)
    plot_all(R, pTL, pTR, 2.0, False, 0,    0.4,  0.05,  0.0,   2.5,  0.5,   0.20, 0.05, 0.65, 0.8)
    '''
    
    # Adjusted for pT[408, 1500] interval 
    #                                y_min y_max  y_step r_min  r_max r_step x    y     x1    x2
    plot_all(R, pTL, pTR, 0.5, True, 0,    0.6,   0.1,   0.0,   3,    0.5,   0.2, 0.05, 0.75, 0.8)
    plot_all(R, pTL, pTR, 1.0, True, 0,    0.35,  0.05,  0.5,   1.75, 0.25,  0.2, 0.03, 0.75, 0.8)
    plot_all(R, pTL, pTR, 2.0, True, 0,    0.30,  0.05,  0.5,   1.75,  0.25, 0.2, 0.67, 0.75, 0.8)
    
    #                                 y_min y_max y_step r_min  r_max r_step x     y     x1    x2
    plot_all(R, pTL, pTR, 0.5, False, 0,    0.6,  0.1,   0.0,   3.00, 0.50,  0.20, 0.05, 0.70, 0.8)
    plot_all(R, pTL, pTR, 1.0, False, 0,    0.40, 0.05,  0.5,   2.00, 0.25,  0.20, 0.05, 0.70, 0.8)
    plot_all(R, pTL, pTR, 2.0, False, 0,    0.30, 0.05,  0.5,   1.75, 0.25,  0.16, 0.05, 0.70, 0.8)
    
    i += 1



