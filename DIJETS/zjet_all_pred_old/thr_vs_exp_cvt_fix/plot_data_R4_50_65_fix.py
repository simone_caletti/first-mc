#! /usr/bin/env python3
# My libs
import sys

sys.path.append('../my_libs_cvt')

from cosmetics import *
from my_drawers import * 
from my_plot import plot_mc, plot_mc_f, plot_res, plot_band_np_vs_ps, plot_cms


# Standard libs
import numpy as np
import math as m

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Patches used for drawing
from matplotlib.lines import Line2D as mlines
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

# Use it to create multiple pages
from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('thr_vs_exp_R4_small_bins_fixed_50_65.pdf')

# Choose a LaTeX font
plt.rc('text',       usetex     = True)
plt.rc('text.latex', preamble   = r"\usepackage{amsmath}")
plt.rc('font',       weight='bold')

# Main sizes
plt.rc('font',   size       = 70)          
plt.rc('axes',   labelsize  = 70)     
plt.rc('axes',   titlesize  = 70)     
plt.rc('axes',   labelpad   = 20)     
plt.rc('hatch',  linewidth  = 0.8)

# Lines: somehow does not work for histos
plt.rc('lines', linewidth = 6)

# Legend
plt.rc('legend', fontsize   = 40) 
plt.rc('legend', framealpha = None)

# Set ticks
plt.rc('ytick.major',   width     = 3) 
plt.rc('ytick.major',   size      = 30) 
plt.rc('ytick.major',   pad       = 10)
plt.rc('ytick.major',   pad       = 15) 

plt.rc('ytick.minor',   size      = 15) 
plt.rc('ytick',         direction = 'in') 

plt.rc('xtick.major',   width     = 3) 
plt.rc('xtick.major',   size      = 30) 
plt.rc('xtick.major',   pad       = 15) 
plt.rc('xtick.major',   size      = 30) 

plt.rc('xtick.minor',   size      = 15) 
plt.rc('xtick',         direction = 'in') 

# Grids
plt.rc('axes', grid = True)
plt.rc('axes.grid', which='major')
plt.rc('grid', linestyle='dotted')
plt.rc('grid', linewidth=0.5)

#######################################################################################
### This one is in charge for custom legend labels
### Needed to switch from "boxes" to lines in histograms
### and to added shaded areas around lines 
### x and y set the legend location
#######################################################################################
def set_custom_legend(ax, x, y, alpha_val = 0.3):
    patches, labels = ax.get_legend_handles_labels()

    # Remove old pathches and labels
    patches = []
    labels = []

    # Define your own patches and set labels
    ###
    
    rRect = mpatches.Patch(facecolor='white', hatch='X', alpha=alpha_val, edgecolor='red')       

    rLine = mlines([0],[0], color='red', linestyle='-')

    patches.append((rRect,rLine))
 
    labels.append('$\mathrm{NLO \,+\, NLL^\prime \,+\, NP} \, (\mu_R, \mu_F, x_L, \delta_{\mathrm{NP}})$')

    ###
    sRect = mpatches.Patch(facecolor='blue', alpha=alpha_val, edgecolor='none')       

    sLine = mlines([0],[0], color='blue', linestyle='--')

    patches.append((sRect,sLine))
 
    labels.append('$\mathrm{SHERPA \, MEPS@NLO} \, (\mu_R, \mu_F)$')
    
    ###
    bLine = mlines([0],[0], color='black', linestyle='-')
 
    bCrc = mlines([0], [0], marker='o', color='black', markerfacecolor='black',  markersize=25)
  
    patches.append((bLine, bCrc))
    
    labels.append('$\mathrm{CMS \,\, data}$')

    ax.legend(patches,  labels, loc = (x, y), borderaxespad=2.)
#######################################################################################
def plot_dat_vs_thr (ax1, ax2, pTL, pTR, R, alpha,  if_gr, if_ch_tr):
    # Choose the write folder (I have three different folders depending on the pT slice I use)
    # The first one  for the pT interval pT in [50, 150)
    # The second one for the pT interval pT in [150, 408)
    # And the last one for the pT interval pT in [408, 1500]
    if pTL < 150:
        pt_sl = '_pT50'
    elif pTL >= 150 and pTL < 408:
        pt_sl = '_pT150'
    else:
        pt_sl = '_pT408'
 
    # Set paths
    # PYTHIA 
    p_path_ps = '../pythia_data_ps_cvt/pythia_ps_R{}'.format( str(R) ) + pt_sl + '/'
    p_path_np = '../pythia_data_np_cvt/pythia_np_R{}'.format( str(R) ) + pt_sl + '/'

    # HERWIG
    h_path_ps = '../herwig_data_ps_cvt/herwig_ps_R{}'.format( str(R) ) + pt_sl + '/'
    h_path_np = '../herwig_data_np_cvt/herwig_np_R{}'.format( str(R) ) + pt_sl + '/'
 
    # SHERPA
    s_path_ps = '../sherpa_data_ps_cvt/sherpa_ps_R{}'.format( str(R) ) + pt_sl + '/'
    s_path_np = '../sherpa_data_np_cvt/sherpa_np_R{}'.format( str(R) ) + pt_sl + '/'

    broken_bin = [ 
        {"alpha": 0.5, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr_w_all_in', "nbc": 8,  "ud": 'd', "deepth": 2},
        {"alpha": 0.5, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr_w_all_in', "nbc": 9,  "ud": 'd', "deepth": 2}, 
        #
        {"alpha": 1.0, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr_w_all_in', "nbc": 10, "ud": 'd', "deepth": 2}, 
        {"alpha": 1.0, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr_w_all_in', "nbc": 10, "ud": 'd', "deepth": 2}, 
        #
        {"alpha": 2.0, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr_w_all_in', "nbc": 9,  "ud": 'd', "deepth": 2},
        {"alpha": 2.0, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr_w_all_in', "nbc": 10, "ud": 'd', "deepth": 2},
        #
        #
        {"alpha": 0.5, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr', "nbc": 10,  "ud": 'd', "deepth": 2},
        {"alpha": 0.5, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr', "nbc": 10,  "ud": 'd', "deepth": 2},
        #
        {"alpha": 1.0, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr', "nbc": 11,  "ud": 'd', "deepth": 2},
        {"alpha": 1.0, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr', "nbc": 11,  "ud": 'd', "deepth": 2},
        #
        {"alpha": 2.0, "pTL": 50, "pTR": 65, "SD": False, "bins": 'chr', "nbc": 8,  "ud": 'd', "deepth": 2},
        {"alpha": 2.0, "pTL": 50, "pTR": 65, "SD": True,  "bins": 'chr', "nbc": 10, "ud": 'd', "deepth": 2}]


    # Get bin_edges, x-section and d\sigma/d\lambda distributions at ps-level
    # Pythia, Herwig, Sherpa                                                            
    # def plot_mc(ax, path, col, ls, R, alpha, pTL, pTR, bin_choice, if_scl_var, if_SD, if_show = True, if_hatch = False):
    # bin_choice = 'all', 'chr', 'chr_w_all_in'
    
    edges_ps, p_sig_ps, p_hst_ps = plot_mc(ax1, p_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    edges_ps, h_sig_ps, h_hst_ps = plot_mc(ax1, h_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    edges_ps, s_sig_ps, s_hst_ps = plot_mc(ax1, s_path_ps, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
    
    # Get bin_edges, x-section and d\sigma/d\lambda distributions at np-level
    if if_ch_tr:
        # Only charged fs-hadrons
        edges_np, p_sig_np, p_hst_np = plot_mc(ax1, p_path_np, '', '-', R, alpha, pTL, pTR, 'chr',  False, if_gr, False)
        edges_np, h_sig_np, h_hst_np = plot_mc(ax1, h_path_np, '', '-', R, alpha, pTL, pTR, 'chr',  False, if_gr, False)
        
        edges_np, s_sig_np, s_hst_np, s_err_m_np, s_err_p_np = plot_mc_f(ax1, s_path_np, 'blue', '--', R, alpha, pTL, pTR, 'chr', True, if_gr, True, False, broken_bin)
    else:
        # all fs-hadrons
        edges_np, p_sig_np, p_hst_np = plot_mc(ax1, p_path_np, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
        edges_np, h_sig_np, h_hst_np = plot_mc(ax1, h_path_np, '', '-', R, alpha, pTL, pTR, 'chr_w_all_in', False, if_gr, False)
 
 
        edges_np, s_sig_np, s_hst_np, s_err_m_np, s_err_p_np = plot_mc_f(ax1, s_path_np, 'blue', '--', R, alpha, pTL, pTR, 'chr_w_all_in', True, if_gr, True, False, broken_bin)

 
    # Resummed
    path_res     = '../data_R{}_Daniel/'.format( str(R) )
    
    #                                                                                                        if_ch_bins if_show
    edges_r, r_sig, r_hist, r_err_mns, r_err_pls = plot_res(ax1, path_res, '', '', R, alpha, pTL, pTR, if_gr, True, False)

    # Uncertainty band for the NP effects (averaged over Pythia, Herwig and Sherpa predictions)
    env_cnt, env_err_mns, env_err_pls =  plot_band_np_vs_ps(ax1, '', '', '-', edges_ps, 
                                                            p_hst_np, h_hst_np, s_hst_np, 
                                                            p_hst_ps, h_hst_ps, s_hst_ps, False)
    
    
    if edges_r != edges_np or edges_np != edges_ps:
        print ('ERROR: BINS ARE DIFFERENT!')
 
    # Get bin centers (needed to plot $(\lambda/sigma)(d\sigma/d\lambda)$ distributions )
    bins_c = []
    
    for i, el in enumerate(edges_r[:-1]):
        bins_c.append( 0.5 * (edges_r[i + 1] + edges_r[i]) )

    # Now multiply central value of $d\sigma_{res} / d\lambda$ distribution
    # by the central value of the NP / PS envelope
    # and combine errors in quadratures
    h_r_np_cnt = []
    
    err_r_np_mns = []
    err_r_np_pls = []
    
    for h1, em1, ep1, h2, em2, ep2 in zip(r_hist,  r_err_mns,   r_err_pls, 
                                          env_cnt, env_err_mns, env_err_pls):
        c_val = h1 * h2
        
        err_mns = c_val * np.sqrt( m.pow(em1 / h1, 2) + m.pow(em2 / h2, 2) )
        err_pls = c_val * np.sqrt( m.pow(ep1 / h1, 2) + m.pow(ep2 / h2, 2) )
        
        h_r_np_cnt.append(c_val)
        
        err_r_np_mns.append(err_mns)
        err_r_np_pls.append(err_pls)
    
    
    # Scale  the central value and uncertainties by a factor $\lambda/\sigma_{res}$
    h_fn = []

    err_fn_mns = []
    err_fn_pls = []
    
    for x, y, em, ep in zip(bins_c, h_r_np_cnt, err_r_np_mns, err_r_np_pls):
        h_fn.append(x * y / r_sig)
        
        err_fn_mns.append(x * em / r_sig)
        err_fn_pls.append(x * ep / r_sig)
    
    # Plot final central value  (multiplied by $\lambda/sigma_{res}$)
    ax1.hist(bins_c, bins=edges_r, weights=h_fn, color='red', linestyle='-', linewidth=6, histtype= 'step')
    
    # Plot errors (multiplied by $\lambda/sigma_{res}$)
    plot_boxes(ax1, 'red', edges_r, h_fn, err_fn_mns, err_fn_pls, True)

    #  Plot the CMS data
    path_cms     = '../cms_prelim/'
    
    #                                                                                                           if_ch_bins 
    edges_cms, cms_sig, cms_hist, cms_err_mns, cms_err_pls = plot_cms(ax1, path_cms, R, alpha, pTL, pTR, if_gr, True)


    # Cast errors on the ratio plots
    # The CMS dist are already scaled to unity so we work with $(1/sigma)(d\sigma/d\lambda)$
    cms_hst_fn = []
    
    cms_err_fn_mns = []
    cms_err_fn_pls = []
    
    for x, y, em, ep in zip(bins_c, cms_hist, cms_err_mns, cms_err_pls):
        cms_hst_fn.append(x * y)
        
        cms_err_fn_mns.append(x * em)
        cms_err_fn_pls.append(x * ep)
        


    # Errors around ratio
    plot_ratio (ax2, 'red', '', '-', edges_r,  h_fn, cms_hst_fn)

    
    # Errors around unity
    plot_errors_rto(ax2, 'red', 'black', edges_r, 
                                         h_fn,       err_fn_mns,     err_fn_pls,
                                         cms_hst_fn, cms_err_fn_mns, cms_err_fn_pls)

 
    
    # Cast errors on the ratio plots
    # Sherpa
    # Scale the central value to unity and plot $(\lambda/sigma)(d\sigma/d\lambda)$
    sig_sh_fn = np.sum( s_hst_np * np.diff(edges_r) )

    s_hst_fn = []
    
    s_err_fn_mns = []
    s_err_fn_pls = []
    
    for x, y, em, ep in zip(bins_c, s_hst_np, s_err_m_np, s_err_p_np):
        s_hst_fn.append(x * y / sig_sh_fn)
        
        s_err_fn_mns.append(x * em / sig_sh_fn)
        s_err_fn_pls.append(x * ep / sig_sh_fn)
        

    # Errors around ratio
    plot_ratio (ax2, 'blue', '', '--', edges_r, s_hst_fn, cms_hst_fn)
 
    
    # Errors around ratio only
    plot_errors_rto_only(ax2, 'blue', edges_r, 
                                      s_hst_fn,   s_err_fn_mns,   s_err_fn_pls, 
                                      cms_hst_fn, cms_err_fn_mns, cms_err_fn_pls)
    
    
    # Output the data in the yoda style (to send it to CMS)
 
    # Set the names of the final files
    # Convert from double to string e.g 1.0 -> '10' etc
    a = get_name (alpha)

    if if_ch_tr:
        f_cms   = get_cms_name('chr', R, if_gr, a, pTL, pTR, prefix = 'THR_RES_NP_d')
        f_cms_s = get_cms_name('chr', R, if_gr, a, pTL, pTR, prefix = 'SIG_THR_RES_NP_d')
        
        f_srp   = get_cms_name('chr', R, if_gr, a, pTL, pTR, prefix = 'SRP_NP_d')
        f_srp_s = get_cms_name('chr', R, if_gr, a, pTL, pTR, prefix = 'SIG_SRP_NP_d')
    else:
        f_cms   = get_cms_name('chr_w_all_in', R, if_gr, a, pTL, pTR, prefix = 'THR_RES_NP_d')
        f_cms_s = get_cms_name('chr_w_all_in', R, if_gr, a, pTL, pTR, prefix = 'SIG_THR_RES_NP_d')
        
        f_srp   = get_cms_name('chr_w_all_in', R, if_gr, a, pTL, pTR, prefix = 'SRP_NP_d')
        f_srp_s = get_cms_name('chr_w_all_in', R, if_gr, a, pTL, pTR, prefix = 'SIG_SRP_NP_d')
 
    # RES
    # Write  d\sigma / d\lambda RES distribution correctd for NP-effects
    fObj = open(f_cms + ".dat", 'w+')
    
    fObj.write('# BEGIN PLOT\n')
    fObj.write('# END PLOT\n\n')
    
    fObj.write('# BEGIN HISTO1D\n') 
    fObj.write('LineColor=red\n')
    fObj.write('LineStyle=solid\n')
    fObj.write('Title=Z+Jet RES+NP\n')
    fObj.write('# Sigma = {:.6e} pb\n'.format(r_sig))

    fObj.write('# xlow\txhigh\tval\terrminus\terrplus\n')

    for i, el in enumerate(edges_r[:-1]):
        fObj.write("{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\n".format(edges_r[i], edges_r[i+1], h_r_np_cnt[i], err_r_np_mns[i], err_r_np_pls[i]))
    
    fObj.write('# END HISTO1D')
    fObj.close()
 
    # RES
    # Write (1 / \sigma) (d\sigma / d\lambda) RES distribution correctd for NP-effects
    fObj = open(f_cms_s + ".dat", 'w+')
    
    fObj.write('# BEGIN PLOT\n')
    fObj.write('# END PLOT\n\n')
    
    fObj.write('# BEGIN HISTO1D\n') 
    fObj.write('LineColor=red\n')
    fObj.write('LineStyle=solid\n')
    fObj.write('Title=Z+Jet RES+NP\n')
    fObj.write('# Sigma = {:.6e} pb\n'.format(r_sig))

    fObj.write('# xlow\txhigh\tval\terrminus\terrplus\n')

    for i, el in enumerate(edges_r[:-1]):
        fObj.write("{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\n".format(edges_r[i], edges_r[i+1], h_r_np_cnt[i]/r_sig, err_r_np_mns[i]/r_sig, err_r_np_pls[i]/r_sig))
    
    fObj.write('# END HISTO1D')
    fObj.close()

    # MC
    # Write d\sigma / d\lambda Sherpa MC distribution at NP level
    fObj = open(f_srp + ".dat", 'w+')
    
    fObj.write('# BEGIN PLOT\n')
    fObj.write('# END PLOT\n\n')
    
    fObj.write('# BEGIN HISTO1D\n') 
    fObj.write('LineColor=red\n')
    fObj.write('LineStyle=solid\n')
    fObj.write('Title=Z+Jet SHERPA MESP@NLO+NP\n')
    fObj.write('# Sigma = {:.6e} pb\n'.format(s_sig_np))

    fObj.write('# xlow\txhigh\tval\terrminus\terrplus\n')
    
    for i, el in enumerate(edges_np[:-1]):
        fObj.write("{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\n".format(edges_np[i], edges_np[i+1], s_hst_np[i], s_err_m_np[i], s_err_p_np[i]))
    
    fObj.write('# END HISTO1D')
    
    fObj.close()
 
    # MC
    # Write d\sigma / d\lambda Sherpa MC distribution at NP level
    fObj = open(f_srp_s + ".dat", 'w+')
    
    fObj.write('# BEGIN PLOT\n')
    fObj.write('# END PLOT\n\n')
    
    fObj.write('# BEGIN HISTO1D\n') 
    fObj.write('LineColor=red\n')
    fObj.write('LineStyle=solid\n')
    fObj.write('Title=Z+Jet SHERPA MESP@NLO+NP\n')
    fObj.write('# Sigma = {:.6e} pb\n'.format(s_sig_np))

    fObj.write('# xlow\txhigh\tval\terrminus\terrplus\n')
    
    for i, el in enumerate(edges_np[:-1]):
        fObj.write("{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\t{:.6e}\n".format(edges_np[i], edges_np[i+1], s_hst_np[i]/sig_sh_fn, s_err_m_np[i]/sig_sh_fn, s_err_p_np[i]/sig_sh_fn))
    
    fObj.write('# END HISTO1D')
    
    fObj.close()
#######################################################################################
# Master plot
# Set layout and cosmetics
def plot_all(R, pTL, pTR, alpha, if_SD, if_ch_tr, y_min, y_max, y_step, r_min, r_max, r_step, X, Y, X1, Y1):
    plt.subplots(1, 1, sharex=True, figsize=(30., 30.), dpi=100)

    # make outer gridspec
    # define relative widths
    w1 = 1.     # width of the first bin
    w12 = 0.25  # width of the empty ('broken') space between the first bin and the rest
    w2 = 20.    # width of the rest of the plot (these are the main bins)
    
    # needed to set the slope of the slanted lines
    ratio_w21 = w2 / w1

    all_widths = [w1, w12, w2] 
    
    # define relative heights 
    h1 = 2  # height of the plot with histograms
    h2 = 1  # height of the ratio plot

    # needed to set slope of the slanted lines
    ratio_h21 = h2 / h1
    
    all_heights = [h1, h2]

    # Main grid (canvas)
    # Adjust left, bottom, right and top parameters to remove unwated white spaces
    outer = gridspec.GridSpec(2, 3,  width_ratios  = all_widths, 
                                     height_ratios = all_heights, wspace=0., hspace=0.,
                                     left=0.12, bottom=0.11, right=0.95, top=0.94)

    # make nested gridspecs
    # first row (hist)
    # array of axes for the first row; ax1[0] -> first bin, ax1[1] -> the rest of the bins
    ax1 = []    
    
    # same as above but for the ratios
    ax2 = []
   
    # Loop  over the main grid and fill in arrays of axes
    # we skip outer[1] and outer[4] to create line breaks!
    # first row
    for i in [0, 2]:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[i])

        ax1.append( plt.subplot(gs[0,0]) )

    for i in [3, 5]:
        gs = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[i])

        ax2.append( plt.subplot(gs[0,0]) )

    # Set axis breaks
    axes_break(ax1[0], ax1[1], ratio_w21)
    axes_break(ax2[0], ax2[1], ratio_w21, ratio_h21)

    # Draw a horizontal line to guide the eye
    for ax in ax2:
        ax.plot([0,1], [1,1], linewidth = 4, linestyle='-', color = 'black')

    # Set x-axes
    # Ranges have to be adjusted manually
    # First bins
    x_ax = []
    
    if alpha == 0.5:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 1.0:
        x_ax = [1e-3, 1e-2, 1]
    elif alpha == 2.0:
        x_ax = [1e-4, 1e-3, 1]
    
    set_x_axis (ax1[0],  x_ax, False)
    set_x_axis (ax2[0],  x_ax, False)
        
    # Other bins (the main part of the plot)
    # use, for example, delta = 5.001e-2 instead of  5e-2  to avoid an overlap between slanted lines and minor ticks on x-axis
    
    # Upper row
    if alpha == 0.5:
        set_logx_axis (ax1[1],  5.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  5.001 * x_ax[1], 1)
    elif alpha == 1.0:
        set_logx_axis (ax1[1],  1.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  1.001 * x_ax[1], 1)
    elif alpha == 2.0:
        set_logx_axis (ax1[1],  2.001 * x_ax[1], 1, False)
        set_logx_axis (ax2[1],  2.001 * x_ax[1], 1)
    
    # Set y-axes
    # Set range and step for ticks on y-axes (for histo plots)
    y_ax_h = [y_min, y_max, y_step]
    
    # Set range and step for ticks on y-axes (for ratio plots)
    y_ax_r = [r_min, r_max, r_step]
    
    # First bin 
    set_y_axis(ax1[0], y_ax_h)
    set_y_axis(ax2[0], y_ax_r)
    
    # Other bins 
    set_y_axis(ax1[1], y_ax_h, False)
    set_y_axis(ax2[1], y_ax_r, False)

    # Set font size 
    # Needed to make some labels a bit bigger
    # Has to be adjusted manually
    f_size=100

    # Name y-axes
    ax1[0].set_ylabel ('$(\lambda_{\\rm bc} / \sigma) \, d\sigma / d\lambda$', fontsize=f_size)

    ax2[0].set_ylabel ('$\\mathrm{Theory \,/\, Data}$')
    
    # Set lables for the lower x-axis
    if alpha == 0.5:
        ax2[1].set_xlabel  ('$\lambda^1_{1/2} \,\, [\mathrm{LHA}]$',  fontsize=f_size)
    elif alpha == 1.0:
        ax2[1].set_xlabel  ('$\lambda^1_{1} \,\, [\mathrm{Width}]$',  fontsize=f_size)
    elif alpha == 2.0:
        ax2[1].set_xlabel  ('$\lambda^1_{2} \,\, [\mathrm{Thrust}]$', fontsize=f_size)

    # Hide unwated ticks for x- and y-axes
    for i, j in zip(ax1, ax2):
        hide_ticks(i)
        hide_ticks(j)
    
    # Actuall plotting starts here
    plot_dat_vs_thr (ax1[0], ax2[0], pTL, pTR, R, alpha,  if_SD, if_ch_tr)
    plot_dat_vs_thr (ax1[1], ax2[1], pTL, pTR, R, alpha,  if_SD, if_ch_tr)


    # Set Legend
    set_custom_legend(ax1[1], X, Y)
    
    # Set annotation
    if if_SD:
        if if_ch_tr:
            if R == 8:
                ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.8$\n' + '$\mathrm{Chr. \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
            else:
                ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.4$\n' + '$\mathrm{Chr. \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
        else:
            if R == 8:
                ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.8$\n' + '$\mathrm{All \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
            else:
                ax1[1].annotate('$\mathrm{Groomed}$\n' + '$R_0 = 0.4$\n' + '$\mathrm{All \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
    else:
        if if_ch_tr:
            if R == 8:
                ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.8$\n' + '$\mathrm{Chr. \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
            else:
                ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.4$\n' + '$\mathrm{Chr. \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
        else:
            if R == 8:
                ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.8$\n' + '$\mathrm{All \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
            else:
                 ax1[1].annotate('$\mathrm{Ungroomed}$\n' + '$R_0 = 0.4$\n' + '$\mathrm{All \, hadrons}$',
                                xy=(X1, Y1), xycoords='axes fraction',
                                size=90,
                                bbox=dict(boxstyle="round", fc="1"))
 
    # Add some text on top of each column
    tit = r' $p_{} \in [{}, {}]$'.format("{T, \, \\rm jet}", str(pTL), str(pTR))  + r' $\mathrm{GeV}$'
    
    ax_tw = ax1[1].twiny()
    ax_tw.set_xticks([])
    ax_tw.set_xlabel (tit, fontsize=f_size, labelpad=40)

    # Hide unwanted axes
    ax_tw.spines['left'].set_visible(False)
    ax_tw.spines['top'].set_visible(False)
    
    # Hide unwanted ticks 
    ax1[1].tick_params(axis = "y", which = "both", left = False)
    ax2[1].tick_params(axis = "y", which = "both", left = False)

    
    # Save your nice plots!
    pp.savefig()
###########################################################################################
pT_bins = [50, 65]

R = 4

# Loop ver pT bins. Make plots
i = 0
while i < len(pT_bins) - 1:
    pTL = pT_bins[i]
    
    pTR = pT_bins[i + 1]
 
    print ('pT in [', str(pTL), ', ', str(pTR), '] GeV')

    
    # Adjusted for pT[50, 65] interval  (all fs-hadrons)
    #                          SD    CH TR  y_min  y_max y_step r_min  r_max r_step legend pos text pos
    plot_all(R, pTL, pTR, 0.5, False, False, 0,    2.0,  0.2,   0.0,   3,    0.5,   0.00, 0.80, 0.00, 0.50)
    plot_all(R, pTL, pTR, 0.5, True,  False, 0,    1.50, 0.1,   0.0,   3,    0.5,   0.00, 0.80, 0.00, 0.50)
    
    plot_all(R, pTL, pTR, 1.0, False, False, 0,    1.1,  0.1,   0.5,   2.0,  0.25,  0.00, 0.80, 0.00, 0.50)
    plot_all(R, pTL, pTR, 1.0, True,  False, 0,    0.80, 0.10,  0.5,   2,    0.25,  0.00, 0.80, 0.00, 0.50)
    
    plot_all(R, pTL, pTR, 2.0, False, False, 0,    1.0,  0.1,   0.0,   2.5,  0.5,   0.00, 0.80, 0.73, 0.75)
    plot_all(R, pTL, pTR, 2.0, True,  False, 0,    0.55, 0.05,  0.5,   1.75, 0.25,  0.00, 0.80, 0.00, 0.50)
    
    
    # Adjusted for pT[50, 65] interval  (charged fs-hadrons)
    #                          SD     CH TR  y_min  y_max y_step r_min  r_max r_step legend pos text pos
    plot_all(R, pTL, pTR, 0.5, False, True, 0,    2.0,  0.2,   0.0,   3,    0.5,   0.00, 0.80, 0.00, 0.5)
    plot_all(R, pTL, pTR, 0.5, True,  True, 0,    1.50, 0.1,   0.0,   3,    0.5,   0.00, 0.80, 0.00, 0.5)
    
    plot_all(R, pTL, pTR, 1.0, False, True, 0,    1.1,  0.1,   0.5,   2.0,  0.25,  0.00, 0.80, 0.00, 0.5)
    plot_all(R, pTL, pTR, 1.0, True,  True, 0,    0.80, 0.10,  0.5,   2,    0.25,  0.00, 0.80, 0.00, 0.5)
    
    plot_all(R, pTL, pTR, 2.0, False, True, 0,    1.0,  0.1,   0.0,   2.5,  0.5,   0.00, 0.80, 0.00, 0.5)
    plot_all(R, pTL, pTR, 2.0, True,  True, 0,    0.55, 0.05,  0.5,   1.75, 0.25,  0.00, 0.80, 0.00, 0.5)
    
    
    
    i += 1



pp.close()
