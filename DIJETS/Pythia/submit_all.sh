#!/usr/bin/bash


# Basic range in for loop


seed=$1
while [ $seed -le $2 ]; do
	run_name="run$seed"
    	cd $run_name/

		bsub  -P c7 -q $3 ./run.sh

    	cd ..

	seed=$(($seed+1))
done

