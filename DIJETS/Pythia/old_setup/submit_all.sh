#!/usr/bin/bash


# Basic range in for loop

max_seed=$(($1 + $2)) 

i=$2
while [ $i -lt $max_seed ]; do
	run_name="run$i"
    	cd $run_name/

		bsub  -P c7 -q $3 ./run.sh

    	cd ..

	i=$(($i+1))
done

