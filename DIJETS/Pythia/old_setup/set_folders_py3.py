#!/usr/bin/python3
import os

import os
import sys

init_seed = int(sys.argv[1])
n_runs = int(sys.argv[2])

i = 0
while i < n_runs:
    
    seed = init_seed + i
    folder_name = "run" + str(seed) 
    
    print("Make folder " + folder_name, end=", ")
    os.makedirs(folder_name)
    
    os.system("./make_runcard.py " + folder_name + "/ " + str(seed))
    print("write Runcard", end=", ")

    os.system("cp test " + folder_name + "/")  
    print("copy test", end=", ")
    
    path = folder_name + "/run.sh"
    fileObj = open(path, "w+")
    fileObj.write("#!/bin/bash\n")
    fileObj.write("./test\n")
    fileObj.write("mv outputfile.yoda ../plots/sim_{}.yoda\n".format(seed)) 
    fileObj.write("rm test")
    os.system("chmod +x " + folder_name + "/run.sh") 
    print("create run.sh", end="")

    i = i + 1
    print("...done!")


