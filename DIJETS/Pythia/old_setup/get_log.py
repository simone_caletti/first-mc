#!/usr/bin/python3

import os
import sys
import csv

folder_name = 'plots' #change name here if you store your YODA in a different folder

######################################################################################
#check_jobs function. Output for Oleh's scripts.

def n_jobs_log (path, files, outFile):

    f_count = len(files)
    info  = path + '	Number of Files = ' + str(f_count) + '\n'

    print(info)
    outFile.write(info)

    return None
    
########################################################################################
# MAIN

#ask for run input
n_runs = int(sys.argv[1])
init_seed = int(sys.argv[2])
path, dirs, files = next(os.walk(folder_name))
last_seed = init_seed + n_runs

#get a complete log in CSV format
db = open("complete_log.csv", "w", newline="")
writer = csv.writer(db) 

print("Writing the complete log...", end="")

for seed in range(init_seed, last_seed, 1):

    FILE = {}
    #Pythia
    yoda = "sim_" + str(seed) + ".yoda" 
    #Herwing
    # yoda = "LHC-DIJET-S{}_sim{}.yoda".format(str(seed), str(seed))
    # Sherpa (put here below sherpa naming convention)    

    FILE["filename"] = yoda
    FILE["seed"] = seed
    if yoda in files:
        FILE["success"] = True
    elif yoda not in files:
        FILE["success"] = False
    
    if seed == init_seed:
        writer.writerow(FILE.keys())
    writer.writerow(FILE.values())

print("done!")

db.close()

#get Oleh log
print("\nQuick log:")
outFile  = open("n_jobs_log.dat", "w+")
n_jobs_log(path, files, outFile)
outFile.close()





