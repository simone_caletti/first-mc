#!/usr/bin/python2.7
import os

import os
import sys

n_events = int(sys.argv[1])

n_runs = int(sys.argv[2])

init_seed = int(sys.argv[3])

mode =  str(sys.argv[4])

i = 0
while i < n_runs:
    
    seed = init_seed + i
    folder_name = "run" + str(seed) 
    
    print "Make folder", folder_name
    
    os.makedirs(folder_name)
    
    os.system("cp test " + folder_name + "/")  
    
    path = "run" + str(seed) + "/run.sh"

    fileObj = open(path, "a")
    fileObj.write("#!/bin/bash" + "\n")
    
    fileObj.write("./test " + str(n_events) + "   " + str(seed) + "   " + mode + "    sim_" + str(seed) + "\n")

    fileObj.write("mv *.yoda* ../plots/" + "\n")
    
    fileObj.write("rm test")

    os.system("chmod +x " + folder_name + "/run.sh") 

    i = i + 1


