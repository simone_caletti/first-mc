#include <fstream>
#include <string>
#include <string>

#include "Pythia8/Pythia.h"

#include "Pythia8Plugins/Pythia8Rivet.h" 

using namespace Pythia8;

int main(int argc, char *argv[]) 
{
    
    //Set Pythia
    Pythia pythia;
 
    //Set parameters
    pythia.settings.addFlag("Main:runRivet",false);
    pythia.settings.addWVec("Main:analyses",vector<string>());
    
    //Read input from Runcard
    pythia.readFile("Run_card.cmnd");

    //Read parameters
    int nEvent = pythia.mode("Main:numberOfEvents");;
    const bool runRivet = pythia.flag("Main:runRivet");
    const vector<string> rAnalyses = pythia.settings.wvec("Main:analyses");

    // Create a pipe between PYTHIA and RIVET
    Pythia8Rivet rivet (pythia, "outputfile.yoda");
      for(int i = 0, N = rAnalyses.size(); i < N; ++i){
    string analysis = rAnalyses[i];
    size_t pos = analysis.find(":");
    // Simple case, no analysis parameters.
    if(pos == string::npos)
      rivet.addAnalysis(analysis);
    else {
      string an = analysis.substr(0,pos);
      analysis.erase(0, pos + 1);
      pos = analysis.find(":");
      string par = analysis.substr(0,pos);
      size_t pos2 = par.find("->");
      if (pos2 == string::npos){
         cout << "Error in main93: malformed parameter " << par << endl;
      }
      string pKey = par.substr(0,pos2);
      string pVal = par.substr(pos2+2,par.length());
      rivet.addAnalysis(an+":"+pKey+"="+pVal);
        }
    }

    //Initialise Pythia
    pythia.init();

    if (!runRivet && rAnalyses.size() > 0 )
    cout << "Warning: Rivet analyses initialized, but runRivet "
         << "set to off." << endl;
    // Loop over events.
    for ( int iEvent = 0; iEvent < nEvent; ++iEvent ) {
        if ( !pythia.next() ) continue;
        if (runRivet) rivet();
        }

    rivet.done(); 
    
    return 0;
}

