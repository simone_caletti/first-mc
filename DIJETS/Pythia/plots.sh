#!/usr/bin/bash

echo -n "Moving log files and the Run card..."
mv complete_log.csv plots/
mv n_jobs_log.dat plots/
mv run1/Run_card.cmnd plots/
echo "done!"

cd plots
ls
echo "Merging all the YODAs..."
yodamerge sim* -o merge.yoda
echo "done!"

cd ..
