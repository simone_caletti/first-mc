#!/usr/bin/python3

import os
import sys

################################################################
# Define your physical framework

prj_name = "Dijet_50pt_ps"

n_events = 1000000
mode = "PS"
processes = ["HardQCD:all"]
ptmin = 30

analyses = ["PAPER_Dijet_R4", "PAPER_Dijet_CHB_R4", "PAPER_Dijet_R8", "PAPER_Dijet_CHB_R8"]

#################################################################
# Write functions

def write_header(outFile):
    outFile.write("! {}.\n".format(prj_name))
    outFile.write("! This file contains commands to be read in for a Pythia8 run.\n")
    outFile.write("! Lines not beginning with a letter or digit are comments.\n")
    outFile.write("! Names are case-insensitive  -  but spellings-sensitive!\n")
    outFile.write("! The settings here are illustrative, not always physics-motivated.\n")

def write_nEvents(outFile, n_events):
    outFile.write("\n# Number of events.\n")
    outFile.write("Main:numberOfEvents = " + str(n_events) + "\n")

def write_Beams(outFile, ecm):
    outFile.write("\n# Collision energy.\n")
    outFile.write("Beams:frameType = 1\n")
    outFile.write("Beams:eCM = " + str(ecm) + "\n")

def write_Processes(outFile, processes):
    outFile.write("\n# Hard process.\n")
    for process in processes:
        outFile.write(process + " = on\n")

def write_Mode(outFile, mode):
    outFile.write("\n# Mode.\n")
    if(mode == "PS"):
        outFile.write("PartonLevel:ISR = on\n")
        outFile.write("PartonLevel:FSR = on\n")
        outFile.write("PartonLevel:MPI = off\n")
        outFile.write("HadronLevel:all = off\n")

    elif(mode == "PS_HAD"):
        outFile.write("PartonLevel:ISR = on\n")
        outFile.write("PartonLevel:FSR = on\n")
        outFile.write("PartonLevel:MPI = off\n")
        outFile.write("HadronLevel:all = on\n")
    
    elif(mode == "PS_MPI_HAD"):
        outFile.write("PartonLevel:ISR = on\n")
        outFile.write("PartonLevel:FSR = on\n")
        outFile.write("PartonLevel:MPI = on\n")
        outFile.write("HadronLevel:all = on\n")

def write_pTHatMin(outFile, ptmin):
    outFile.write("\n# Minimum pT cut.\n")
    outFile.write("PhaseSpace:pTHatMin = " + str(ptmin) + "\n") 

def write_Seed(outFile, seed):
    outFile.write("\n# Random seed.\n")
    outFile.write("Random:setSeed = on\n")
    outFile.write("Random:seed = " + str(seed) + "\n")

def write_footer(outFile):
    outFile.write("\nPartonLevel:Remnants = on\n")
    outFile.write("Check:event = on\n")

def write_Rivet(outFile, analyses):
    outFile.write("\n# Run Rivet analyses.\n")
    outFile.write("Main:runRivet = on\n")
    outFile.write("Main:analyses = " + ",".join(analyses) + "\n")

def write_Log(outFile):
    outFile.write("\n# Put all printed output to a log file.\n")
    outFile.write("Main:outputLog = on\n")

def write_HepMC(outFile):
    outFile.write("\n# Write .hepmc events to a file.\n")
    outFile.write("Main:writeHepMC = on\n")

def write_Root(outFile):
    outFile.write("\n# Write particle level output to a root file.\n")
    outFile.write("Main:writeRoot = on\n")

############################################################################
# make Runcard

def make_runcard(path, seed):
    if len(sys.argv) != 3:
        print("Error: wrong number of arguments!")
        sys.exit()

    outFile = open(path + "Run_card.cmnd", "w+")
    write_header(outFile)

    write_nEvents(outFile, n_events)
    write_Beams(outFile, 13000.)
    write_Seed(outFile, seed)
    write_pTHatMin(outFile, ptmin)
    write_Processes(outFile, processes)
    write_Mode(outFile, mode)

    write_footer(outFile)

    write_Rivet(outFile, analyses)

    outFile.close()

###############################################################################
# MAIN

init_seed = int(sys.argv[1])
last_seed = int(sys.argv[2])

n_runs = last_seed - init_seed

seed = init_seed
while seed <= last_seed:
    
    folder_name = "run" + str(seed) 
    
    print("Make folder " + folder_name, end=", ")
    os.makedirs(folder_name)
    
    #os.system("./make_runcard.py " + folder_name + "/ " + str(seed))
    make_runcard(folder_name + "/", seed)
    print("write Runcard", end=", ")

    os.system("cp test " + folder_name + "/")  
    print("copy test", end=", ")
    
    path = folder_name + "/run.sh"
    fileObj = open(path, "w+")
    fileObj.write("#!/bin/bash\n")
    fileObj.write("./test\n")
    fileObj.write("mv outputfile.yoda ../plots/sim_{}.yoda\n".format(seed)) 
    fileObj.write("rm test")
    os.system("chmod +x " + folder_name + "/run.sh") 
    print("create run.sh", end="")

    seed += 1
    print("...done!")


