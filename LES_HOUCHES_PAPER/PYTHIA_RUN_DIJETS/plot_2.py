#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

x_data = []
y_data = []
z_data = []

filename = input('Filename : ')
#filename = '100_500_fin.dat'
x_data.append(np.genfromtxt(filename, dtype=float, usecols=(0)))
y_data.append(np.genfromtxt(filename, dtype=float, usecols=(1)))
z_data.append(np.genfromtxt(filename, dtype=float, usecols=(2)))

for i in range(len(x_data)):
    plt.plot(x_data[i], y_data[i], marker='.')
    plt.plot(x_data[i], z_data[i], marker='.')

plt.ylabel('channel fractions')
plt.xlabel('pT')
plt.legend(['gg->gg', 'qq->gg'])

plt.show()

