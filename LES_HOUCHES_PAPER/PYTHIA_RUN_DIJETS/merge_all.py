#!/usr/bin/python2.7
import sys
####################################
def extract_data (fileName, pT_bin, pT_sig,  pT_min, pT_max):
    infile = open(fileName, 'r')
    for line in infile:
        words = line.split()
        
        if len(words) == 5:
            if float(words[2]) >= pT_min and float(words[2]) < pT_max:
                pT_bin.append( float(words[2]) )
                pT_sig.append( float(words[3]) )
                
    infile.close()
####################################
n_runs = int(sys.argv[1])

pT_min  = float(sys.argv[2])
pT_max  = float(sys.argv[3])
pt_step = float(sys.argv[4])

pT_bin = []
pT_sig = []

for i in range(1, n_runs + 1):
    #print i
    
    fName = 'run' + str(i) + '/output.dat'
    
    print fName
    
    extract_data(fName, pT_bin, pT_sig,  pT_min, pT_max)
    
    pT_min = pT_max
    pT_max += pt_step
    

#for i, j in zip(pT_bin, pT_sig):
    #print i, j 

print 'Now writing to file...'


f = open("pythia_merged.dat","w")    

for i, j in zip(pT_bin, pT_sig):
    line  = str(i) + '  ' + "{:.5e}".format(j) + ' pb\n'
    
    f.write(line)

f.close()
