#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys

#initialize variables
filenames = []

x_data = []
y_data = []
y_sum = 0
line = ''

#take inputs from thet terminal
n_file = int(sys.argv[1])
for i in range(n_file):
    filenames.append(str(sys.argv[i+2]))
if len(sys.argv) != n_file+2:
    print('Error: wrong number of argument.')

#Read data from the raw files (merge_all.py outputs)
for i in range(n_file):
    x_data.append(np.genfromtxt(filenames[i], dtype=float, usecols=(0)))
    y_data.append(np.genfromtxt(filenames[i], dtype=float, usecols=(1)))

#Lenght safety check
for i in range(n_file-1):
    if len(y_data[i]) != len(y_data[i+1]):
        print('Error: different lenght for at least one file.')

#Channels fraction calculations
for j in range(len(y_data[0])):
    y_sum = 0
    for i in range(n_file):
        y_sum += y_data[i][j]
    for i in range(n_file):
        y_data[i][j] = y_data[i][j] / y_sum

#Output in a dat file (if request)
file = open('ready2plot.dat', 'w')
line = 'pT'
for filename in filenames:
    line += '          ' + filename
file.write(line + '\n')
for j in range(len(x_data[0])):
    line = str(x_data[0][j])
    for i in range(n_file):
        line += ' ' + str(y_data[i][j])
    file.write(line + '\n')
file.close()



