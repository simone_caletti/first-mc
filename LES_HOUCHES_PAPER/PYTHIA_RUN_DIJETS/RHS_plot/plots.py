#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys

#initialize variables
filenames = []
data = []

#take inputs from thet terminal
ready2plot = str(sys.argv[1])
if len(sys.argv) != 2:
    print('Error: wrong number of argument.')

#extract filenames from the first line and 
#count columns
infile = open(ready2plot, 'r')
filenames = infile.readlines(1)[0].split()
filenames.remove('pT')
n_file = len(filenames)

#Read data from the ready2plot file (channels_fraction.py output)
for i in range(n_file+1):
    data.append(np.genfromtxt(ready2plot, dtype=float,skip_header=1, usecols=(i)))

#Print plot
for i in range(1, n_file+1):
    plt.plot(data[0], data[i], marker='.')

plt.ylabel('channels fraction')
plt.xlabel('pT')
plt.legend(filenames)
plt.grid(linestyle='--', linewidth=1)
plt.show()