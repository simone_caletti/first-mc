#include <fstream>
#include <string>
#include <string>

#include "Pythia8/Pythia.h"

#include "Pythia8Plugins/Pythia8Rivet.h" 

using namespace Pythia8;
//---------------------------------------------------------------------------------------------------------------------------
void set_pythia (Pythia &pythia, int seed, std::string mode, std::string pt_min_cut, std::string pt_max_cut) 
{
    //pdf settings
    pythia.readString("PDF:pSet = LHAPDF6:MSTW2008lo68cl");
    pythia.readString("PDF:pSet = LHAPDF6:CT14nlo");	

    //Common settings
    // Seed 
    pythia.readString("Random:setSeed = on");

    pythia.readString("Random:seed = " + std::to_string(seed));
    
    pythia.readString("Beams:frameType = 1");
    pythia.readString("Beams:eCM = 13000.");
 
    pythia.readString("PhaseSpace:pTHatMin  = " + pt_min_cut);
    pythia.readString("PhaseSpace:pTHatMax  = " + pt_max_cut);

    //  Processes 
    //pythia.readString("HardQCD:gg2gg = on");
    pythia.readString("HardQCD:qqbar2gg  = on");
    //pythia.readString("HardQCD:gg2qqbar  = on");
    //pythia.readString("HardQCD:qg2qg  = on");
    //pythia.readString("HardQCD:qq2qq  = on");
    //pythia.readString("HardQCD:gg2qqbar  = on");
    //pythia.readString("HardQCD:qqbar2qqbar  = on");

    // Force decay Z -> e+-, mu+-
    //pythia.readString("23:onMode = off");
    //pythia.readString("23:onIfAny = 11 13");
    
    // Process-level
    if (mode == "OFF") {
        pythia.readString("PartonLevel:ISR = off");
        pythia.readString("PartonLevel:FSR = off");

        pythia.readString("PartonLevel:MPI = off");
        pythia.readString("HadronLevel:all = off");
    }

    else if (mode == "PS") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = off");
        pythia.readString("HadronLevel:all = off");
    }
    else if (mode == "PS_HAD") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = off");
        pythia.readString("HadronLevel:all = on");
    }
    else if (mode == "PS_MPI_HAD") {
        pythia.readString("PartonLevel:ISR = on");
        pythia.readString("PartonLevel:FSR = on");

        pythia.readString("PartonLevel:MPI = on");
        pythia.readString("HadronLevel:all = on");
    }
    else {
        std::cout << "Wrong settings!\n";
        
        abort();
    }
    
    pythia.readString("PartonLevel:Remnants  = on");
    pythia.readString("Check:event = on");
}
//---------------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[]) 
{
    // Get argumets from command line ( <n_of_events> <seed> <mode> <pt_min_cut>  <pt_max_cut> <sim_name>)
    if (argc != 7) 
    {
        std::cout << "Wrong number of argumenst " << argc << " != 7 is provided!\n";
    
        abort();
    }
    
    int n_event = std::stoi(argv[1]);
    
    int seed = std::stoi(argv[2]);
    
    std::string mode = argv[3];
    
    std::string pt_min_cut = argv[4];
    
    std::string pt_max_cut = argv[5];
    
    std::string yoda_name = argv[6];
    
    //Set Pythia
    Pythia pythia;
    
    set_pythia (pythia, seed, mode, pt_min_cut, pt_max_cut);
 
    pythia.init();
    
    // Create a pipe between PYTHIA and RIVET
    Pythia8Rivet rivet (pythia, yoda_name + ".yoda"); 
    
    // Set name of the RIVET analysis you use  
    rivet.addAnalysis("DIJETS_LES_HOUCHES_2019");
    //rivet.addAnalysis("name");

    // Loop over all events
    for (int iEvent = 0; iEvent < n_event; ++iEvent) 
    {
        // Check if the event is not broken
        if (!pythia.next()) continue; 
        
        // If event is OK pass it to the RIVET
        rivet(); 
    } 
 
    // End of job. Create YODA files
    rivet.done(); 
    
    return 0;
}

