#!/usr/bin/python2.7
import os
import sys

n_events = int(sys.argv[1])

n_runs = int(sys.argv[2])

mode = str(sys.argv[3])

pt_min_cut = float(sys.argv[4])
pt_max_cut = float(sys.argv[5])
pt_step    = float(sys.argv[6])


i = 1
while i <= n_runs:
    
    folder_name = "run" + str(i) 
    
    print "Make folder", folder_name
    
    os.makedirs(folder_name)
    
    os.system("cp test " + folder_name + "/") 
    
    path = "run" + str(i) + "/run.sh"

    fileObj = open(path, "a")
    fileObj.write("#!/bin/bash" + "\n")
    
    fileObj.write("./test " + str(n_events) + " " + str(i) + " " + mode + " " + str(pt_min_cut) + " "  + str(pt_max_cut) + " sim_" + str(i) + "\n")

    fileObj.write("rm test")

    os.system("chmod +x " + folder_name + "/run.sh") 

    i = i + 1

    pt_min_cut = pt_max_cut
    
    pt_max_cut += pt_step

