#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys

x_data = []
y_data = []
y_sum = 0

n_file = 3

#filenames = ['200_300.dat', '300_400.dat']
for i in range(n_file):
    filename = input('Input file name {}: '.format(i+1))
    #filename = filenames[i]
    x_data.append(np.genfromtxt(filename, dtype=float, usecols=(0)))
    y_data.append(np.genfromtxt(filename, dtype=float, usecols=(1)))
    
if len(y_data[0]) != len(y_data[1]):
    print('Error: different lenght of the two files.')
else:
    for j in range(len(y_data[0])):
        y_sum = y_data[0][j] + y_data[1][j] + y_data[2][j]
        y_data[0][j] = y_data[0][j] / y_sum
        y_data[1][j] = y_data[1][j] / y_sum
        y_data[2][j] = y_data[2][j] / y_sum
    # print(y_data[0])
    # print(y_data[1])

#Output in a dat file
output = input('Output file name: ')
#output = '200_400.dat'
file = open(output, 'w')
for j in range(len(x_data[0])):
    col1 = str(x_data[0][j])
    col2 = str(y_data[0][j])
    col3 = str(y_data[1][j])
    col4 = str(y_data[2][j])
    file.write( col1 + ' ' + col2 + ' ' + col3 + ' ' + col4 + '\n')
file.close()



