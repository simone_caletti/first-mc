#!/bin/bash

# Basic range in for loop
for i in {1..65}
do
    run_name="run$i"
    echo $run_name
    cd $run_name/
    #sbatch submit
    ./run.sh
    cd ..
done

