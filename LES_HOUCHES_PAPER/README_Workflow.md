# Workflow

## Here I want to give a recipe to obtain the Les Houches plots using the script we wrote.

0) Put in the same folder:
- pythia_dijets.cc (with Makefile and Makefile.inc)
- all the python scripts (set_folder.py, merge_all.py, channels_fraction.py, plots.py)
- the bash script submit_all.sh

Select channels you want to be active in the pythia_dijets.cc file and compile it using the command: make -j4 main

1) command: ./set_folder.py <n_events> <n_runs> <_mode> <pT_min> <pT_max> <_step>

The set_folder.py script creates n_runs different folders called 'run$i' which contain the exe file of the pyhtia file and a bash file named run.sh

where pt_min and pt_max define pT interval for the first run. For example, if you type 

./set_folders.py 1000 3 OFF 100 300 200

then the first simulation will be performed in the pT slice [100, 300) and the second one in [300, 500), the third one in [500, 700).

2) command: ./merge_all.py <n_runs> <pT_min> <pT_max> <_step>

**Important:** use the same parameters you set in the first step.

This script merge all the 'output.dat' file created by default by the pythia simulation in one file named 'pythia_merge.dat'.

Rename the file if you want to apply the procedure more than one time in order to do not overwrite the first file. For example: for the LHS plot o the Les Houches paper we have to consider only the gg2gg channel active and after only the qqbar2gg channel active. I will use this example in the following calling the two files respectively gg2gg_merge.dat and qqbar2gg_merge.dat.

3) command: python3 channels_fraction.py <n_file> <filename_1> <filename_2> ... <filename_n> 

The filename_i are the file that you merged in the previous step and n_file thus is the number of the processes contribute at the total cross section (and obviously also the number of files). 

This script compute the fraction channel bin by bin and print a new single output named by default ready2plot.dat.

It works for every number of files thus is good both for the LHS and the RHS plots. It requires only to specify n_files as the first argument.

In addition, the order of the files does not matter.

4) command: python3 plots.py <filename>

Insert the output of the previous step as the only argument of this script. You have not to specify the number of files you insert in the channels_fraction.py script and also the name of the files is automatically returned in the legend.

The script will show you the LHS or the RHS plot of the Les Houches paper depending of our previous choices.

Simone
