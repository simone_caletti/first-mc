// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Jet.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <algorithm>

// Oleh
#include <fstream>

using std::cout;
using std::endl;
using std::vector;
using namespace fastjet;


namespace Rivet {

/// @brief Routine for QG substructure analysis
  class DIJETS_LES_HOUCHES_2019: public Analysis {
  public:
    
    DEFAULT_RIVET_ANALYSIS_CTOR(DIJETS_LES_HOUCHES_2019);

    // Jet Radius
    const double _jetR = 0.4;

    
    // pT-bins
    // We have to change this binning to the Les Houches paper one.
    vector<double> _ptBinsGen;
    //= {50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800, 1850, 1900, 1950, 2000, 2050, 2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500, 2550, 2600, 2650, 2700, 2750, 2800, 2850, 2900, 2950, 3000, 3050, 3100, 3150, 3200, 3250, 3300, 3350, 3400, 3450, 3500, 3550, 3600, 3650, 3700, 3750, 3800, 3850, 3900, 3950, 4000, 4050, 4100, 4150, 4200, 4250, 4300, 4350, 4400, 4450, 4500, 4550, 4600, 4650, 4700, 4750, 4800, 4850, 4900, 4950, 5000};
    
    /// Book histograms and initialise projections before the run
    void init() 
    {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        addProjection(jet_input, "JET_INPUT");

        //Generate the pt bins automatically
        double pT_min = 100;
        while(pT_min < 2000)
        { 
               _ptBinsGen.push_back(pT_min);
                pT_min += 25;
        }

	    //Oleh
        _weights_pT.resize(_ptBinsGen.size() - 1);

        // Used to get cross section for all pT bins
        _weights_pT_all = 0.;

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
        const double weight = event.weight();

        // Convert Particles into PseudoJets for clustering
        const VetoedFinalState & fs = applyProjection<VetoedFinalState>(event, "JET_INPUT");
      
        const ParticleVector & fsParticles = fs.particles();
      
        vector<PseudoJet> particles;
      
        particles.reserve(fsParticles.size());
        
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            particles.push_back(p);
        }
        
        JetDefinition jet_def(antikt_algorithm, _jetR);
        
        vector<PseudoJet> jets = (SelectorAbsRapMax(4.7))(jet_def(particles));

        // Now do selection criteria        
        if (jets.size() < 2) vetoEvent;
        
        PseudoJet jet1 = jets[0];
        PseudoJet jet2 = jets[1];
        
        double jet1pt = jet1.pt();
        double jet2pt = jet2.pt();
        
        //pass_dijets_event = ((zpt > 30) && (asym < 0.3) && (dphi > 2.0));
        // MAybe we could introduce some useful condition check
        //if (!passZpJ) vetoEvent;

        if (jet1pt < _ptBinsGen[0]) vetoEvent;
        if (jet2pt < _ptBinsGen[0]) vetoEvent;
 
        if (jet1pt > _ptBinsGen.back()) vetoEvent;
        if (jet2pt > _ptBinsGen.back()) vetoEvent;

        // If comes here then accept event
        _weights_pT_all += weight;
        
        // Run over pT bins, get angularities, fill histograms (charged + neutral and charged only)
        // Compute weights for each pT bin
        for (unsigned int i = 0; i < _ptBinsGen.size() - 1; ++i)
        {
            double l_edge = _ptBinsGen[i];
                   
            double r_edge = _ptBinsGen[i + 1];
 
            // Run analysis per given pT bin
            if (l_edge < jet1pt && jet1pt < r_edge)
            {
                //Oleh
                _weights_pT[i] += weight;
            }
        }
    }

    /// Normalise histograms etc., after the run
    void finalize() 
    {
        // Open file 
        std::ofstream outFile;

        outFile.open("output.dat");

        // Compute cross section (in pb) after the cuts
        double sigma_tot = crossSection() / picobarn;

        double weight_tot = sumOfWeights();

        //Oleh
        for (unsigned int i = 0; i < _ptBinsGen.size() - 1; ++i)
        {
            double l_edge = _ptBinsGen[i];
                   
            double r_edge = _ptBinsGen[i + 1];

            double bin_center = (r_edge + l_edge) / 2;

            double sigma_cut = _weights_pT[i] * sigma_tot / weight_tot;
 
            outFile << "pT = " << bin_center << "          " << sigma_cut << " pb\n";
        }

 
        double sigma_cut_all = _weights_pT_all * sigma_tot / weight_tot;
 
        outFile << "sigma_cut_all = " << sigma_cut_all << " pb\n";
  		
	outFile.close();
    } 
        
    // Array of weights and corss sections (for each pT bin)
    //Oleh
    vector<double> _weights_pT;
 
    // use to get cross section for all pT bins
    double _weights_pT_all;
  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(DIJETS_LES_HOUCHES_2019);


}
