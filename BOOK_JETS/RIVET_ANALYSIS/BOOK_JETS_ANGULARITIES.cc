// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Jet.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <algorithm>


using std::cout;
using std::endl;
using std::vector;
using namespace fastjet;


namespace Rivet {

/// \class Angularity
/// definition of angularity
///
class Angularity : public FunctionOfPseudoJet<double>{
public:
  /// ctor
  Angularity(double alpha, double jet_radius, double kappa=1.0, Selector constitCut=SelectorPtMin(0.)) : _alpha(alpha), _radius(jet_radius), _kappa(kappa), _constitCut(constitCut) {}

  /// description
  std::string description() const{
    ostringstream oss;
    oss << "Angularity with alpha=" << _alpha;
    return oss.str();
  }

  /// computation of the angularity itself
  double result(const PseudoJet &jet) const{
    // get the jet constituents
    vector<PseudoJet> constits = jet.constituents();

    // get the reference axis
    PseudoJet reference_axis = _get_reference_axis(jet);

    // do the actual coputation
    double numerator = 0.0, denominator = 0.0;
    unsigned int num = 0;
    for (const auto &c : constits){
      if (!_constitCut.pass(c)) continue;
      double pt = c.pt();
      // Note: better compute (dist^2)^(alpha/2) to avoid an extra square root
      numerator   += pow(pt, _kappa) * pow(c.squared_distance(reference_axis), 0.5*_alpha);
      denominator += pt;
      num += 1;
    }
    if (denominator == 0) return -1;
    // the formula is only correct for the the typical angularities which satisfy either kappa==1 or alpha==0.
    else return numerator/(pow(denominator, _kappa)*pow(_radius, _alpha));
  }

protected:
  PseudoJet _get_reference_axis(const PseudoJet &jet) const{
    if (_alpha>1) return jet;

    Recluster recluster(JetDefinition(antikt_algorithm, JetDefinition::max_allowable_R, WTA_pt_scheme));
    return recluster(jet);
  }

  double _alpha, _radius, _kappa;
  Selector _constitCut;
};


/// \class SD_array
// A simple helper class to perform scan over different SoftDrop parameters
class SD_array 
{
public:
    SD_array () : sd_gr_b0_z005 (0.0, 0.05, 0.8),
                  sd_gr_b0_z01  (0.0, 0.10, 0.8),
                  sd_gr_b0_z015 (0.0, 0.15, 0.8),
                  sd_gr_b0_z02  (0.0, 0.20, 0.8),
                  sd_gr_b0_z025 (0.0, 0.25, 0.8),
                  sd_gr_b0_z03  (0.0, 0.30, 0.8), 
                  sd_gr_b0_z035 (0.0, 0.35, 0.8), 
                  sd_gr_b0_z04  (0.0, 0.40, 0.8), 
                      
                  sd_gr_b1_z005 (1.0, 0.05, 0.8),
                  sd_gr_b1_z01  (1.0, 0.10, 0.8),
                  sd_gr_b1_z015 (1.0, 0.15, 0.8),
                  sd_gr_b1_z02  (1.0, 0.20, 0.8),
                  sd_gr_b1_z025 (1.0, 0.25, 0.8),
                  sd_gr_b1_z03  (1.0, 0.30, 0.8), 
                  sd_gr_b1_z035 (1.0, 0.35, 0.8), 
                  sd_gr_b1_z04  (1.0, 0.40, 0.8), 
                      
                  sd_gr_b2_z005 (2.0, 0.05, 0.8),
                  sd_gr_b2_z01  (2.0, 0.10, 0.8),
                  sd_gr_b2_z015 (2.0, 0.15, 0.8),
                  sd_gr_b2_z02  (2.0, 0.20, 0.8),
                  sd_gr_b2_z025 (2.0, 0.25, 0.8),
                  sd_gr_b2_z03  (2.0, 0.30, 0.8), 
                  sd_gr_b2_z035 (2.0, 0.35, 0.8), 
                  sd_gr_b2_z04  (2.0, 0.40, 0.8) { }
protected:
    contrib::SoftDrop sd_gr_b0_z005;
    contrib::SoftDrop sd_gr_b0_z01;
    contrib::SoftDrop sd_gr_b0_z015;
    contrib::SoftDrop sd_gr_b0_z02;
    contrib::SoftDrop sd_gr_b0_z025;
    contrib::SoftDrop sd_gr_b0_z03;
    contrib::SoftDrop sd_gr_b0_z035;
    contrib::SoftDrop sd_gr_b0_z04;
    
protected:
    contrib::SoftDrop sd_gr_b1_z005;
    contrib::SoftDrop sd_gr_b1_z01;
    contrib::SoftDrop sd_gr_b1_z015;
    contrib::SoftDrop sd_gr_b1_z02;
    contrib::SoftDrop sd_gr_b1_z025;
    contrib::SoftDrop sd_gr_b1_z03;
    contrib::SoftDrop sd_gr_b1_z035;
    contrib::SoftDrop sd_gr_b1_z04;

protected:
    contrib::SoftDrop sd_gr_b2_z005;
    contrib::SoftDrop sd_gr_b2_z01;
    contrib::SoftDrop sd_gr_b2_z015;
    contrib::SoftDrop sd_gr_b2_z02;
    contrib::SoftDrop sd_gr_b2_z025;
    contrib::SoftDrop sd_gr_b2_z03;
    contrib::SoftDrop sd_gr_b2_z035;
    contrib::SoftDrop sd_gr_b2_z04;
        
public: 
    PseudoJet groomer(const double beta, const double z_cut, const PseudoJet &jet) 
    {
        PseudoJet sd_jet;
            
        if (beta == 0.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b0_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b0_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b0_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b0_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b0_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b0_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b0_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b0_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 1.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b1_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b1_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b1_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b1_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b1_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b1_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b1_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b1_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 2.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b2_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b2_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b2_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b2_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b2_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b2_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b2_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b2_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else
        {
            std::cout << "Error: bete = " << beta << " and is out of range!\n";
                    
            abort();
        }
            
        return sd_jet;
    }
};

  /// @brief Routine for QG substructure analysis
  class BOOK_JETS_ANGULARITIES : public Analysis, SD_array {
  public:
    
    DEFAULT_RIVET_ANALYSIS_CTOR(BOOK_JETS_ANGULARITIES);

    // Jet Radius
    const double _jetR = 0.8;

    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 2.;
            
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.05;
            
    // Set bins (for log10(lambda) distributions)
    const int _bin_min = -7;
            
    const int _bin_max = 0;
            
    const int _n_bins = 25;

    /// Book histograms and initialise projections before the run
    void init() 
    {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);
      
        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;
      
        ZFinder zfinder(fs,
                        Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                        PID::MUON,
                        mz_min*GeV, mz_max*GeV,
                        0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        FinalState fs_muons(-eta_max, eta_max, 0*GeV);
        IdentifiedFinalState muons_noCut(fs_muons, {PID::MUON, PID::ANTIMUON});
        addProjection(muons_noCut, "MUONS_NOCUT");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");

        // Book histograms:
        _h_alpha05 = bookHisto1D("alpha_05_ungroomed", linspace(_n_bins, _bin_min, _bin_max));
        _h_alpha10 = bookHisto1D("alpha_10_ungroomed", linspace(_n_bins, _bin_min, _bin_max));
        _h_alpha20 = bookHisto1D("alpha_20_ungroomed", linspace(_n_bins, _bin_min, _bin_max));

        _h_sd_alpha05 = bookHisto1D("alpha_05_sd", linspace(_n_bins, _bin_min, _bin_max));
        _h_sd_alpha10 = bookHisto1D("alpha_10_sd", linspace(_n_bins, _bin_min, _bin_max));
        _h_sd_alpha20 = bookHisto1D("alpha_20_sd", linspace(_n_bins, _bin_min, _bin_max));
 
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
        const double weight = event.weight();

        // Convert Particles into PseudoJets for clustering
        const VetoedFinalState & fs = applyProjection<VetoedFinalState>(event, "JET_INPUT");
      
        const ParticleVector & fsParticles = fs.particles();
      
        vector<PseudoJet> particles;
      
        particles.reserve(fsParticles.size());
        
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            p.set_user_index(el.isCharged()); // for later reference to charge
        
            particles.push_back(p);
        }
        
        JetDefinition jet_def(antikt_algorithm, _jetR);
        
        vector<PseudoJet> jets = (SelectorAbsRapMax(1.7) * SelectorPtMin(500))(jet_def(particles));

        const FinalState& muons = applyProjection<IdentifiedFinalState>(event, "MUONS_NOCUT");
        
        // Reconstruct Z
        const ZFinder& zfinder = applyProjection<ZFinder>(event, "ZFinder");
        
        if (zfinder.bosons().size() < 1) vetoEvent;

        const Particle & z = zfinder.bosons()[0];
        
        double zpt = z.pt();

        // Now do selection criteria        
        if (jets.size() < 1) vetoEvent;
        
        PseudoJet jet1 = jets[0];
        
        double jet1pt = jet1.pt();

        // Get angularities and fill the histos for the ungroomed
        _h_alpha05->fill(_get_ang(0.5, jet1), weight);
        _h_alpha10->fill(_get_ang(1.0, jet1), weight);
        _h_alpha20->fill(_get_ang(2.0, jet1), weight);

        // Run SoftDrop
        PseudoJet sd_jet = groomer(_beta, _z_cut, jet1);

        // Get angularities and fill the histos for the sd jet
        _h_sd_alpha05->fill(_get_ang(0.5, sd_jet), weight);
        _h_sd_alpha10->fill(_get_ang(1.0, sd_jet), weight);
        _h_sd_alpha20->fill(_get_ang(2.0, sd_jet), weight);

    }

    /// Normalise histograms etc., after the run
    void finalize() 
    {
        //empty
    }
        
    Histo1DPtr _h_alpha05;
    Histo1DPtr _h_alpha10;
    Histo1DPtr _h_alpha20;
    
    Histo1DPtr _h_sd_alpha05;
    Histo1DPtr _h_sd_alpha10;
    Histo1DPtr _h_sd_alpha20;

    // A helper function to compute angularities
    // if \lambda = 0 we put it in the very first bin (which should be located at -inf)
    double _get_ang(double alpha, const PseudoJet &jet) 
   {
        double res = 0.;
            
        Angularity angularity(alpha, _jetR);
                
        double lambda_tmp = angularity(jet);
        
        if (lambda_tmp == 0.) res = 0.99 * _bin_min;
        else                  res = std::log10(lambda_tmp);
            
        return res;
    }
   
   

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(BOOK_JETS_ANGULARITIES);


}
