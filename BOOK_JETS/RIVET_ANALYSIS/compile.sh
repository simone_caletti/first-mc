#!/bin/bash

prefix="/home/chiara/Simone"

# Include files

RIVET_I="${prefix}/RIVET2/include"

HEPMC2_I="${prefix}/hepmc2/include"

YODA_I="${prefix}/yoda-1.7.7/include" 

FJ_I="${prefix}/fastjet/include"

# Libs

RIVET_L="${prefix}/RIVET2/lib"

HEPMC_L="${prefix}/hepmc2/lib"

YODA_L="${prefix}/yoda-1.7.7/lib"

FJ_L="${prefix}/fastjet/lib"

echo 'Compiling plugins...'

g++ -o "RivetBOOK_JETS_ANGULARITIES.so" -w -shared -fPIC \
-I$RIVET_I -I$HEPMC2_I -I$YODA_I -I$FJ_I -pedantic -Wall \
-Wno-long-long -Wno-format -Werror=uninitialized \
-Werror=delete-non-virtual-dtor -fopenmp -O2   -Wl,--no-as-needed \
-L$RIVET_L -L$HEPMC_L -L$YODA_L -Wl,-rpath,$FJ_L -lm \
-L$FJ_L -lfastjettools -lfastjet -lfastjetplugins \
-lsiscone_spherical -lsiscone -lRecursiveTools \
-lfastjetcontribfragile -lfastjettools -lRivet\
-std=c++11 BOOK_JETS_ANGULARITIES.cc 

echo '...done!'
