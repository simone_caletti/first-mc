// Do not forget to add   -lRecursiveTools and -lfastjetcontribfragile -lfastjettools flags to compile it!
// Written by Oleh Fedkevych, oleh.fedkevych@ge.infn.it
// It uses the Angularity class by Gregory Soyez,  gregory.soyez@ipht.fr
// this analysis is using the same projections / selection cuts as in the CMS analysis by Robin Cameron Aggleton, robin.aggleton@cern.ch
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Jet.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <algorithm>

using std::vector;
using std::tuple;
using std::make_tuple;
using namespace fastjet;

namespace Rivet {

/// \class Angularity
/// definition of angularity
///
class Angularity : public FunctionOfPseudoJet<double>{
public:
  /// ctor
  Angularity(double alpha, double jet_radius, double kappa=1.0, Selector constitCut=SelectorPtMin(0.)) : _alpha(alpha), _radius(jet_radius), _kappa(kappa), _constitCut(constitCut) {}

  /// description
  std::string description() const{
    ostringstream oss;
    oss << "Angularity with alpha=" << _alpha;
    return oss.str();
  }

  /// computation of the angularity itself
  double result(const PseudoJet &jet) const{
    // get the jet constituents
    vector<PseudoJet> constits = jet.constituents();

    // get the reference axis
    PseudoJet reference_axis = _get_reference_axis(jet);

    // do the actual coputation
    double numerator = 0.0, denominator = 0.0;
    unsigned int num = 0;
    for (const auto &c : constits){
      if (!_constitCut.pass(c)) continue;
      double pt = c.pt();
      // Note: better compute (dist^2)^(alpha/2) to avoid an extra square root
      numerator   += pow(pt, _kappa) * pow(c.squared_distance(reference_axis), 0.5*_alpha);
      denominator += pt;
      num += 1;
    }
    if (denominator == 0) return -1;
    // the formula is only correct for the the typical angularities which satisfy either kappa==1 or alpha==0.
    else return numerator/(pow(denominator, _kappa)*pow(_radius, _alpha));
  }

protected:
  PseudoJet _get_reference_axis(const PseudoJet &jet) const{
    if (_alpha>1) return jet;

    Recluster recluster(JetDefinition(antikt_algorithm, JetDefinition::max_allowable_R, WTA_pt_scheme));
    return recluster(jet);
  }

  double _alpha, _radius, _kappa;
  Selector _constitCut;
};



/// \class SD_array
// A simple helper class to perform scan over different SoftDrop parameters
class SD_array 
{
public:
    SD_array () : sd_gr_b0_z005 (0.0, 0.05, 0.8),
                  sd_gr_b0_z01  (0.0, 0.10, 0.8),
                  sd_gr_b0_z015 (0.0, 0.15, 0.8),
                  sd_gr_b0_z02  (0.0, 0.20, 0.8),
                  sd_gr_b0_z025 (0.0, 0.25, 0.8),
                  sd_gr_b0_z03  (0.0, 0.30, 0.8), 
                  sd_gr_b0_z035 (0.0, 0.35, 0.8), 
                  sd_gr_b0_z04  (0.0, 0.40, 0.8), 
                      
                  sd_gr_b1_z005 (1.0, 0.05, 0.8),
                  sd_gr_b1_z01  (1.0, 0.10, 0.8),
                  sd_gr_b1_z015 (1.0, 0.15, 0.8),
                  sd_gr_b1_z02  (1.0, 0.20, 0.8),
                  sd_gr_b1_z025 (1.0, 0.25, 0.8),
                  sd_gr_b1_z03  (1.0, 0.30, 0.8), 
                  sd_gr_b1_z035 (1.0, 0.35, 0.8), 
                  sd_gr_b1_z04  (1.0, 0.40, 0.8), 
                      
                  sd_gr_b2_z005 (2.0, 0.05, 0.8),
                  sd_gr_b2_z01  (2.0, 0.10, 0.8),
                  sd_gr_b2_z015 (2.0, 0.15, 0.8),
                  sd_gr_b2_z02  (2.0, 0.20, 0.8),
                  sd_gr_b2_z025 (2.0, 0.25, 0.8),
                  sd_gr_b2_z03  (2.0, 0.30, 0.8), 
                  sd_gr_b2_z035 (2.0, 0.35, 0.8), 
                  sd_gr_b2_z04  (2.0, 0.40, 0.8) { }
protected:
    contrib::SoftDrop sd_gr_b0_z005;
    contrib::SoftDrop sd_gr_b0_z01;
    contrib::SoftDrop sd_gr_b0_z015;
    contrib::SoftDrop sd_gr_b0_z02;
    contrib::SoftDrop sd_gr_b0_z025;
    contrib::SoftDrop sd_gr_b0_z03;
    contrib::SoftDrop sd_gr_b0_z035;
    contrib::SoftDrop sd_gr_b0_z04;
    
protected:
    contrib::SoftDrop sd_gr_b1_z005;
    contrib::SoftDrop sd_gr_b1_z01;
    contrib::SoftDrop sd_gr_b1_z015;
    contrib::SoftDrop sd_gr_b1_z02;
    contrib::SoftDrop sd_gr_b1_z025;
    contrib::SoftDrop sd_gr_b1_z03;
    contrib::SoftDrop sd_gr_b1_z035;
    contrib::SoftDrop sd_gr_b1_z04;

protected:
    contrib::SoftDrop sd_gr_b2_z005;
    contrib::SoftDrop sd_gr_b2_z01;
    contrib::SoftDrop sd_gr_b2_z015;
    contrib::SoftDrop sd_gr_b2_z02;
    contrib::SoftDrop sd_gr_b2_z025;
    contrib::SoftDrop sd_gr_b2_z03;
    contrib::SoftDrop sd_gr_b2_z035;
    contrib::SoftDrop sd_gr_b2_z04;
        
public: 
    PseudoJet groomer(const double beta, const double z_cut, const PseudoJet &jet) 
    {
        PseudoJet sd_jet;
            
        if (beta == 0.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b0_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b0_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b0_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b0_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b0_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b0_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b0_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b0_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 1.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b1_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b1_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b1_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b1_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b1_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b1_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b1_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b1_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 2.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b2_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b2_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b2_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b2_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b2_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b2_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b2_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b2_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else
        {
            std::cout << "Error: bete = " << beta << " and is out of range!\n";
                    
            abort();
        }
            
        return sd_jet;
    }
};

/// @brief Routine for QG substructure analysis
class PAPER_FIN_R8 : public Analysis, SD_array {
public:
    DEFAULT_RIVET_ANALYSIS_CTOR(PAPER_FIN_R8);

    // Jet Radius
    const double _jetR = 0.8;

    // SD parameters:
    // Choose a value of beta = 0., 1., 2.
    const double _beta = 0.0;
            
    // Choose a value of z_cut = 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4
    const double _z_cut = 0.1;
            
    // Set bins (for log10(lambda) distributions)
    const int _bin_min = -7;
            
    const int _bin_max = 0;
            
    const int _n_bins = 100;
    
    // pT-bins
    const vector<double> _ptBinsGen = {50.0, 65.0, 88.0, 120.0, 150.0, 186.0, 254.0, 326.0, 408.0, 1500.0};

    //CMS
    //Ungroomed binning
    //Charged+neutral lambda variables
    
    //LHA (kappa=1, alpha=0.5) 
    const vector<double> _lhaBins = {0.0, 0.17, 0.25, 0.32, 0.38, 0.45, 0.52, 0.59, 1.0};

    //Width (kappa=1, alpha=1)
    const vector<double> _widthBins = {0.0, 0.105, 0.165, 0.23, 0.305, 0.38, 0.46, 0.55, 1.0};
    
    //Thrust (kappa=1, alpha=2)
    const vector<double> _thrustBins = {0.0, 0.05, 0.09, 0.15, 0.205, 1.0};
      
    //Charged-only lambda variables
    //LHA (kappa=1, alpha=0.5) 
    const vector<double> _lhaBinsCh = {0.0, 0.06, 0.11, 0.15, 0.19, 0.23, 0.27, 0.31, 0.35, 0.39, 0.44, 0.49, 0.54, 0.6, 1.0};
    
    //Width (kappa=1, alpha=1)
    const vector<double> _widthBinsCh = {0.0, 0.0225, 0.04, 0.0575, 0.0775, 0.1, 0.125, 0.152, 0.185, 0.22, 0.26, 0.307, 0.362, 0.425, 0.497, 1.0};
    
    //Thrust (kappa=1, alpha=2) 
    const vector<double> _thrustBinsCh = {0.0, 0.005, 0.0125, 0.0225, 0.035, 0.05, 0.07, 0.0925, 0.12, 0.152, 0.188, 0.228, 1.0};
    
    //CMS
    //Groomed binning
    //Charged+neutral lambda variables
        
    //LHA (kappa=1, alpha=0.5)
    const vector<double> _lhaBinsSD = {0.0, 0.1, 0.18, 0.26, 0.34, 0.42, 0.5, 0.57, 0.64, 1.0};
    
    //Width (kappa=1, alpha=1) 
    const vector<double> _widthBinsSD = {0.0, 0.02, 0.05, 0.095, 0.147, 0.225, 0.307, 0.388, 0.468, 1.0};
        
    //Thrust (kappa=1, alpha=2)
    const vector<double> _thrustBinsSD = {0.0, 0.0025, 0.01, 0.025, 0.06, 0.12, 0.177, 0.23, 1.0};
        
    //Charged-only lambda variables
    //LHA (kappa=1, alpha=0.5)
    const vector<double> _lhaBinsChSD = {0.0, 0.06, 0.09, 0.12, 0.15, 0.19, 0.23, 0.27, 0.32, 0.37, 0.42, 0.48, 0.54, 0.6, 1.0};
        
    //Width (kappa=1, alpha=1)
    const vector<double> _widthBinsChSD = {0.0, 0.0125, 0.0225, 0.035, 0.05, 0.07, 0.095, 0.128, 0.17, 0.225, 0.29, 0.365, 0.45, 1.0};
        
    //Thrust (kappa=1, alpha=2): 
    const vector<double> _thrustBinsChSD = {0.0, 0.0025, 0.005, 0.0075, 0.0125, 0.02, 0.0325, 0.05, 0.0775, 0.115, 0.16, 0.21, 1.0};
    
    void init() {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);
    
        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;
    
        ZFinder zfinder(fs,
                    Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                    PID::MUON,
                    mz_min*GeV, mz_max*GeV,
                    0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");
        
        // Book histograms:
        for (uint i = 0; i < _ptBinsGen.size() - 1; ++i)
        {
            double l_edge = _ptBinsGen[i];
            double r_edge = _ptBinsGen[i + 1];
                
            std::stringstream s1, s2;
                
            s1.precision(0);
            s2.precision(0);
                
            s1 << std::fixed;
            s2 << std::fixed;
                
            s1 << l_edge;
            s2 << r_edge;
       
            // Log10 bins
            // Ungroomed histograms (charged + neutral)
            _h_alpha05.push_back( bookHisto1D("l05_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_alpha10.push_back( bookHisto1D("l10_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_alpha20.push_back( bookHisto1D("l20_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );

            // Ungroomed histograms (charged only)
            _h_alpha05_ch.push_back( bookHisto1D("ch_l05_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_alpha10_ch.push_back( bookHisto1D("ch_l10_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_alpha20_ch.push_back( bookHisto1D("ch_l20_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );

            // Groomed histograms (charged + neutral)
            _h_sd_alpha05.push_back( bookHisto1D("SD_l05_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_sd_alpha10.push_back( bookHisto1D("SD_l10_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_sd_alpha20.push_back( bookHisto1D("SD_l20_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            
            // Groomed histograms (charged only)
            _h_sd_alpha05_ch.push_back( bookHisto1D("ch_SD_l05_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_sd_alpha10_ch.push_back( bookHisto1D("ch_SD_l10_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            _h_sd_alpha20_ch.push_back( bookHisto1D("ch_SD_l20_pT_" + s1.str() + "_" + s2.str(), linspace(_n_bins, _bin_min, _bin_max)) );
            
            // CMS bins
            // Ungroomed histograms (CMS; charged + neutral)
            _h_alpha05_cms.push_back( bookHisto1D("lin_l05_pT_" + s1.str() + "_" + s2.str(), _lhaBins) );
            _h_alpha10_cms.push_back( bookHisto1D("lin_l10_pT_" + s1.str() + "_" + s2.str(), _widthBins) );
            _h_alpha20_cms.push_back( bookHisto1D("lin_l20_pT_" + s1.str() + "_" + s2.str(), _thrustBins) );
 
            // Ungroomed histograms (CMS; charged only)
            _h_alpha05_cms_ch.push_back( bookHisto1D("ch_lin_l05_pT_" + s1.str() + "_" + s2.str(), _lhaBinsCh) );
            _h_alpha10_cms_ch.push_back( bookHisto1D("ch_lin_l10_pT_" + s1.str() + "_" + s2.str(), _widthBinsCh) );
            _h_alpha20_cms_ch.push_back( bookHisto1D("ch_lin_l20_pT_" + s1.str() + "_" + s2.str(), _thrustBinsCh) );
            
            // Groomed histograms (CMS; charged + neutral)
            _h_sd_alpha05_cms.push_back( bookHisto1D("lin_SD_l05_pT_" + s1.str() + "_" + s2.str(), _lhaBinsSD) );
            _h_sd_alpha10_cms.push_back( bookHisto1D("lin_SD_l10_pT_" + s1.str() + "_" + s2.str(), _widthBinsSD) );
            _h_sd_alpha20_cms.push_back( bookHisto1D("lin_SD_l20_pT_" + s1.str() + "_" + s2.str(), _thrustBinsSD) );
            
            // Groomed histograms (CMS; charged only)
            _h_sd_alpha05_cms_ch.push_back( bookHisto1D("ch_lin_SD_l05_pT_" + s1.str() + "_" + s2.str(), _lhaBinsChSD) );
            _h_sd_alpha10_cms_ch.push_back( bookHisto1D("ch_lin_SD_l10_pT_" + s1.str() + "_" + s2.str(), _widthBinsChSD) );
            _h_sd_alpha20_cms_ch.push_back( bookHisto1D("ch_lin_SD_l20_pT_" + s1.str() + "_" + s2.str(), _thrustBinsChSD) );
            
            // Used to compute cross section for each pT bin after cuts
            // Corresponding histogram with 1 bin to store the value of the cross section after cuts
            // Needed to get cross section for each pT bin as well as for the scale variation
            Histo1DPtr tmp_mc_xs = bookHisto1D("XS_CUTS_pT_" + s1.str() + "_" + s2.str(), linspace(1, -0.5, 0.5) );

            tuple<double, Histo1DPtr> t_tmp = make_tuple(0., tmp_mc_xs);
                
            _weights_pT.push_back(t_tmp);
        }
            
        // Used to get cross section for all pT bins
        _weights_pT_all = 0.;
  
        _h_sigma_cuts = bookHisto1D("XS_CUTS_pT_all", linspace(1, -0.5, 0.5) );
    }
    
    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
        const double weight = event.weight();

        // Convert Particles into PseudoJets for clustering
        const VetoedFinalState &fs = applyProjection<VetoedFinalState>(event, "JET_INPUT");
        
        const ParticleVector &fsParticles = fs.particles();
        
        vector<PseudoJet> particles;
        
        particles.reserve(fsParticles.size());
        
        for (uint iFS = 0; iFS < fsParticles.size(); iFS++)
        {
            PseudoJet p = fsParticles[iFS].pseudojet();
            
            p.set_user_index(fsParticles[iFS].isCharged()); // for later reference to charge
                
            particles.push_back(p);
        }

        //Cluster fs-particles into jets
        JetDefinition jet_def(antikt_algorithm, _jetR);
        
        vector<PseudoJet> jets = (SelectorPtMin(15))(jet_def(particles));

        // Reconstruct Z-boson
        const ZFinder &zfinder = applyProjection<ZFinder>(event, "ZFinder");
            
        if (zfinder.bosons().size() < 1) vetoEvent;

        const Particle &z = zfinder.bosons()[0];
        
        double zpt = z.pt();

        // Now do selection criteria
        bool passZpJ = false;
            
        if (jets.size() < 1) vetoEvent;
        
        PseudoJet jet1 = jets[0];
        
        double jet1pt = jet1.pt();
        
        double asym = fabs((jet1pt - zpt) / (jet1pt + zpt));
 
        double dphi = Rivet::deltaPhi(jet1.phi(), z.phi());
        
        passZpJ = ( (fabs(jet1.rapidity()) < 1.7) && (zpt > 30) && (asym < 0.3) && (dphi > 2.0) );

        if (!passZpJ) vetoEvent;

        if (jet1pt < _ptBinsGen[0]) vetoEvent;
        
        if (jet1pt > _ptBinsGen.back()) vetoEvent;

        // If gets here then accept event
        _weights_pT_all += weight;
        
        // Recluster using only charged particles inside jet1
        vector<PseudoJet> chargedParticles;
 
        for (auto el : jet1.constituents())
        {
            if ( el.user_index() ) chargedParticles.push_back(el);
        }

        vector<PseudoJet> chargedJets = jet_def(chargedParticles);
        
        // Run over pT bins, get angularities, fill histograms (charged + neutral and charged only)
        // Compute weights for each pT bin
        for (uint i = 0; i < _ptBinsGen.size() - 1; ++i)
        {
            double l_edge = _ptBinsGen[i];
                   
            double r_edge = _ptBinsGen[i + 1];
 
            // Run analysis per given pT bin
            if (l_edge < jet1pt && jet1pt < r_edge)
            {
                std::get<0>(_weights_pT[i]) += weight; 
            
                // Ungroomed angularities (charged + neutral)
                _h_alpha05[i]->fill(_get_ang(0.5, jet1), weight);
                _h_alpha10[i]->fill(_get_ang(1.0, jet1), weight);
                _h_alpha20[i]->fill(_get_ang(2.0, jet1), weight);
                
                // Ungroomed angularities (CMS; charged + neutral)
                _h_alpha05_cms[i]->fill(_get_ang_cms(0.5, jet1), weight);
                _h_alpha10_cms[i]->fill(_get_ang_cms(1.0, jet1), weight);
                _h_alpha20_cms[i]->fill(_get_ang_cms(2.0, jet1), weight);
                
                // Groomed angularities (charged + neutral)
                // Run SoftDrop  
                PseudoJet sd_jet = groomer(_beta, _z_cut, jet1);
                    
                _h_sd_alpha05[i]->fill(_get_ang(0.5, sd_jet), weight);
                _h_sd_alpha10[i]->fill(_get_ang(1.0, sd_jet), weight);
                _h_sd_alpha20[i]->fill(_get_ang(2.0, sd_jet), weight);
                
                // Groomed angularities (CMS; charged + neutral)
                _h_sd_alpha05_cms[i]->fill(_get_ang_cms(0.5, sd_jet), weight);
                _h_sd_alpha10_cms[i]->fill(_get_ang_cms(1.0, sd_jet), weight);
                _h_sd_alpha20_cms[i]->fill(_get_ang_cms(2.0, sd_jet), weight);
                
                if  (chargedJets.size() == 0) continue;
                
                const PseudoJet jet1_ch = chargedJets[0];

                // Ungroomed angularities (charged only)
                _h_alpha05_ch[i]->fill(_get_ang(0.5, jet1_ch), weight);
                _h_alpha10_ch[i]->fill(_get_ang(1.0, jet1_ch), weight);
                _h_alpha20_ch[i]->fill(_get_ang(2.0, jet1_ch), weight);
                
                // Ungroomed angularities (CMS; charged only)
                _h_alpha05_cms_ch[i]->fill(_get_ang_cms(0.5, jet1_ch), weight);
                _h_alpha10_cms_ch[i]->fill(_get_ang_cms(1.0, jet1_ch), weight);
                _h_alpha20_cms_ch[i]->fill(_get_ang_cms(2.0, jet1_ch), weight);

                // Groomed angularities (charged only)
                // Run SoftDrop  
                PseudoJet sd_jet_ch = groomer(_beta, _z_cut, jet1_ch);

                _h_sd_alpha05_ch[i]->fill(_get_ang(0.5, sd_jet_ch), weight);
                _h_sd_alpha10_ch[i]->fill(_get_ang(1.0, sd_jet_ch), weight);
                _h_sd_alpha20_ch[i]->fill(_get_ang(2.0, sd_jet_ch), weight);

                // Groomed angularities (CMS; charged only)
                _h_sd_alpha05_cms_ch[i]->fill(_get_ang_cms(0.5, sd_jet_ch), weight);
                _h_sd_alpha10_cms_ch[i]->fill(_get_ang_cms(1.0, sd_jet_ch), weight);
                _h_sd_alpha20_cms_ch[i]->fill(_get_ang_cms(2.0, sd_jet_ch), weight);
            }
        }
        
        
    }

    /// Normalise histograms etc., after the run
    void finalize() 
    {
        // Compute cross section (in pb) after the cuts
        double sigma_tot = crossSection() / picobarn;

        double weight_tot = sumOfWeights();
            
        for (auto el : _weights_pT)
        {
            double w_pt_bin = std::get<0>(el);
                
            double sigma_cut = w_pt_bin * sigma_tot / weight_tot;

            std::get<1>(el)->fill(0., sigma_cut);
        }
 
        double sigma_cut_all = _weights_pT_all * sigma_tot / weight_tot;
 
        _h_sigma_cuts->fill(0., sigma_cut_all);
    } 

    // Array of  ungroomed log10(lambda) histograms (charged + neutral)
    vector< Histo1DPtr > _h_alpha05;
    vector< Histo1DPtr > _h_alpha10;
    vector< Histo1DPtr > _h_alpha20;

    // Array of log10(lambda) ungroomed histograms (charged only)
    vector< Histo1DPtr > _h_alpha05_ch;
    vector< Histo1DPtr > _h_alpha10_ch;
    vector< Histo1DPtr > _h_alpha20_ch;

    // Array of groomed log10(lambda) histograms (charged + neutral)
    vector< Histo1DPtr > _h_sd_alpha05;
    vector< Histo1DPtr > _h_sd_alpha10;
    vector< Histo1DPtr > _h_sd_alpha20;
 
    // Array of groomed log10(lambda) histograms (charged only)
    vector< Histo1DPtr > _h_sd_alpha05_ch;
    vector< Histo1DPtr > _h_sd_alpha10_ch;
    vector< Histo1DPtr > _h_sd_alpha20_ch;

    // Array of ungroomed histograms (CMS linear binning; charged + neutral)
    vector< Histo1DPtr > _h_alpha05_cms;
    vector< Histo1DPtr > _h_alpha10_cms;
    vector< Histo1DPtr > _h_alpha20_cms;
 
    // Array of ungroomed histograms (CMS linear binning; charged only)
    vector< Histo1DPtr > _h_alpha05_cms_ch;
    vector< Histo1DPtr > _h_alpha10_cms_ch;
    vector< Histo1DPtr > _h_alpha20_cms_ch;
    
    // Array of groomed histograms (CMS linear binning; charged + neutral)
    vector< Histo1DPtr > _h_sd_alpha05_cms;
    vector< Histo1DPtr > _h_sd_alpha10_cms;
    vector< Histo1DPtr > _h_sd_alpha20_cms;
    
    // Array of groomed histograms (CMS linear binning; charged only)
    vector< Histo1DPtr > _h_sd_alpha05_cms_ch;
    vector< Histo1DPtr > _h_sd_alpha10_cms_ch;
    vector< Histo1DPtr > _h_sd_alpha20_cms_ch;
    
    // Array of weights and corss sections (for each pT bin)
    vector< tuple<double, Histo1DPtr> > _weights_pT;

    // use to get cross section for all pT bins
    double _weights_pT_all;
        
    Histo1DPtr _h_sigma_cuts;
    
    // A helper function to compute angularities
    // if \lambda = 0 we put it in the very first bin (which should be located at -inf)
    double _get_ang(double alpha, const PseudoJet &jet) 
    {
        double res = 0.;
            
        Angularity angularity(alpha, _jetR);
                
        double lambda_tmp = angularity(jet);
        
        if (lambda_tmp == 0.) res = 0.99 * _bin_min;
        else                  res = std::log10(lambda_tmp);
            
        return res;
    }
   
    double _get_ang_cms(double alpha, const PseudoJet &jet) 
    {
        double res = 0.;
            
        Angularity angularity(alpha, _jetR);
                
        res = angularity(jet);
            
        return res;
    }
};

// The hook for the plugin system
DECLARE_RIVET_PLUGIN(PAPER_FIN_R8);
}
