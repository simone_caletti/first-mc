#!/bin/bash

# Yoda need Python dev packeges 
# sudo apt-get install python2.7-dev python3.8-dev
# you also need to install Cmake: sudo apt-get install cmake
# and git: sudo apt-get install git

# OpenLoops will install only those libs which are needed for NLO QCD corrections 
# to Z+jet production proces.
# if you want to use OpenLoops to study some other processes make sure to install 
# corresponding libs.
# Alternatively use Recola (better for Z+jet production at NLO + merging)

#Set number of cores of your CPU
nproc=8

PREFIX=$PWD

FILE=$HOME/.bashrc

[ -z $BASH ] || shopt -s expand_aliases
alias BEGINCOMMENT="if [ ]; then"
alias ENDCOMMENT="fi"


echo "Hello, "$USER""

echo "We are going to install LHAPDF" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_lhapdf

if [ $if_lhapdf = 'yes' ]; then
	echo 'Installing LHAPDF'
	mkdir lhapdf
	wget https://lhapdf.hepforge.org/downloads/LHAPDF-6.3.0.tar.gz
	tar -xvf LHAPDF-6.3.0.tar.gz
	cd LHAPDF-6.3.0

	./configure --prefix=$PREFIX/lhapdf --disable-python
        make -j$nproc
	make check
	make install

	cd ../
	rm -r LHAPDF-6.3.0/ LHAPDF-6.3.0.tar.gz

	LHAPATH=$PREFIX/lhapdf
	echo '#LHAPDF'  | tee -a $FILE
	echo 'LHAPATH='$LHAPATH  | tee -a $FILE
	echo 'export PATH=$LHAPATH/bin:$PATH' | tee -a $FILE
	echo 'export LD_LIBRARY_PATH=$LHAPATH/lib:$LD_LIBRARY_PATH' | tee -a $FILE

	echo '...done!' 
fi

echo""
echo""
echo""
echo "We are going to install FastJet" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_fastjet

if [ $if_fastjet = 'yes' ]; then
	echo 'Installing FastJet'

	wget http://fastjet.fr/repo/fastjet-3.3.4.tar.gz
	tar -xvf fastjet-3.3.4.tar.gz
	mkdir fastjet
	cd fastjet-3.3.4

	./configure --prefix=$PREFIX/fastjet --enable-shared-install --enable-allcxxplugins
	make -j$nproc 
	make check
	make install

	cd ..
	rm -r fastjet-3.3.4/ fastjet-3.3.4.tar.gz

	echo '...done!' 
fi

echo""
echo""
echo""
echo "We are going to install FastJet::contrib" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_fjcontrib

if [ $if_fjcontrib = 'yes' ]; then
	echo 'Installing FastJet contributions'

	wget http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.045.tar.gz
	tar -xvf fjcontrib-1.045.tar.gz
	cd fjcontrib-1.045/

	./configure CXXFLAGS=-std=c++11  --fastjet-config=$PREFIX/fastjet/bin/fastjet-config
	make -j$nproc
	make fragile-shared-install
	make install

	cd ..
	rm -r fjcontrib-1.045/ fjcontrib-1.045.tar.gz

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install HepMC2" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_hepmc2

if [ $if_hepmc2 = 'yes' ]; then
	echo 'Installing HepMC2'

	wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.11.tgz
	tar -xvf hepmc2.06.11.tgz
	mkdir hepmc2
	cd HepMC-2.06.11

	./configure --prefix=$PREFIX/hepmc2 --with-momentum=GEV --with-length=MM
	make -j$nproc
	make check
	make install

	cd ..
	rm -r HepMC-2.06.11 hepmc2.06.11.tgz

	echo '..done!'
fi

echo""
echo""
echo""
echo "We are going to install YODA" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_yoda

if [ $if_yoda = 'yes' ]; then
	echo 'Installing YODA v1.8.5'

	wget https://yoda.hepforge.org/downloads/YODA-1.8.5.tar.gz
	tar -xvf YODA-1.8.5.tar.gz
	mkdir yoda-1.8.5
	cd YODA-1.8.5/

	./configure  --prefix=$PREFIX/yoda-1.8.5  PYTHON=/usr/bin/python3.8
	make -j$nproc
	make install

	cd ..
	rm -r YODA-1.8.5/ YODA-1.8.5.tar.gz

	YODAPATH=$PREFIX/yoda-1.8.5
	echo '#YODA'  | tee -a $FILE
	echo 'YODA='$YODAPATH  | tee -a $FILE

	echo 'export PATH=${PATH}:${YODA}/bin' | tee -a $FILE
	echo 'export PATH=${PATH}:${YODA}' | tee -a $FILE
	echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${YODA}/lib' | tee -a $FILE
	echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${YODA}' | tee -a $FILE
	echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${YODA}/include/' | tee -a $FILE

	echo '...done!' 
fi

echo""
echo""
echo""
echo "We are going to install zlib" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_zlib

if [ $if_zlib = 'yes' ]; then
	echo 'Installing zlib'
	
	wget http://www.zlib.net/zlib-1.2.11.tar.gz
	tar -xvf zlib-1.2.11.tar.gz
	mkdir zlib
	cd zlib-1.2.11

	./configure --prefix=$PREFIX/zlib
	make -j$nproc
	make install

	cd ..
	rm -r zlib-1.2.11/ zlib-1.2.11.tar.gz

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install RIVET2" 
echo 'Make sure that LHAPDF, FastJet, FastJet::contrib, YODA, HepMC2 and zlib are installed'
echo 'Also make sure that you have PYTHON2.7 installed at /usr/bin/python2.7'
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_rivet

if [ $if_rivet = 'yes' ]; then
	echo 'Installing RIVET2'

	wget https://rivet.hepforge.org/downloads/Rivet-2.7.2.tar.gz
	tar -xvf Rivet-2.7.2.tar.gz
	mkdir RIVET2
	cd Rivet-2.7.2/

	./configure --prefix=$PREFIX/RIVET2 \
       		--with-fastjet=$PREFIX/fastjet \
	       	--with-yoda=$PREFIX/yoda-1.8.5 \
      		--with-hepmc=$PREFIX/hepmc2 \
		--with-zlib=$PREFIX/zlib \
		PYTHON=/usr/bin/python2.7  PYTHON_VERSION=2.7
	make -j$nproc
        make check
	make install

	cp rivetenv.sh ../RIVET2
	cd ..
	rm -r  Rivet-2.7.2/ Rivet-2.7.2.tar.gz

	RIVETPATH=$PREFIX/RIVET2/rivetenv.sh
	echo '#RIVET2'  | tee -a $FILE
	echo 'source '$RIVETPATH  | tee -a $FILE
	mkdir MY_RIVET_FILES
	echo 'export RIVET_ANALYSIS_PATH='$PREFIX'/MY_RIVET_FILES' | tee -a $FILE

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install PYTHIA8" 
echo 'Make sure that LHAPDF, FastJet, FastJet::contrib, YODA, HepMC2 and RIVET are installed'
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_pythia

if [ $if_pythia = 'yes' ]; then
	echo 'Installing PYTHIA8'
	wget http://home.thep.lu.se/~torbjorn/pythia8/pythia8303.tgz

	tar -xvf pythia8303.tgz
	mkdir PYTHIA8
	cd pythia8303/ 

	./configure  --prefix=$PREFIX/PYTHIA8 \
		--with-rivet=$PREFIX/RIVET2 \
		--with-yoda=$PREFIX/yoda-1.8.5 \
		--with-fastjet3=$PREFIX/fastjet  \
		--with-hepmc2=$PREFIX/hepmc2 \
		--with-lhapdf6=$PREFIX/lhapdf

	make -j$nproc
	make install

	cp -r examples/ ../PYTHIA8
	cd ..
	rm -rf pythia8303/ pythia8303.tgz

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install SQlite3 (needed by SHERPA)" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_sqlite

if [ $if_sqlite = 'yes' ]; then
	echo 'Installing SQlite3'

	wget https://www.sqlite.org/src/tarball/sqlite.tar.gz
	tar -xvf sqlite.tar.gz
	mkdir sqlite3
	cd sqlite/

	./configure --prefix=$PREFIX/sqlite3    --disable-tcl
	make -j$nproc
	make install
	
	cd ..
	rm -r sqlite.tar.gz sqlite

	echo '...done!'
fi


echo""
echo""
echo""
echo "We are going to install Recola-collier (needed by SHERPA for NLO runs)" 
echo "Also make sure that cmake is installed"
echo "you can install it by typing: sudo apt-get install cmake"
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_recola

if [ $if_recola = 'yes' ]; then
	echo 'Installing Recola 1.4.0'

	wget https://recola.hepforge.org/downloads/recola-collier-1.4.0.tar.gz
	tar -xvf recola-collier-1.4.0.tar.gz
	mkdir recola_collier
	
	cd recola-collier-1.4.0/build
	cmake -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX/recola_collier ..
	cmake --build . --target install
	cd ../../
	rm -r recola-collier-1.4.0/ recola-collier-1.4.0.tar.gz
	cp recola_collier/lib/*.so* recola_collier/

	echo '...done!'
fi

echo ""
echo ""
echo ""
echo "We are going to install Open Loops (meeded by SHERPA and HERWIG7 for NLO runs)" 
echo "Also make sure that autoconf and git are installed"
echo "you can install them by typing: sudo apt-get install autoconf"
echo "and : sudo apt-get install git"
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_open_loops

if [ $if_open_loops = 'yes' ]; then
	echo 'Installing OpenLoops'
	git clone https://gitlab.com/openloops/OpenLoops.git
	cd OpenLoops
	nano scons
	./scons

	echo 'Installing libs which we need for Z+jet production!'
	./openloops libinstall ppllj ppllj2 ppllj_nf5 pplljj pplljjj ppvj ppvj2 ppzjj ppzjjj
	cd ..

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install SHERPA" 
echo 'Make sure that LHAPDF, FastJet, FastJet::contrib, YODA, HepMC2, RIVET, SQlite3, Recola and OpenLoops are installed'
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_sherpa

if [ $if_sherpa = 'yes' ]; then
	echo 'Installing SHERPA'

	wget https://sherpa.hepforge.org/downloads/SHERPA-MC-2.2.10.tar.gz
	tar -xvf SHERPA-MC-2.2.10.tar.gz
	mkdir SHERPA
	cd SHERPA-MC-2.2.10
	
        ./configure CXXFLAGS=-std=c++11 \
                --prefix=$PREFIX/SHERPA \
                --with-sqlite3=$PREFIX/sqlite3 \
                --enable-recola=$PREFIX/recola_collier \
                --enable-openloops=$PREFIX/OpenLoops \
                --enable-hepmc2=$PREFIX/hepmc2 \
                --enable-lhapdf=$PREFIX/lhapdf \
                --enable-fastjet=$PREFIX/fastjet \
                --enable-rivet=$PREFIX/RIVET2 --enable-pythia --enable-gzip 

	make -j$nproc
	make install
	cd ..
	rm -r SHERPA-MC-2.2.10 SHERPA-MC-2.2.10.tar.gz

	# SHERPA
	SHERPAPATH=$PREFIX/SHERPA
	echo '#SHERPA'  | tee -a $FILE
	echo 'SHERPA='$SHERPAPATH  | tee -a $FILE
	echo 'export PATH=${PATH}:${SHERPA}/bin' | tee -a $FILE

	echo '...done!'
fi

echo""
echo""
echo""
echo "We are going to install BOOST (needed by HERWIG7)" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_boost

if [ $if_boost = 'yes' ]; then
	echo 'Installing BOOST'
	wget https://dl.bintray.com/boostorg/release/1.71.0/source/boost_1_71_0.tar.gz

	tar -xvf boost_1_71_0.tar.gz
	mkdir boost-1-71-0
	cd boost_1_71_0/

	./bootstrap.sh --prefix=$PREFIX/boost-1-71-0
	./b2 install --layout=tagged

	cd ../
	rm -r boost_1_71_0/ boost_1_71_0.tar.gz
	
	echo 'done!'
fi


echo""
echo""
echo""
echo "We are going to install GSL (needed by HERWIG7)" 
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_gsl

if [ $if_gsl = 'yes' ]; then
  	echo 'Installing GSL'

	wget https://mirror.ibcp.fr/pub/gnu/gsl/gsl-latest.tar.gz
        tar -xvf gsl-latest.tar.gz
        mkdir gsl
        cd gsl-2.6/

        ./configure --prefix=$PREFIX/gsl
        make -j$nproc
        make install

        cd ../
        rm -rf gsl-2.6/ gsl-latest.tar.gz

	echo 'done!'
fi

echo""
echo""
echo""
echo "We are going to install ThePEG (needed by HERWIG7)" 
echo 'Make sure that FastJet, GSL, RIVET, HepMC2, LHAPDF, BOOST and zlib are installed'
echo "Also make sure that hd and autoconf are installed!"
echo "You can install them by typing: sudo apt-get install hgsubversion"
echo "and: sudo apt-get install autoconf"

echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_peg

if [ $if_peg = 'yes' ]; then
  	echo 'Installing ThePEG'


	hg clone https://phab.hepforge.org/source/thepeghg ThePEG
	mkdir the_peg
	cd ThePEG
	autoreconf -vi

	cd ThePEG

	./configure --prefix=$PREFIX/the_peg \
		             --with-fastjet=$PREFIX/fastjet \
			     --with-gsl=$PREFIX/gsl \
			     --with-rivet=$PREFIX/RIVET2 \
			     --with-hepmc=$PREFIX/hepmc2 \
			     --with-lhapdf=$PREFIX/lhapdf \
			     --with-boost=$PREFIX/boost-1-71-0 \
			     --with-zlib=$PREFIX/zlib
	make -j$nproc
	make check
	make install

	cd ../
	rm -rf ThePEG/ 

	echo 'done!'
fi


echo""
echo""
echo""
echo "We are going to install HERWIG7" 
echo 'Make sure that HepMC2, LHAPDF, FastJet, FastJet::contrib, OpenLoops, YODA, RIVET2, BOOST, GSL, ThePEG and PYTHIA8 are installed'
echo "Enter 'yes' to continue. Enter 'no' to skip installation of the aforementioned lib(s):"
read if_herwig

if [ $if_herwig = 'yes' ]; then
	echo 'Installing CT14lo and CT14nlo PDF sets!'
	cd lhapdf/share/LHAPDF/
	
	wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT14lo.tar.gz
	tar -xvf CT14lo.tar.gz
        rm CT14lo.tar.gz

	wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT14nlo.tar.gz
	tar -xvf CT14nlo.tar.gz
        rm CT14nlo.tar.gz

	cd $PREFIX
	echo '...done!'

  	echo 'Installing HERWIG7'

	wget https://herwig.hepforge.org/downloads/Herwig-7.2.1.tar.bz2
	tar -xvf Herwig-7.2.1.tar.bz2
	mkdir HERWIG7
	cd Herwig-7.2.1/

	./configure --prefix=$PREFIX/HERWIG7 \
		    --with-thepeg=$PREFIX/the_peg \
	 	    --with-fastjet=$PREFIX/fastjet \
		    --with-gsl=$PREFIX/gsl \
		    --with-openloops=$PREFIX/OpenLoops \
		    --with-pythia=$PREFIX/PYTHIA8 \
		    --with-boost=$PREFIX/boost-1-71-0

	 make -j$nproc
	 make install

	 cd ../
	 rm -rf Herwig-7.2.1/ Herwig-7.2.1.tar.bz2
	 
	
	 HERWIGPATH=$PREFIX/HERWIG7
	 echo '#HERWIG'  | tee -a $FILE
	 echo 'HERWIG='$HERWIGPATH  | tee -a $FILE
	 echo 'export PATH=${PATH}:${HERWIG}/bin' | tee -a $FILE

	 echo 'done!'
fi

