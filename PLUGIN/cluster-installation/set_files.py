#!/usr/bin/python2.7

import os
import sys

n_runs = int(sys.argv[1])

n = 1
while n <= n_runs:

    print("Create run_{}.sh script for Sherpa.".format(n))

    fileName = "run_" + str(n) + ".sh"

    fileObj = open(fileName, "a")

    fileObj.write("#!/usr/bin/bash\n")

    fileObj.write("Sherpa -R " + str(n + 1000) + " -A analysis_" + str(n) + "\n")

    fileObj.close()
    
    os.system("chmod +x " + fileName)

    n = n + 1

  


