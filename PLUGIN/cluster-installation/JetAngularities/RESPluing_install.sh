#!/usr/bin/env bash

# set how many cores make can use
CORES=8

# write file to set environment variables
echo "export PROJHOME=$PWD" > $PWD/projenv.sh
echo 'export PATH=$PROJHOME/bin:$PATH' >> $PWD/projenv.sh
echo 'export LD_LIBRARY_PATH=$PROJHOME/lib:$LD_LIBRARY_PATH' >> $PWD/projenv.sh
source projenv.sh

# Get fastjet and fjcontrib
wget http://fastjet.fr/repo/fastjet-3.3.4.tar.gz
tar xzf fastjet-3.3.4.tar.gz
cd fastjet-3.3.4
./configure --prefix=$PROJHOME --enable-shared --disable-auto-ptr --enable-allcxxplugins
make -j$CORES
make install  -j$CORES
cd $PROJHOME

wget http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.045.tar.gz
tar xzf fjcontrib-1.045.tar.gz
cd fjcontrib-1.045
./configure --prefix=$PROJHOME --fastjet-config=$PROJHOME/bin/fastjet-config
make -j$CORES
make install -j$CORES
make fragile-shared -j$CORES
make fragile-shared-install -j$CORES
cd $PROJHOME

# Get OpenLoops
git clone https://gitlab.com/openloops/OpenLoops.git
cd OpenLoops 
./scons
# still need to install relevant libraries
cd $PROJHOME


# Get Sherpa
# Note this clones the repo and hence needs autoreconf, alternatively just use the tarball
git clone -b rel-2-2-10 https://gitlab.com/sherpa-team/sherpa.git
cd sherpa
autoreconf -i
./configure --prefix=$PROJHOME --enable-openloops=$PROJHOME/OpenLoops --enable-fastjet=$PROJHOME --with-sqlite3=install --enable-gzip
make -j$CORES
make install -j$CORES
cd $PROJHOME


# Finally, get the actual plugin
git clone -b devel https://gitlab.com/Reichelt/SherpaResummerPlugin.git
cd SherpaResummerPlugin
scons install sherpa=$PROJHOME yodapath=$HOME/PROGRAMS/yoda-1.8.5 pythonpath=/usr/bin/python3.8 --enable-fastjetcontrib
cd $PROJHOME


echo ""
echo "Source projenv.sh to reset environment variables."
echo "Remember that OpenLoops has no libraries installed yet."
