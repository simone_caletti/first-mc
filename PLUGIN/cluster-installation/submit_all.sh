#!/bin/bash

#Submit script for Sherpa runs

i=1
while [ $i -le $1 ]; do

		bsub  -P c7 -q $2 ./run_$i.sh

	i=$(($i+1))
done