#!/usr/bin/python2.7
import os

import os
import sys

n_events = int(sys.argv[1])

n_runs = int(sys.argv[2])

mode =  str(sys.argv[3])

i = 1
while i <= n_runs:
    
    folder_name = "run" + str(i) 
    
    print "Make folder", folder_name
    
    os.makedirs(folder_name)
    
    os.system("cp test " + folder_name + "/") 

    os.system("cp CMS_2018_PAS_SMP_18_QGX_DIJET_PL.yoda " + folder_name + "/")
    os.system("cp CMS_2018_PAS_SMP_18_QGX_DIJET.yoda " + folder_name + "/")  
    
    path = "run" + str(i) + "/run.sh"

    fileObj = open(path, "a")
    fileObj.write("#!/bin/bash" + "\n")
    
    fileObj.write("./test " + str(n_events) + "   " + str(i) + "   " + mode + "    sim_" + str(i) + "\n")

    fileObj.write("mv *.yoda* ../plots/" + "\n")
    
    fileObj.write("rm test")

    os.system("chmod +x " + folder_name + "/run.sh") 

    i = i + 1


