// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Jet.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <algorithm>

// Oleh
#include <fstream>

using std::cout;
using std::endl;
using std::vector;
using namespace fastjet;


namespace Rivet {

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \class SD_array
// A simple helper class to perform scan over different SoftDrop parameters
class SD_array 
{
public:
    SD_array () : sd_gr_b0_z005 (0.0, 0.05, 0.8),
                  sd_gr_b0_z01  (0.0, 0.10, 0.8),
                  sd_gr_b0_z015 (0.0, 0.15, 0.8),
                  sd_gr_b0_z02  (0.0, 0.20, 0.8),
                  sd_gr_b0_z025 (0.0, 0.25, 0.8),
                  sd_gr_b0_z03  (0.0, 0.30, 0.8), 
                  sd_gr_b0_z035 (0.0, 0.35, 0.8), 
                  sd_gr_b0_z04  (0.0, 0.40, 0.8), 
                      
                  sd_gr_b1_z005 (1.0, 0.05, 0.8),
                  sd_gr_b1_z01  (1.0, 0.10, 0.8),
                  sd_gr_b1_z015 (1.0, 0.15, 0.8),
                  sd_gr_b1_z02  (1.0, 0.20, 0.8),
                  sd_gr_b1_z025 (1.0, 0.25, 0.8),
                  sd_gr_b1_z03  (1.0, 0.30, 0.8), 
                  sd_gr_b1_z035 (1.0, 0.35, 0.8), 
                  sd_gr_b1_z04  (1.0, 0.40, 0.8), 
                      
                  sd_gr_b2_z005 (2.0, 0.05, 0.8),
                  sd_gr_b2_z01  (2.0, 0.10, 0.8),
                  sd_gr_b2_z015 (2.0, 0.15, 0.8),
                  sd_gr_b2_z02  (2.0, 0.20, 0.8),
                  sd_gr_b2_z025 (2.0, 0.25, 0.8),
                  sd_gr_b2_z03  (2.0, 0.30, 0.8), 
                  sd_gr_b2_z035 (2.0, 0.35, 0.8), 
                  sd_gr_b2_z04  (2.0, 0.40, 0.8) { }
protected:
    contrib::SoftDrop sd_gr_b0_z005;
    contrib::SoftDrop sd_gr_b0_z01;
    contrib::SoftDrop sd_gr_b0_z015;
    contrib::SoftDrop sd_gr_b0_z02;
    contrib::SoftDrop sd_gr_b0_z025;
    contrib::SoftDrop sd_gr_b0_z03;
    contrib::SoftDrop sd_gr_b0_z035;
    contrib::SoftDrop sd_gr_b0_z04;
    
protected:
    contrib::SoftDrop sd_gr_b1_z005;
    contrib::SoftDrop sd_gr_b1_z01;
    contrib::SoftDrop sd_gr_b1_z015;
    contrib::SoftDrop sd_gr_b1_z02;
    contrib::SoftDrop sd_gr_b1_z025;
    contrib::SoftDrop sd_gr_b1_z03;
    contrib::SoftDrop sd_gr_b1_z035;
    contrib::SoftDrop sd_gr_b1_z04;

protected:
    contrib::SoftDrop sd_gr_b2_z005;
    contrib::SoftDrop sd_gr_b2_z01;
    contrib::SoftDrop sd_gr_b2_z015;
    contrib::SoftDrop sd_gr_b2_z02;
    contrib::SoftDrop sd_gr_b2_z025;
    contrib::SoftDrop sd_gr_b2_z03;
    contrib::SoftDrop sd_gr_b2_z035;
    contrib::SoftDrop sd_gr_b2_z04;
        
public: 
    PseudoJet groomer(const double beta, const double z_cut, const PseudoJet &jet) 
    {
        PseudoJet sd_jet;
            
        if (beta == 0.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b0_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b0_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b0_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b0_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b0_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b0_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b0_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b0_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 1.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b1_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b1_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b1_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b1_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b1_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b1_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b1_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b1_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else if (beta == 2.0) 
        {
            if      (z_cut == 0.05) sd_jet = sd_gr_b2_z005(jet);
            else if (z_cut == 0.1)  sd_jet = sd_gr_b2_z01(jet);
            else if (z_cut == 0.15) sd_jet = sd_gr_b2_z015(jet);
            else if (z_cut == 0.2)  sd_jet = sd_gr_b2_z02(jet);
            else if (z_cut == 0.25) sd_jet = sd_gr_b2_z025(jet);
            else if (z_cut == 0.3)  sd_jet = sd_gr_b2_z03(jet);
            else if (z_cut == 0.35) sd_jet = sd_gr_b2_z035(jet);
            else if (z_cut == 0.4)  sd_jet = sd_gr_b2_z04(jet);
            else
            {
                std::cout << "Error: beta = " << beta << " but z_cut = " << z_cut << " and is out of range!\n";
                    
                abort();
            }
        }
        else
        {
            std::cout << "Error: bete = " << beta << " and is out of range!\n";
                    
            abort();
        }
            
        return sd_jet;
    }
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @brief Routine for QG substructure analysis
  class ZJET_CONSTITUENTS_2: public Analysis, SD_array {
  public:
    
    DEFAULT_RIVET_ANALYSIS_CTOR(ZJET_CONSTITUENTS_2);

    // Jet Radius
    const double _jetR = 1;

    //pt bins
    const double pT_min = 100;
    const double pT_max = 5100;
    const double bin_width = 100;

    const double n_bins = (pT_max - pT_min) / bin_width;

    /// Book histograms and initialise projections before the run
    void init() 
    {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);

        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;

        //ZFinder
        ZFinder zfinder(fs,
                    Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                    PID::MUON,
                    mz_min*GeV, mz_max*GeV,
                    0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
        
        const double weight = event.weight();

        // Convert Particles into PseudoJets for clustering
        const VetoedFinalState & fs = applyProjection<VetoedFinalState>(event, "JET_INPUT");
      
        const ParticleVector & fsParticles = fs.particles();
      
        vector<PseudoJet> particles;
      
        particles.reserve(fsParticles.size());
        
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            particles.push_back(p);
        }
        
        JetDefinition jet_def(antikt_algorithm, _jetR);
        
        vector<PseudoJet> jets = (SelectorAbsRapMax(1.7)*SelectorPtMin(100))(jet_def(particles));

        // Reconstruct the Z and apply selection criteria   
        const ZFinder& zfinder = applyProjection<ZFinder>(event, "ZFinder");
        if (zfinder.bosons().size() < 1) vetoEvent;
        const Particle & z = zfinder.bosons()[0];
        double zpt = z.pt();

        if (jets.size() < 1) vetoEvent;
        PseudoJet jet1 = jets[0];
        double jet1pt = jet1.pt();
        
        double asym = fabs((jet1pt - zpt) / (jet1pt + zpt));
        double dphi = Rivet::deltaPhi(jet1.phi(), z.phi());

        bool passZpJ = false;
        passZpJ = ( (fabs(jet1.rapidity()) < 1.7) && (zpt > 30) && (asym < 0.3) && (dphi > 2.0) );
        if (!passZpJ) vetoEvent;
        
        if (jet1pt < pT_min) vetoEvent;
        if (jet1pt > pT_max) vetoEvent;

        // If comes here then accept event
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            particles.push_back(p);
        }


        //Create the CSV database for the ungroomed leading jet
        std::ofstream outFile;
        outFile.open("jet1_U.csv");
        outFile << "pt,eta,phi\n";

        vector<PseudoJet> constituents = jet1.constituents();
        for (unsigned j = 0; j < constituents.size(); j++)
        {
            double pt = constituents[j].pt();                   
            double eta = constituents[j].rapidity();
            double phi = constituents[j].phi();
 
            outFile << pt << "," << eta << "," << phi << "\n";
        }
        	outFile.close();
        
        //Create the CSV database for all the ungroomed jets
        outFile.open("jets_U.csv");
        outFile << "num,pt,eta,phi\n";

        for (unsigned jj = 0; jj < jets.size(); jj++)
        {   
            vector<PseudoJet> constituents = jets[jj].constituents();
            for (unsigned j = 0; j < constituents.size(); j++)
            {
            
            double pt = constituents[j].pt();                   
            double eta = constituents[j].rapidity();
            double phi = constituents[j].phi();
            outFile << jj << "," << pt << "," << eta << "," << phi << "\n";

            }
        }
        outFile.close();

        //Create the CSV database for the SoftDrop leading jet
        const double _beta = 0;
        const double _z_cut = 0.1;

        outFile.open("jet1_SD.csv");
        outFile << "pt,eta,phi\n";

        PseudoJet sd_jet1 = groomer(_beta, _z_cut, jet1);
        vector<PseudoJet> sd_constituents = sd_jet1.constituents();
        for (unsigned j = 0; j < sd_constituents.size(); j++)
        {
            double pt = sd_constituents[j].pt();                   
            double eta = sd_constituents[j].rapidity();
            double phi = sd_constituents[j].phi();
            outFile << pt << "," << eta << "," << phi << "\n";
        }
        outFile.close();

        //Create the CSV database for all the SoftDrop jets
        outFile.open("jets_SD.csv");
        outFile << "num,pt,eta,phi\n";

        for (unsigned jj = 0; jj < jets.size(); jj++)
        {   
            PseudoJet sd_jet = groomer(_beta, _z_cut, jets[jj]);
            vector<PseudoJet> sd_constituents = sd_jet.constituents();
            for (unsigned j = 0; j < constituents.size(); j++)
            {

            double pt = constituents[j].pt();                   
            double eta = constituents[j].rapidity();
            double phi = constituents[j].phi();
            outFile << jj << "," << pt << "," << eta << "," << phi << "\n";

            }
        }
        outFile.close();




    }

    /// Normalise histograms etc., after the run
    void finalize() 
    {  

    } 



  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ZJET_CONSTITUENTS_2);




}