// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Jet.hh"

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Recluster.hh"
#include "fastjet/contrib/SoftDrop.hh"

#include <algorithm>

// Oleh
#include <fstream>

using std::cout;
using std::endl;
using std::vector;
using namespace fastjet;


namespace Rivet {

/// @brief Routine for QG substructure analysis
  class ZJET_CONSTITUENTS: public Analysis {
  public:
    
    DEFAULT_RIVET_ANALYSIS_CTOR(ZJET_CONSTITUENTS);

    // Jet Radius
    const double _jetR = 1;

    //pt bins
    const double pT_min = 100;
    const double pT_max = 5100;
    const double bin_width = 100;

    const double n_bins = (pT_max - pT_min) / bin_width;

    /// Book histograms and initialise projections before the run
    void init() 
    {
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);

        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;

        //ZFinder
        ZFinder zfinder(fs,
                    Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                    PID::MUON,
                    mz_min*GeV, mz_max*GeV,
                    0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
        
        const double weight = event.weight();

        // Convert Particles into PseudoJets for clustering
        const VetoedFinalState & fs = applyProjection<VetoedFinalState>(event, "JET_INPUT");
      
        const ParticleVector & fsParticles = fs.particles();
      
        vector<PseudoJet> particles;
      
        particles.reserve(fsParticles.size());
        
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            particles.push_back(p);
        }
        
        JetDefinition jet_def(antikt_algorithm, _jetR);
        
        vector<PseudoJet> jets = (SelectorAbsRapMax(1.7)*SelectorPtMin(100))(jet_def(particles));

        // Reconstruct the Z and apply selection criteria   
        const ZFinder& zfinder = applyProjection<ZFinder>(event, "ZFinder");
        if (zfinder.bosons().size() < 1) vetoEvent;
        const Particle & z = zfinder.bosons()[0];
        double zpt = z.pt();

        if (jets.size() < 1) vetoEvent;
        PseudoJet jet1 = jets[0];
        double jet1pt = jet1.pt();
        
        double asym = fabs((jet1pt - zpt) / (jet1pt + zpt));
        double dphi = Rivet::deltaPhi(jet1.phi(), z.phi());

        bool passZpJ = false;
        passZpJ = ( (fabs(jet1.rapidity()) < 1.7) && (zpt > 30) && (asym < 0.3) && (dphi > 2.0) );
        if (!passZpJ) vetoEvent;
        
        if (jet1pt < pT_min) vetoEvent;
        if (jet1pt > pT_max) vetoEvent;

        // If comes here then accept event
        for (auto el : fsParticles)
        {
            PseudoJet p = el.pseudojet();
            
            particles.push_back(p);
        }

        std::ofstream outFile;
        outFile.open("db.csv");
        outFile << "pt, eta, phi\n";

        vector<PseudoJet> constituents = jet1.constituents();
        for (unsigned j = 0; j < constituents.size(); j++)
        {
            double pt = constituents[j].pt();                   
            double eta = constituents[j].rapidity();
            double phi = constituents[j].phi();
 
            outFile << pt << "," << eta << "," << phi << "\n";
        }
        	outFile.close();
    }

    /// Normalise histograms etc., after the run
    void finalize() 
    {  

    } 



  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ZJET_CONSTITUENTS);




}