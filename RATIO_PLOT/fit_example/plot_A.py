#!/usr/bin/python2.7
############################################################################################################################################
from matplotlib import pyplot as plt
from matplotlib import rc
import math as m
import numpy as np
import scipy.optimize as optimization
from scipy import stats

import Tkinter as tk # use tkinter for python 3
root = tk.Tk()
width = root.winfo_screenwidth()
height = root.winfo_screenheight()

from matplotlib import rcParams
############################################################################################################################################
def func(x, a):
    return 1. + pow(x - 1., a)

def func2(x, a, b):
    return 1. + a * pow(x - 1., b)

def func3(x, a, b, c):
    return 1. + 1.5 * (x - 1) * (a + b * pow(x, c) ) / (x * x)


def func4(x, a, b, c, d):
    return 1. + a * pow(x - 1., b) + c * pow(x - 1., d)

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
plot_A       = open('A_dependence.dat')
plot_A 	     = [line.split() for line in plot_A]
A            = [float(x[1])  for x in plot_A]
A_enhanement = [float(x[2])  for x in plot_A]

# Initial guess
xdata = np.asarray(A, dtype=np.float32)
ydata = np.asarray(A_enhanement, dtype=np.float32)


x_guess    = np.array([0.5])
x2_guess    = np.array([1.0, 0.5])
x3_guess    = np.array([0.0, 1.0, 3.5])
x4_guess    = np.array([1.0, 0.5, 1.0, 1.0])
    

popt, _  = optimization.curve_fit(func,  xdata, ydata, x_guess)
popt2, _ = optimization.curve_fit(func2, xdata, ydata, x2_guess)
popt3, _ = optimization.curve_fit(func3, xdata, ydata, x3_guess)
popt4, _ = optimization.curve_fit(func4, xdata, ydata, x4_guess)


fit_x = []
fit_y = []

fit_y2 = []
fit_y3 = []
fit_y4 = []

x = 1.
while  x <= 208.:
    fit_x.append(x)
    
    y  = func(x, popt[0])
    y2 = func2(x, popt2[0], popt2[1])
    y3 = func3(x, popt3[0], popt3[1], popt3[2])
    y4 = func4(x, popt4[0], popt4[1], popt4[2], popt4[3])

    fit_y.append(y)
    fit_y2.append(y2)
    fit_y3.append(y3)
    fit_y4.append(y4)

    x = x + 0.1

C1 = str( "{:.2e}".format(popt4[0]) )    
C2 = str( "{:.2e}".format(popt4[1]) )    
C3 = str( "{:.2e}".format(popt4[2]) )    
C4 = str( "{:.2e}".format(popt4[3]) )    
############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
f, ax = plt.subplots(1, 1, sharex=True, figsize=(width/80., height/50.), dpi=100)


plt.plot(A, A_enhanement, 'bo', markersize=15, label = 'NUCLEUS')

#plt.plot(fit_x, fit_y, linewidth = 2, label = 'FIT 1')
#plt.plot(fit_x, fit_y2, linewidth = 2, label = 'FIT 2')
#plt.plot(fit_x, fit_y3, linewidth = 2, label = 'FIT 3')
plt.plot(fit_x, fit_y4, linewidth = 4, color = 'red', label = 'FIT 4 PARAMETERS')


ax.grid(True)

xticks_value =[4, 6, 12, 20, 40, 63, 80, 100, 120, 129, 140, 160, 180, 197, 208]
    
ax.set_xticks(xticks_value)
#ax.set_xticklabels(xticks)

# Change size and font of tick labels
fontsize = 28

ax.xaxis.set_tick_params(width=4, length=6)
ax.yaxis.set_tick_params(width=4, length=6)

for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)
    tick.label1.set_fontweight('bold')


#plot.set_xlim(min(x), max(x))
ax.set_ylim(1.0, 3.5)
ax.set_xlim(1., 210.)
    

ax.legend(frameon = True,loc = 'best', fontsize = 50)

rcParams['axes.titlepad'] = 50 

ax.text(0.45,0.06, 'NN repulsion $\\rm d = 0.9$ fm'+
		  '\nWood-Saxon parametrization $4 \leq A \leq 208$' + 
		  '\n $ \\rho_A(r) = \\rho_0 \, \\frac{1}{1 + exp((r - R) / a)}$\n' +
		  r'$\frac{1}{A} \, \frac{\sigma_{pA}^{DPS}}{\sigma_{pp}^{DPS}} = 1 + C_1 (A - 1)^{C_2} +  C_3 (A - 1)^{C_4}}$' + 
		  '\n$C_1$ = ' + C1 +
		  '\n$C_2$ = ' + C2 +
		  '\n$C_3$ = ' + C3 +
		  '\n$C_4$ = ' + C4,
	          transform = ax.transAxes, bbox=dict(facecolor='white', alpha=1.0), fontsize=50)

ax.set_xlabel(r'Atomic mass number A', size = 70)
ax.set_ylabel(r'$\frac{1}{A} \, \frac{\sigma_{pA}^{DPS}}{\sigma_{pp}^{DPS}}$', size = 70)
ax.set_title(r'Enhancement of the DPS cross section in pA collisions', size = 70)

plt.savefig('DPS_pA_enchancement_hard_core.pdf', bbox_inches='tight')

#plt.show()


