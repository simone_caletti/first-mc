#!/usr/bin/env python3

from matplotlib import pyplot as plt

import matplotlib as mpt

import numpy as np

# Nice fonts
#mpt.rc('text', usetex=True)
#mpt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
#mpt.rcParams["legend.framealpha"] = None


import tkinter as tk # use tkinter for python 3
root = tk.Tk()
width = root.winfo_screenwidth()
height = root.winfo_screenheight()

import matplotlib.gridspec as gridspec

import math as m
from mylibs.mylib3 import *
#####################
def plot_all():
    # Adjust width and height with respect to your screen
    f, ax = plt.subplots(1, 1, sharex=True, figsize=(width/40., height/25.), dpi=100)

    # Set subplots and their relative height
    gs = gridspec.GridSpec(2, 1, height_ratios = [3, 1])

    gs.update(wspace = 0.0, hspace = 0.0)

    ax = []
    
    # Create a column of plots
    ax     += [plt.subplot(gs[0, 0])]
    ax     += [plt.subplot(gs[1, 0])]
    
    set_x_axis(ax[0], 0, 1, 0.1, False)
    set_x_axis(ax[1], 0, 1, 0.1)
    
    # Customize y-axis
    set_y_axis(ax[0], 0.0, 1., 0.1)
    set_y_axis(ax[1], 0.5, 2., 0.5)

    # Customize grids
    ax[0].grid(True, which = 'major')
    ax[1].grid(True, which = 'major')

    # Set lables for y-axes
    ax[0].set_ylabel ('$\epsilon_G$', size = 90)
    ax[1].set_ylabel ('$\\mathrm{Ratio}$', size = 90)
    
    ax[1].set_xlabel ('$\epsilon_Q$', size = 90)

    
    # Put some text on top
    tit = r'$\mathrm{AK8 \, jets,}$'
        
    ax2 = ax[0].twiny()
    ax2.set_xticks([])
    ax2.set_xlabel (tit, size = 65, labelpad=20)

    # Remove overlapping ticks
    for i in range(0, 2):
        yticks = ax[i].yaxis.get_major_ticks()
        yticks[-1].set_visible(False)
        yticks[0].set_visible(False)
        
        xticks = ax[i].xaxis.get_major_ticks()
        xticks[-1].set_visible(False)
        xticks[0].set_visible(False)
        
        # Set pads such that tick lables do not overlap with axes
        ax[i].tick_params(axis='x', which='major', pad=15)
        ax[i].tick_params(axis='y', which='major', pad=15)


    # Make a plot more compact 
    plt.tight_layout(rect=[0.01, 0.01, 0.99, 0.99])
    

    # Actual plotting starts here 
    # Just an example
    # Change this part
    mu, sigma = 0.5, 0.8 # mean and standard deviation

    s = np.random.normal(mu, sigma, 10000)
    
    count, bins, ignored = ax[0].hist(s, 100,  linewidth=4, label = 'It is a distribution', density=True)
    
    ax[0].plot(bins, get_gauss(bins, sigma, mu), linewidth=4, label = 'It is exact Gauss', color='r')
    
    
    bin_center = []
    
    for i, el in enumerate(bins[:-1]):
        bin_center.append( 0.5 * (bins[i + 1] + bins[i]) )
    
    
    ratio = []
    for i, j in zip(count, bin_center):
        ratio.append(i / get_gauss(j, sigma, mu))
    
    ax[1].plot(bin_center, ratio, linewidth=4, label='It is per bin ratio', color='r')
   
    # Draw a horizontla line to guide the eye
    X0 = [0, 1]
    Y0 = [1, 1]
 
    ax[1].plot(X0, Y0, linewidth = 2, color = 'black')
 
    # Legend for histograms
    ax[0].legend(loc = 2, borderaxespad=2., prop={'size': 90})
    ax[1].legend(loc = 2, borderaxespad=2., prop={'size': 90})

    plt.savefig('test_ratio.pdf')
    
    plt.close(f)
###########################

# Make a plot
plot_all()




